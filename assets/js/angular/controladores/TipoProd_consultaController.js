angular.module("ContentManagerApp")
	.controller("tipoProd_consultaController", function($scope,$http,$location,serverDataMensajes,tipoProdFactory,sesionFactory,idiomaFactory){
		$(".li-menu").removeClass("active");
        $("#li_productos").addClass("active");
        $(".a-menu").removeClass("active");
        $("#tipo_prod").addClass("active");
		$scope.titulo_pagina = "Consulta Tipo de Producto";
		$scope.base_url = $("#base_url").val();
		$scope.id_tipo_prod = "";
		$scope.tipo_prod = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'categoria_prod': {
							"id":"",
							"titulo":"",
							"descripcion":""
						},
						'titulo' : '',
						'descripcion' : '',
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
			    "order": [[ 0, "desc" ]]
			});
			console.log($scope.tipo_prod);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_tipoProd = function(){
			tipoProdFactory.asignar_valores("","",$scope.base_url)
			tipoProdFactory.cargar_tipoProd(function(data){
				$scope.tipo_prod=data;
				console.log($scope.tipo_prod);
			});
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.ver_tipoprod = function(valor){
			var id = $("#ver"+valor).attr("data");
			$scope.id_tipo_prod = id;
			$("#id_tipo_prod").val($scope.id_tipo_prod)
			let form = document.getElementById('formConsultaTipoProd');
			form.action = "./tipoProdVer";
			form.submit();
		}
		////////////////////////////////////////////////////////////////////////////////////////.
		$scope.activar_registro = function(event){
			$scope.id_seleccionado_tipoprod = []
			$scope.estatus_seleccionado_tipoprod = []
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_tipoprod = arreglo_atributos[0];
			$scope.estatus_seleccionado_tipoprod = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");
			//--
			if ($scope.estatus_seleccionado_tipoprod==0){
				mensaje = "Desea modificar el estatus de este registro a publicado? ";
				$scope.estatus_seleccionado_tipoprod=1;
			}else{
				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
				$scope.estatus_seleccionado_tipoprod=0
			}
			//($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

			//--
			$scope.modificar_estatus("activar_inactivar",mensaje)
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.modificar_estatus = function(opcion,mensaje){
			 swal({
				  title: 'Esta seguro?',
				  text: mensaje,
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si!',
				  cancelButtonText: 'No',
				}).then((result) => {
					  if (result.value) {
						$scope.accion_estatus()
					  }
				})
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.accion_estatus = function(){
			$http.post($scope.base_url+"/tipo_prod/modificar_estatus",
			{
				'id' : $scope.id_seleccionado_tipoprod,
				'estatus' : $scope.estatus_seleccionado_tipoprod,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					Swal(
						  'Realizado!',
						  'El proceso fue ejecutado.',
						  'success'
					).then((result) => {
						  if (result.value) {
								let form = document.getElementById('formConsultaTipoProd');
								form.action = "./consultar_tipoProd";
								form.submit();
						  }

					});
				}else{
					Swal(
						  'No realizado!',
						  'El proceso no pudo ser ejecutado.',
						  'warning'
					)
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.eliminar_registro = function(event){
			$scope.id_seleccionado_tipoprod = []
			$scope.estatus_seleccionado_tipoprod = []
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data")
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_tipoprod = arreglo_atributos[0];
			$scope.estatus_seleccionado_tipoprod = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado_tipoprod	==2){
				mensaje = "Desea eliminar este registro? ";
			}
			$scope.modificar_estatus("eliminar",mensaje)
		}
		////////////////////////////////////////////////////////////////////////////////////////
		setTimeout(function(){
				$scope.iniciar_datatable();
		},500);

		$scope.consultar_tipoProd();
	})
