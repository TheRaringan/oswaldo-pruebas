angular.module("ContentManagerApp")
	.controller("detalleProd_consultaController", function($scope,$http,$location,serverDataMensajes,detalleProdFactory,sesionFactory,idiomaFactory){
		$(".li-menu").removeClass("active");
		$("#li_productos").addClass("active");
		$(".a-menu").removeClass("active");
		$("#detalle_prod").addClass("active");

		$scope.titulo_pagina = "Detalle de Productos";
		$scope.subtitulo_pagina = "Registro";
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.base_url = $("#base_url").val();

		$scope.id_detalle_prod = "";
		$scope.detalle_prod = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'categoria_prod': {
							"id":"",
							"titulo":"",
							"descripcion":""
						},
						'tipo_prod': {
							"id":"",
							"titulo":"",
							"descripcion":""
						},
						'marca': {
							"id":"",
							"titulo":"",
							"descripcion":""
						},
						'titulo' : '',
						'descripcion' : '',
						'color': {
							"id":"",
							"descripcion":""
						},
						'talla': {
							"id":"",
							"descripcion":""
						},
						'precio': '',
						'id_imagen': ''
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
			    "order": [[ 0, "desc" ]]
			});
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_detalleProd = function(){
			detalleProdFactory.asignar_valores("","",$scope.base_url)
			detalleProdFactory.cargar_detalleProd(function(data){
				$scope.detalle_prod=data;
				console.log($scope.detalle_prod);
			});
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.ver_detalleprod = function(valor){
			var id = $("#ver"+valor).attr("data");
			$scope.id_detalle_prod = id;
			$("#id_detalle_prod").val($scope.id_detalle_prod)
			let form = document.getElementById('formConsultadetalleProd');
			form.action = "./detalleProdVer";
			form.submit();
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.ver_detalleprodClonar = function(valor){
			var data = $("#clonar"+valor).attr("data");
			var arr_id = data.split("|")
			console.log(arr_id)
			var id = arr_id[0]
			var clonado = arr_id[1]
			if(clonado=="0"){
				$scope.id_detalle_prod = id;
				$("#id_detalle_prod").val($scope.id_detalle_prod)
				let form = document.getElementById('formConsultadetalleProd');
				form.action = "./detalleProdVerClonar";
				form.submit();
			}else{
				mostrar_notificacion("Mensaje","Ya se realizó una clonación a dicho producto!","warning");
			}
			
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.activar_registro = function(event){
			$scope.id_seleccionado_detprod = []
			$scope.estatus_seleccionado_detprod = []
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_detprod = arreglo_atributos[0];
			$scope.estatus_seleccionado_detprod = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");
			//--
			if ($scope.estatus_seleccionado_detprod==0){
				mensaje = "Desea modificar el estatus de este registro a publicado? ";
				$scope.estatus_seleccionado_detprod=1;
			}else{
				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
				$scope.estatus_seleccionado_detprod=0
			}
			//($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

			//--
			$scope.modificar_estatus("activar_inactivar",mensaje)
		}
		//----------------------------------------------------------------
		$scope.modificar_estatus = function(opcion,mensaje){
			 swal({
				  title: 'Esta seguro?',
				  text: mensaje,
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si!',
				  cancelButtonText: 'No',
				}).then((result) => {
					  if (result.value) {
						$scope.accion_estatus()
					  }
				})
		}
		//----------------------------------------------------------------
		$scope.accion_estatus = function(){

			$http.post($scope.base_url+"/Detalle_prod/modificarDetalleEstatus",
			{
				 'id':$scope.id_seleccionado_detprod,
				 'estatus':$scope.estatus_seleccionado_detprod,

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					Swal(
						  'Realizado!',
						  'El proceso fue ejecutado.',
						  'success'
					).then((result) => {
						  if (result.value) {
								let form = document.getElementById('formConsultadetalleProd');
								form.action = "./consultar_detProd";
								form.submit();
						  }

					});
				}else if($scope.mensajes.mensaje == "existe_carrito"){
					Swal(
						  'No realizado!',
						  'El proceso no pudo ser ejecutado ya que el pŕoducto se encuentra asociado a una compra ',
						  'warning'
					)
				}else{
					Swal(
						  'No realizado!',
						  'El proceso no pudo ser ejecutado.',
						  'warning'
					)
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.eliminar_registro = function(event){
			$scope.id_seleccionado_detprod = []
			$scope.estatus_seleccionado_detprod = []
			var caja = event.currentTarget.id;
			//alert(caja);
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_detprod = arreglo_atributos[0];
			$scope.estatus_seleccionado_detprod = arreglo_atributos[1];
			console.log($scope.estatus_seleccionado_marca);
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado_detprod==2){
				mensaje = "Desea eliminar este registro? ";
			}
			$scope.modificar_estatus("eliminar",mensaje)
		}
		////////////////////////////////////////////////////////////////////////////////////////
		setTimeout(function(){
				$scope.iniciar_datatable();
		},500);
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_detalleProd();
	})
