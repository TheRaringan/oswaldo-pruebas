angular.module("ContentManagerApp")
	.controller("Color_Controller", function($scope,$http,$location,serverDataMensajes,sesionFactory,galeriaFactory,idiomaFactory,marcasFactory,coloresFactory){

		$(".li-menu").removeClass("active");
		$("#li_productos").addClass("active");
		$(".a-menu").removeClass("active");
		$("#colores").addClass("active");

		$scope.titulo_pagina = "Registrar colores";
		$scope.subtitulo_pagina = "Registro";
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresar el contenido"
		$scope.activo_img = "inactivo";
		$scope.searchMarcas = []
		$scope.borrar_imagen = []

		$scope.color = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'descripcion' : '',
						'id_imagen' : '',
						'imagen' : ''
		}

		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				//console.log($scope.idioma);
			});
		}
		///////////////////////////////////////////////////////////////
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.color.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.color.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.color.descripcion)
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('2','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
                console.log($scope.galery);

			});
		}

		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}
		//PARA SELECCIONAR UNA IMAGEN Y MOSTRARLA EN EL CAMPO DE IMG
		$scope.seleccionar_imagen = function(event){
				var imagen = event.target.id;//Para capturar id
				console.log(imagen);
				var vec = $("#"+imagen).attr("data");
				var vector_data = vec.split("|")
				var id_imagen = vector_data[0];
				var ruta = vector_data[1];

				$scope.borrar_imagen.push(id_imagen);
				$scope.activo_img = "activo"
				$scope.color.id_imagen = id_imagen
				$scope.color.imagen = ruta
				//alert($scope.color.id_imagen);
				//--
				$("#modal_img1").modal("hide");
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.limpiar_cajas_color = function(){
			$scope.color = {
							'id': '',
							'idioma': '',
							'id_idioma' : '',
							'titulo' : '',
							'descripcion' : '',
							'id_imagen' : '',
							'imagen' : ''
			}

			$scope.activo_img = "inactivo";
			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.registrarColor = function(){
			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.color.id!="")&&($scope.color.id!=undefined)){
					$scope.modificar_colores();
				}else{
					$scope.insertar_colores();
				}

			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.validar_form = function(){
			//console.log($scope.color)
			if(($scope.color.id_idioma=="")||($scope.color.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			}else if($scope.color.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar un color","warning");
				return false;
			}//else if(($scope.color.id_imagen=="NULL")||($scope.color.id_imagen=="")){
			// 	mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
			// 	return false;
			// }
			else{
				return true;
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.insertar_colores = function(){
			$http.post($scope.base_url+"/Colores/registrarColores",
			{
				'id' 	     : $scope.color.id,
				'id_idioma'  : $scope.color.id_idioma.id,
				'descripcion': $scope.color.descripcion,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_color();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe ese color","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
				$mensajes["mensaje"] = "no_registro";

			}).error(function(data,estatus){
				console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultarColorIndividual = function(){
			coloresFactory.asignar_valores("",$scope.id_color,$scope.base_url)
			coloresFactory.cargar_colores(function(data){
				$scope.color=data[0];
				console.log(data[0]);
				$("#div_descripcion").html($scope.color.descripcion)
				$scope.borrar_imagen.push($scope.color.id_imagen);
				$scope.activo_img = "activo"
				$scope.color.imagen = $scope.color.ruta
				$scope.titulo_pagina = "Modificar color";
				$scope.subtitulo_pagina  = "Modificar";
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.color.id_idioma+'"]').prop('selected', true);
				},300);
				$("#idioma").prop('disabled', true);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.modificar_colores = function(){
			$http.post($scope.base_url+"/Colores/modificarColores",
			{
                'id' 	     : $scope.color.id,
				'id_idioma'  : $scope.color.id_idioma.id,
				'descripcion': $scope.color.descripcion,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					//$scope.limpiar_cajas_color();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe ese color","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
				$mensajes["mensaje"] = "no_registro";

			}).error(function(data,estatus){
				//console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();

        $scope.id_color = $("#id_color").val();

        if($scope.id_color){
              $scope.consultarColorIndividual();
		}
	})
