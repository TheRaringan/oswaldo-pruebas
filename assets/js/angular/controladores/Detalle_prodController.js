angular.module("ContentManagerApp")
	.controller("Detalle_prodController", function($scope,$http,$location,serverDataMensajes,sesionFactory,marca_consFactory,detalleProdFactory,idiomaFactory,categoriaProdFactory,tipoProdFactory,detalle_form_ProdFactory,galeriaFactory,tallaFactory,colorFactory){
		$(".li-menu").removeClass("active");
		$("#li_productos").addClass("active");
		$(".a-menu").removeClass("active");
		$("#det_prod").addClass("active");

		$scope.titulo_pagina = "Detalle de Productos";
		$scope.subtitulo_pagina = "Registro";
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresas el contenido"

		$scope.currentTab = 'datos_basicos'
		$scope.activo_img = "inactivo";
		$scope.inhabilitarImg = false

		$scope.detalle_prod = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'categoria_prod': {
							"id":"",
							"titulo":"",
							"descripcion":""
						},
						'tipo_prod': {
							"id":"",
							"titulo":"",
							"descripcion":""
						},
						'marca': {
							"id":"",
							"titulo":"",
							"descripcion":""
						},
						'titulo' : '',
						'descripcion' : '',
						'color': {
							"id":"",
							"descripcion":""
						},
						'talla': {
							"id":"",
							"descripcion":""
						},
						'precio': '',
						'cantidad' : '',
						'id_imagen': ''
		}

		$scope.imagenes_marca = []
		$scope.borrar_imagen = []
		$scope.galeria_marca = []
		$scope.imagenes_productos = []
		$scope.activo_img_soportes = "inactivo"
		$scope.path_imagen_seleccionada = []
		$scope.galery = []

		///////////////////////////////////////////////////////////////
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				$scope.detalle_prod.id_idioma = data[0]
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.cargar_catprod_idioma = function(){
			/*if ($scope.detalle_prod.id_idioma.id!= '') {*/
			//categoriaProdFactory.asignar_valor('1',$scope.detalle_prod.id_idioma.id);//ya que es galeria de imagenes
			categoriaProdFactory.asignar_valor('1','1');//ya que es galeria de imagenes
			categoriaProdFactory.cargar_catprod(function(data){
				$scope.catprod=data;
				console.log($scope.catprod)
			});
			/*}*/
		}

		$scope.cargar_catprod_idioma_edit = function(){
			//$scope.prueba = $("#idioma").val();
			if ($scope.detalle_prod.id_idioma!= '') {
				categoriaProdFactory.asignar_valor('1',$scope.detalle_prod.id_idioma);//ya que es galeria de imagenes
				categoriaProdFactory.cargar_catprod(function(data){
					$scope.catprod=data;
					//$('#idioma > option[value="'+$scope.detalle_prod.id_idioma+'"]').prop('selected', true);
				});
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.cargar_marca_idioma = function(){
		    //marca_consFactory.asignar_valor('1',$scope.detalle_prod.id_idioma.id);//ya que es galeria de imagenes
		    marca_consFactory.asignar_valor('1','1');//ya que es galeria de imagenes
		    marca_consFactory.cargar_marca(function(data){
		        $scope.marca=data;
		        console.log($scope.marca);
		    });
		}
		$scope.cargar_marca_idioma_edit = function(){
		    marca_consFactory.asignar_valor('1',$scope.detalle_prod.id_idioma);//ya que es galeria de imagenes
		    marca_consFactory.cargar_marca(function(data){
		        $scope.marca=data;
		        console.log($scope.marca);
		    });
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.cargar_color_idioma = function(){
			//colorFactory.asignar_valor('1',$scope.detalle_prod.id_idioma.id);//ya que es galeria de imagenes
			colorFactory.asignar_valor('1','1');//ya que es galeria de imagenes
 	       	colorFactory.cargar_color(function(data){
 	           $scope.color=data;
		    });
		}
		$scope.cargar_color_idioma_edit = function(){
		    colorFactory.asignar_valor('1',$scope.detalle_prod.id_idioma);//ya que es galeria de imagenes
		    colorFactory.cargar_color(function(data){
		        $scope.color=data;
		    });
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_talla = function(){
			tallaFactory.asignar_valores("")
			tallaFactory.cargar_talla(function(data){
				$scope.talla=data;
				/*if(editar!=""){
					$('#talla > option[value="'+$scope.detalle_prod.talla+'"]').prop('selected', true);
				}*/
				//console.log($scope.talla);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.cargar_tipoprod = function(){
//			detalle_form_ProdFactory.asignar_valor('1',$scope.detalle_prod.categoria_prod.id);//ya que es galeria de imagenes
			detalle_form_ProdFactory.asignar_valor('1','1');//ya que es galeria de imagenes
			detalle_form_ProdFactory.cargar_detprod(function(data){
				$scope.detprod=data;
				console.log($scope.detprod);
			});
		}

		$scope.cargar_tipoprod_edit = function(){
			detalle_form_ProdFactory.asignar_valor('1',$scope.detalle_prod.id_idioma);//ya que es galeria de imagenes
			detalle_form_ProdFactory.cargar_detprod(function(data){
				$scope.detprod=data;
				console.log($scope.detprod);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.agregarWisi = function(){
			$scope.detalle_prod.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.detalle_prod.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.detalle_prod.descripcion)
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('22','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galeria_m=data;
				//console.log($scope.galeria_m);
			});
		}

		$scope.seleccione_img_marcas = function(){
			if(($scope.clonar!=1)&&($scope.inhabilitarImg==false)){
				//-----------------------------------
				$("#modal_img2").modal("show");
				var arreglo_marcas = $scope.galeria_marca;
				console.log( $scope.galeria_marca)
				var galeria_soporte = $scope.galeria_m
				var galeryx = new Array();
				for (i=0;i<galeria_soporte.length;i++){
					galeryx[i] = galeria_soporte[i]["ruta"];
				}
				console.log(arreglo_marcas)
				//Marco c/u
				for(j=0;j<arreglo_marcas.length;j++){
					posicion = galeryx.indexOf(arreglo_marcas[j]);
					//alert(arreglo_marcas[j]);
					//alert(posicion);
					$scope.seleccionar_imagen_soportes_individual("img_soporte"+posicion);
				}
				//-----------------------------------
			}
		}

		$scope.seleccionar_imagen_soportes_individual = function(imagen){
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			//--
			$("#"+imagen).addClass("marcado");
			if($scope.imagenes_productos.indexOf(id_imagen)==-1){
				$scope.imagenes_productos.push(id_imagen);//manejo los id
			}
			if($scope.galeria_marca.indexOf(ruta)==-1){
				$scope.galeria_marca.push(ruta); //maneja las rutas
			}
			$scope.activo_img_soportes = "activo"
		}

		$scope.limpiar_arreglos = function(){
			$scope.imagenes_productos = []
			$scope.galeria_marca = []
			$(".imgbiblioteca").removeClass("marcado");
			console.log($scope.galeria_marca)
			console.log($scope.imagenes_productos)
		}

		$scope.seleccionar_imagen_soportes = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			console.log(ruta);
			if(($("#"+imagen).hasClass("marcado"))==true){
				$("#"+imagen).removeClass("marcado");
				$indice = $scope.imagenes_productos.indexOf(id_imagen);
				$scope.imagenes_productos.splice($indice,1);
				$indice_ruta = $scope.galeria_marca.indexOf(ruta);
				$scope.galeria_marca.splice($indice_ruta,1);
				if($scope.galeria_marca.length==0){
					$scope.activo_img_soportes = "inactivo"
				}
			}else{
				$("#"+imagen).addClass("marcado");
				$scope.imagenes_productos.push(id_imagen);//manejo los id
				$scope.galeria_marca.push(ruta); //maneja las rutas
				$scope.activo_img_soportes = "activo"
			}
			console.log($scope.galeria_marca)
			console.log($scope.imagenes_productos)
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.registrardetProd = function(){	//Para guardar y modificar
			if($scope.validar_form()==true){
				if($scope.clonar==0){
				//----------------------
					if(($scope.detalle_prod.id!="")&&($scope.detalle_prod.id!=undefined)){
						$scope.modificar_detalleProd();
					}else{
						$scope.insertar_detalleProd();
					}
				//----------------------	
				}else if($scope.clonar==1){
					//----------------------
					if(($scope.detalle_prod.id!="")&&($scope.detalle_prod.id!=undefined)){
						$scope.clonar_detalleProd();
					}else{
						
					}
					//----------------------	
				}
				
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.validar_form = function(){
			console.log($scope.detalle_prod);
			if(($scope.detalle_prod.id_idioma=="")||($scope.detalle_prod.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			}else if (($scope.detalle_prod.marca.id=="")||($scope.detalle_prod.marca.id=="")) {
				mostrar_notificacion("Campos no validos","Debe seleccionar una Linea","warning");
				return false;
			}else if(($scope.detalle_prod.categoria_prod.id=="")||($scope.detalle_prod.categoria_prod.id=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar un Género","warning");
				return false;
			}else if (($scope.detalle_prod.tipo_prod.id=="")||($scope.detalle_prod.tipo_prod.id=="")) {
				mostrar_notificacion("Campos no validos","Debe seleccionar un Tipo de Producto","warning");
				return false;
			}else if (($scope.detalle_prod.color.id=="")||($scope.detalle_prod.color.id=="")){
				mostrar_notificacion("Campos no validos","Debe ingresar un Color","warning");
				return false;
			}else if (($scope.detalle_prod.talla.id=="")||($scope.detalle_prod.talla.id=="")){
				mostrar_notificacion("Campos no validos","Debe ingresar una Talla","warning");
				return false;
			}else if($scope.detalle_prod.precio==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el Precio","warning");
				return false;
			}else if($scope.detalle_prod.cantidad==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el cantidad","warning");
				return false;
			}else if($scope.detalle_prod.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if($scope.detalle_prod.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
				return false;
			}else if($scope.imagenes_productos.length==0){
				mostrar_notificacion("Campos no validos","Debe ingresar al menos una imagen","warning");
				return false;
			}else{
				return true;
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.limpiar_cajas_detProd = function(){
			if($scope.clonar==0){
				/*$scope.detalle_prod = {
							'id': '',
							'idioma': '',
							'id_idioma' : '',
							'categoria_prod': {
								"id":"",
								"titulo":"",
								"descripcion":""
							},
							'tipo_prod': {
								"id":"",
								"titulo":"",
								"descripcion":""
							},
							'marca': {
								"id":"",
								"titulo":"",
								"descripcion":""
							},
							'titulo' : '',
							'descripcion' : '',
							'cantidad' : '',
							'id_imagen': ''
				}*/
				//--Limpio solo algunos campos
				$scope.detalle_prod.id = ''
				//-Categoria
				$scope.detalle_prod.categoria_prod.id=''
				$scope.detalle_prod.categoria_prod.titulo=''
				$scope.detalle_prod.categoria_prod.descripcion=''
				//-Tipo porducto
				$scope.detalle_prod.tipo_prod.id=''
				$scope.detalle_prod.tipo_prod.titulo=''
				$scope.detalle_prod.tipo_prod.descripcion=''
				//-Marca
				$scope.detalle_prod.marca.id=''
				$scope.detalle_prod.marca.titulo=''
				$scope.detalle_prod.marca.descripcion=''
				//-Color
				$scope.detalle_prod.color.id=''
				$scope.detalle_prod.color.titulo=''
				$scope.detalle_prod.color.descripcion=''
				//-talla
				$scope.detalle_prod.talla.id=''
				$scope.detalle_prod.talla.titulo=''
				$scope.detalle_prod.talla.descripcion=''
				//
				$scope.detalle_prod.precio=''
				$scope.detalle_prod.titulo=''
				$scope.detalle_prod.descripcion=''	
				$scope.detalle_prod.cantidad=''
				$scope.detalle_prod.id_imagen=''

				//console.log($scope.detalle_prod)
				$scope.limpiar_arreglos();
				$scope.activo_img_soportes = "inactivo"
			}else{
				$scope.detalle_prod.titulo = ""
			}
			
			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
			$("#idioma > option[value='']").removeAttr('selected', 'selected');
			$("#idioma > option[value='1']").attr('selected', 'selected');
			$scope.detalle_prod.id_idioma.id = 1
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.clonar_detalleProd = function(){
			alert($scope.detalle_prod.categoria_prod);
			$http.post($scope.base_url+"/detalle_prod/clonar_detalleProd",
			{
				'id' 	     		: $scope.detalle_prod.id,
				'id_idioma'  		: '2',
				'categoria_prod' 	: $scope.detalle_prod.categoria_prod,
				'tipo_prod' 		: $scope.detalle_prod.tipo_prod,
				'marca' 			: $scope.detalle_prod.marca,
				'color' 			: $scope.detalle_prod.color,
				'talla' 			: $scope.detalle_prod.talla,
				'precio' 			: $scope.detalle_prod.precio,
				'titulo'     		: $scope.detalle_prod.titulo,
				'descripcion'		: $scope.detalle_prod.descripcion,
				'cantidad'			: $scope.detalle_prod.cantidad,
				'id_imagen' 		:$scope.imagenes_productos,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					if($scope.clonar==0)
						$scope.limpiar_cajas_detProd();
					else
						$("#btn-registrar,#btn-limpiar").prop("disabled",true)
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una producto con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.insertar_detalleProd = function(){
			$http.post($scope.base_url+"/detalle_prod/insertar_detalleProd",
			{
				'id' 	     		: $scope.detalle_prod.id,
				'id_idioma'  		: $scope.detalle_prod.id_idioma.id,
				'categoria_prod' 	: $scope.detalle_prod.categoria_prod.id,
				'tipo_prod' 		: $scope.detalle_prod.tipo_prod.id,
				'marca' 			: $scope.detalle_prod.marca.id,
				'color' 			: $scope.detalle_prod.color.id,
				'talla' 			: $scope.detalle_prod.talla.id,
				'precio' 			: $scope.detalle_prod.precio,
				'titulo'     		: $scope.detalle_prod.titulo,
				'descripcion'		: $scope.detalle_prod.descripcion,
				'cantidad'			: $scope.detalle_prod.cantidad,
				'id_imagen' 		:$scope.imagenes_productos,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					if($scope.clonar==0)
						$scope.limpiar_cajas_detProd();
					else
						$("#btn-registrar,#btn-limpiar").prop("disabled",true)
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una producto con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultaDetalleProdIndividual = function(){
			detalleProdFactory.asignar_valores("",$scope.id_detalle_prod,$scope.base_url)
			detalleProdFactory.cargar_detalleProd(function(data){
				$scope.detalle_prod=data[0];

				$("#div_descripcion").html($scope.detalle_prod.descripcion)

				$scope.activo_img_soportes = "activo"

				arreglo_marca = $scope.detalle_prod.imagen[0]['ruta']
				$scope.galeria_marca  = arreglo_marca.split("|")
				arreglo_id = $scope.detalle_prod.imagen[0]['id_imagen']
				$scope.imagenes_productos  = arreglo_id.split("|")
				/*console.log($scope.imagenes_productos);
				console.log(arreglo_marca);*/
				$scope.galeria_marca.length>0 ? $scope.activo_img_soportes = "activo" : $scope.activo_img_soportes = "inactivo"

				$("#idioma").prop('disabled', true);
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar categorías";
				//------------------------------------------------------------------
				//--Si es clonacion cambia los valores del form
				if($scope.clonar==1){
					$scope.titulo_registrar = "Registrar";
					$('#idioma > option[value="2"]').attr('selected', 'selected');
					$("#idioma").prop('disabled', true);
					$scope.detalle_prod.id_idioma = 2
					$scope.detalle_prod.id_idioma.id = 2
					$scope.id_detalle_prod_es =  $scope.id_detalle_prod
					$scope.id_detalle_prod = ""
					$scope.titulo_pagina = "Detalle de Productos";
					$scope.subtitulo_pagina = "Registro(EN)";
					$("#div_descripcion").html("")
					$scope.detalle_prod.descripcion = "..."
					$scope.detalle_prod.titulo = ""
					$scope.titulo_text = "Pulse aquí para ingresas el contenido"
					
					/*$("#precio").attr("disabled")
					$("#cantidad").attr("disabled")
					$("#masonrry").attr("disabled")*/
				}
				//------------------------------------------------------------------
				$scope.cargar_catprod_idioma_edit();
				$scope.cargar_tipoprod_edit();
				$scope.cargar_marca_idioma_edit();
				$scope.cargar_color_idioma_edit();
				$scope.consultar_talla();
				//-----------------------------------------------------
				if($scope.clonar==1){
					setTimeout(function(){
						$scope.valoresOtroIdioma();
					},500)
					$scope.deshabilitarCampos();
				}
				else{
					//-----------------------------------------------------
					//--Select de idioma
					if($scope.detalle_prod.id_idioma){
						setTimeout(function(){
							$('#idioma').val($scope.detalle_prod.id_idioma);
							$('#idioma > option[value="'+$scope.detalle_prod.id_idioma+'"]').attr('selected', 'selected');
						},500);
					}
					//--Select de categoria
					if($scope.detalle_prod.categoria_prod){
						setTimeout(function(){
							$('#categoria_prod').val($scope.detalle_prod.categoria_prod);
							$('#categoria_prod > option[value="'+$scope.detalle_prod.categoria_prod+'"]').attr('selected', 'selected');
						},500);
					}
					//--Select de tipo producto
					if($scope.detalle_prod.tipo_prod){
						setTimeout(function(){
							$('#tipo_prod').val($scope.detalle_prod.tipo_prod);
							$('#tipo_prod > option[value="'+$scope.detalle_prod.tipo_prod+'"]').attr('selected', 'selected');
						},500);
					}
					//--Select de marca
					if($scope.detalle_prod.marca){
						setTimeout(function(){
							$('#marca').val($scope.detalle_prod.marca);
							$('#marca > option[value="'+$scope.detalle_prod.marca+'"]').attr('selected', 'selected');
						},500);
					}
					//--Select de color
					if($scope.detalle_prod.color){
						setTimeout(function(){
							$('#color').val($scope.detalle_prod.color);
							$('#color > option[value="'+$scope.detalle_prod.color+'"]').attr('selected', 'selected');
						},500);
					}

					//--Select de talla
					if($scope.detalle_prod.talla){
						setTimeout(function(){
							$('#talla').val($scope.detalle_prod.talla);
							$('#talla > option[value="'+$scope.detalle_prod.talla+'"]').attr('selected', 'selected');
						},500);
					}
					//-------------------------------------------------------------
					//-Si viene a modificar y es un producto clonado
					if($scope.detalle_prod.clonado!=0){
						//&&($scope.detalle_prod.id_original_clonado)){
						$scope.deshabilitarCampos();
						$scope.inhabilitarImg = true
					}
					//-------------------------------------------------------------
				}
				
				//-----------------------------------------------------
				/*setTimeout(function(){
					$('#idioma > option[value="'+$scope.detalle_prod.id_idioma+'"]').prop('selected', true);

					$('#categoria_prod > option[value="'+$scope.detalle_prod.categoria_prod+'"]').prop('selected', true);

					$('#tipo_prod > option[value="'+$scope.detalle_prod.tipo_prod+'"]').prop('selected', true);

					$('#marca > option[value="'+$scope.detalle_prod.marca+'"]').prop('selected', true);

					$('#color > option[value="'+$scope.detalle_prod.color+'"]').prop('selected', true);

					$('#talla > option[value="'+$scope.detalle_prod.talla+'"]').prop('selected', true);
				},300);*/

			});
		}
		/*
		*	DeshabilitarCampos
		*/
		$scope.deshabilitarCampos = function(){
			$("#marca").prop('disabled', true);
			$("#categoria_prod").prop('disabled', true);
			$("#tipo_prod").prop('disabled', true);
			$("#color").prop('disabled', true);
			$("#talla").prop('disabled', true);
			$("#precio").prop('disabled', true);
			$("#cantidad").prop('disabled', true);
			$("#img_galeria").prop('disabled',true);
		}
		/*
		*	Habilitar campos
		*/
		$scope.habilitarCampos = function(){
			$("#marca").prop('disabled', true);
			$("#categoria_prod").prop('disabled', true);
			$("#tipo_prod").prop('disabled', true);
			$("#color").prop('disabled', true);
			$("#talla").prop('disabled', true);
			$("#precio").prop('disabled', true);
			$("#cantidad").prop('disabled', true);
			$("#img_galeria").prop('disabled',true);
		}
		/*
		*	ValoresOtrodIdioma
		*/
		$scope.valoresOtroIdioma = function(){
			$http.post($scope.base_url+"/detalle_prod/consultarValoresOtroIdioma",
			{
				'categoria': $scope.detalle_prod.categoria_prod,
				'tipo_producto': $scope.detalle_prod.tipo_prod,
				'marca':$scope.detalle_prod.marca,
				'color':$scope.detalle_prod.color,
				'talla':$scope.detalle_prod.talla
			}).success(function(data, estatus, headers, config){
				$scope.valores  = data;
				console.log($scope.valores);
				//--Select de marca
				if($scope.valores["marca"]!=""){
					$scope.detalle_prod.marca = $scope.valores["marca"]

					if($scope.detalle_prod.marca!=""){
						setTimeout(function(){
							$('#marca').val($scope.detalle_prod.marca);
							$('#marca > option[value="'+$scope.detalle_prod.marca+'"]').attr('selected', 'selected');
						},500);
					}
				}	
				//--Select de categoria
				if($scope.valores["categoria"]!=""){
					$scope.detalle_prod.categoria_prod = $scope.valores["categoria"]
					//--Select de categoria}
					if($scope.detalle_prod.categoria_prod!=""){
						setTimeout(function(){
							$('#categoria_prod').val($scope.detalle_prod.categoria_prod);
							$('#categoria_prod > option[value="'+$scope.detalle_prod.categoria_prod+'"]').attr('selected', 'selected');
						},500);
					}
				}
				//--Select de tipo producto
				if($scope.valores ["tipo_producto"]!=""){
					$scope.detalle_prod.tipo_prod = $scope.valores ["tipo_producto"]
					//--Select de tipo producto
					if($scope.detalle_prod.tipo_prod!=""){
						setTimeout(function(){
							$('#tipo_prod').val($scope.detalle_prod.tipo_prod);
							$('#tipo_prod > option[value="'+$scope.detalle_prod.tipo_prod+'"]').attr('selected', 'selected');
						},500);
					}
				}

				//--Select de color
				if($scope.valores ["colores"]!=""){
					$scope.detalle_prod.color = $scope.valores ["colores"]
					//--Select de color
					if($scope.detalle_prod.color!=""){
						setTimeout(function(){
							$('#color').val($scope.detalle_prod.color);
							$('#color > option[value="'+$scope.detalle_prod.color+'"]').attr('selected', 'selected');
						},500);
					}
				}
				//------------------------------------------------------------------
				if($scope.detalle_prod.id_idioma){
					setTimeout(function(){
						$('#idioma').val($scope.detalle_prod.id_idioma);
						$('#idioma > option[value="'+$scope.detalle_prod.id_idioma+'"]').attr('selected', 'selected');
					},500);
				}
				
				//--Select de talla
				if($scope.detalle_prod.talla){
					setTimeout(function(){
						$('#talla').val($scope.detalle_prod.talla);
						$('#talla > option[value="'+$scope.detalle_prod.talla+'"]').attr('selected', 'selected');
					},500);
				}
				//------------------------------------------------------------------
			}).error(function(data,estatus){
				console.log(data);
			});
				
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////

		$scope.modificar_detalleProd = function(){
			$scope.id_marca  = $("#marca").val();//Linea
			$scope.id_categoria_prod = $("#categoria_prod").val();//Genero
			$scope.id_color = $("#color").val();
			$scope.id_talla = $("#talla").val();

			$scope.tipo_producto = "";
			$scope.id_tipo_producto1 = $scope.detalle_prod.tipo_prod;
			$scope.id_tipo_producto = $scope.detalle_prod.tipo_prod.id;

			if ($scope.id_tipo_producto===undefined) {
				$scope.tipo_producto = $scope.id_tipo_producto1;
			}else{
				$scope.tipo_producto = $scope.id_tipo_producto;
			}

			$http.post($scope.base_url+"/detalle_prod/modificar_detalleProd",
			{
				'id' 	     		: $scope.detalle_prod.id,
				'id_idioma'  		: $scope.detalle_prod.id_idioma,
				'categoria_prod' 	: $scope.id_categoria_prod,
				'tipo_prod' 		: $scope.tipo_producto,
				'marca' 			: $scope.id_marca,
				'color' 			: $scope.id_color,
				'talla' 			: $scope.id_talla,
				'precio' 			: $scope.detalle_prod.precio,
				'titulo'     		: $scope.detalle_prod.titulo,
				'descripcion'		: $scope.detalle_prod.descripcion,
				'cantidad'			: $scope.detalle_prod.cantidad,
				'id_imagen' 		: $scope.imagenes_productos,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					//$scope.limpiar_cajas_detProd();
				}else if($scope.mensajes.mensaje == "existe_titulo"){
					mostrar_notificacion("Mensaje","Existe un registro con ese título!","warning");
				}else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.quitarDisabled = function(){
			$("#btn-registrar").prop("disabled",false)
			$("#btn-limpiar").prop("disabled",false)
			$("#marca").prop('disabled', false);
			$("#categoria_prod").prop('disabled', false);
			$("#tipo_prod").prop('disabled', false);
			$("#color").prop('disabled', false);
			$("#talla").prop('disabled', false);
			$("#precio").prop('disabled', false);
			$("#cantidad").prop('disabled', false);
			$("#img_galeria").prop('disabled',false);
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();
		$scope.consultar_talla();
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.id_detalle_prod  = $("#id_detalle_prod").val();
		$scope.clonar = $("#clonar").val();
			if($scope.id_detalle_prod){
				$scope.consultaDetalleProdIndividual();
			}

		//---------------------------------------------------------------
		if((($scope.detalle_prod.id=="")||($scope.detalle_prod.id==undefined))&&($scope.clonar!=1)){
			setTimeout(function(){
				$("#idioma > option[value='']").removeAttr('selected');
				$("#idioma > option[value='1']").attr('selected', 'selected');
			},2000)
			$scope.quitarDisabled();
			$scope.cargar_catprod_idioma(); 
			$scope.cargar_marca_idioma(); 
			$scope.cargar_color_idioma();
			$scope.cargar_tipoprod();
		}
		//---------------------------------------------------------------	
	})
