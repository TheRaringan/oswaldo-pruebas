angular.module("ContentManagerApp")
	.controller("categoria_consulta_prodController", function($scope,$http,$location,serverDataMensajes,categoriasProdFactory,sesionFactory,idiomaFactory){
		$(".li-menu").removeClass("active");
		$("#li_productos").addClass("active");
		$(".a-menu").removeClass("active");
		$("#cat_prod").addClass("active");

		$scope.titulo_pagina = "Consulta de categorías de productos";
		$scope.id_categoria_prod = "";
		$scope.base_url = $("#base_url").val();
		//////////////////////////////////////////////////////////////////////////////
		$scope.categoria_prod  = {
									'id' : '',
									'titulo' : '',
									'descripcion' : '',
									'id_idioma' : '',
									'idioma' : ''
		}
		//////////////////////////////////////////////////////////////////////////////
		$scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
			    "order": [[ 0, "desc" ]]
			});
			//console.log($scope.categoria_prod);
		}
		//////////////////////////////////////////////////////////////////////////////
		$scope.consultar_categorias_prod = function(){
			categoriasProdFactory.asignar_valores("","",$scope.base_url)
			categoriasProdFactory.cargar_categorias_prod(function(data){
				$scope.categoria_prod=data;
				//console.log($scope.categoria_prod);
			});
		}
		//////////////////////////////////////////////////////////////////////////////
		$scope.ver_categoriaprod = function(valor){
			var id = $("#ver"+valor).attr("data");
			$scope.id_categoria_prod = id;
			$("#id_categoria_prod").val($scope.id_categoria_prod)
			let form = document.getElementById('formConsultaCategoriasProd');
			form.action = "./categoriaProdVer";
			form.submit();
		}
		//////////////////////////////////////////////////////////////////////////////
		$scope.eliminar_registro = function(event){
			$scope.id_seleccionado_catprod = []
			$scope.estatus_seleccionado_catprod = []
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data")
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_catprod = arreglo_atributos[0];
			$scope.estatus_seleccionado_catprod = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado_catprod	==2){
				mensaje = "Desea eliminar este registro? ";
			}
			$scope.modificar_estatus("eliminar",mensaje)
		}
		//////////////////////////////////////////////////////////////////////////////
		$scope.modificar_estatus = function(opcion,mensaje){
			 swal({
				  title: 'Esta seguro?',
				  text: mensaje,
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si!',
				  cancelButtonText: 'No',
				}).then((result) => {
					  if (result.value) {
					  	$scope.accion_estatus()
					  }
				})
		}
		//////////////////////////////////////////////////////////////////////////////
		$scope.accion_estatus = function(){
			$http.post($scope.base_url+"Categoria_prod/modificarCatProdEstatus",
			{
				'id' : $scope.id_seleccionado_catprod,
				'estatus' : $scope.estatus_seleccionado_catprod,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					Swal(
					      'Realizado!',
					      'El proceso fue ejecutado.',
					      'success'
				    ).then((result) => {
						  if (result.value) {
							    let form = document.getElementById('formConsultaCategoriasProd');
								form.action = "./consultar_categoriasProd";
								form.submit();
						  }

					});
				}else{
					Swal(
					      'No realizado!',
					      'El proceso no pudo ser ejecutado.',
					      'warning'
				    )
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		//////////////////////////////////////////////////////////////////////////////
		$scope.activar_registro = function(event){
			$scope.id_seleccionado_catprod = []
			$scope.estatus_seleccionado_catprod = []
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_catprod = arreglo_atributos[0];
			$scope.estatus_seleccionado_catprod = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");
			//--
			if ($scope.estatus_seleccionado_catprod==0){
				mensaje = "Desea modificar el estatus de este registro a publicado? ";
				$scope.estatus_seleccionado_catprod=1;
			}else{
				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
				$scope.estatus_seleccionado_catprod=0
			}
			//($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

			//--
			$scope.modificar_estatus("activar_inactivar",mensaje)
		}
		//////////////////////////////////////////////////////////////////////////////
		setTimeout(function(){
				$scope.iniciar_datatable();
		},500);
		$scope.consultar_categorias_prod();
	});
