angular.module('ContentManagerApp')
.controller("nosotrosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,nosotrosFactory){
	//DECLARACIONES
	$(".li-menu").removeClass("active");
    $("#li_empresa").addClass("active");	
    $(".a-menu").removeClass("active");
    $("#nosotros").addClass("active");	
	$scope.titulo_pagina = "Nosotros";
	$scope.subtitulo_pagina = "Registrar Nosotros";
	$scope.activo_img = "inactivo";

	$scope.nosotros = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'somos': '',
						'mision': '',
						'vision':'',
						'estatus' : '',
						'id_imagen' : '',
						'imagen' : ''
	}
	$scope.titulo_reg = "Registrar";
	$scope.titulo_cons = "Consultar";
	$scope.searchNosotros = []
	$scope.borrar_imagen = []
	$scope.currentTab = 'datos_basicos'
	$scope.base_url = $("#base_url").val();
	$scope.opcion = ''
	$scope.seccion = ''
	//------------------------------------------
	$scope.consultar_idioma = function(){
		idiomaFactory.asignar_valores("",$scope.base_url)
		idiomaFactory.cargar_idioma(function(data){
			$scope.idioma=data;
		});
	}
	//------------------------------------------
	$scope.wisi_modal = function(opcion){
		$('#textarea_editor').data("wysihtml5").editor.clear();
		$scope.opcion = opcion
		if ($scope.nosotros!=undefined){
			//----------------
				switch ($scope.opcion) {
					case "1":
						$scope.seccion= "Somos"
						if ($scope.nosotros.somos!="") {
							//$(".textarea_editor").html($scope.nosotros.somos)
							$('iframe').contents().find('.wysihtml5-editor').html($scope.nosotros.somos);
						}
						break;
					case "2":
						$scope.seccion= "Mision"
						if ($scope.nosotros.mision!="") {
							//$(".textarea_editor").html($scope.nosotros.digital_agency)
							$('iframe').contents().find('.wysihtml5-editor').html($scope.nosotros.mision);
						}
						break;
					case "3":
						$scope.seccion= "Vision"
						if ($scope.nosotros.vision!="") {
							//$(".textarea_editor").html($scope.nosotros.digital_agency)
							$('iframe').contents().find('.wysihtml5-editor').html($scope.nosotros.vision);
						}
						break;	
				}
			//----------------
		}
	}

	$scope.agregar_contenido = function(){
		if ($scope.nosotros==undefined){
				$scope.nosotros = {
									'id': '',
									'idioma': '',
									'id_idioma' : '',
									'somos': '',
									'mision': '',
									'vision': '',
									'estatus' : '',
									'id_imagen' : '',
									'imagen' : ''
				}
		}
		switch ($scope.opcion) {
			case "1":
				$scope.nosotros.somos = $(".textarea_editor").val()
				$("#div_somos").html($scope.nosotros.somos)
				break;
			case "2":
				$scope.nosotros.mision = $(".textarea_editor").val()
				$("#div_mision").html($scope.nosotros.mision)
				break;
			case "3":
				$scope.nosotros.vision = $(".textarea_editor").val()
				$("#div_vision").html($scope.nosotros.vision)
				break;	
		}
		$(".textarea_editor").text("")
		$('.textarea_editor').data("wysihtml5").editor.clear();
		$("#cerrarModal").click()
	}
	//------------------------------------------CONSULTA DE IMAGEN SEGUN ID DE CATEGORIA
	$scope.consultar_galeria_img = function(){
		galeriaFactory.asignar_valores('20','',$scope.base_url);//ya que es galeria de imagenes
		galeriaFactory.cargar_galeria_fa(function(data){
			$scope.galery=data;
			console.log($scope.galery);
		});
	}
	//MODAL DE IMG
	$scope.seleccione_img_principal = function(){
		$("#modal_img1").modal("show");
	}
	//PARA SELECCIONAR UNA IMAGEN Y MOSTRARLA EN EL CAMPO DE IMG
	$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];

			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.nosotros.id_imagen = id_imagen
			$scope.nosotros.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
	}
	//LIMPIAR CAJAS---
	$scope.limpiar_cajas_nosotros = function(){
		$scope.nosotros = {
							'id': '',
							'idioma': '',
							'id_idioma' : '',
							'somos': '',
							'mision': '',
							'vision': '',
							'estatus' : '',
							'id_imagen' : '',
							'imagen' : ''
		}
		$("#idioma").val("");
		$("#div_somos").text("Pulse aquí para ingresas el contenido de Somos");
		$("#div_mision").text("Pulse aquí para ingresas el contenido de misión");
		$("#div_mision").text("Pulse aquí para ingresas el contenido de visión");
		$scope.activo_img = "inactivo"
	}

	//--
	$scope.validar_form = function(){
		if($scope.nosotros.idioma==""){
			mostrar_notificacion("Campos no validos","Debe seleccionar un idioma","warning");
			return false;
		}else if ($scope.nosotros.somos==""){
			mostrar_notificacion("Campos no validos","El campo Somos debe tener contenido","warning");
			return false;
		}else if ($scope.nosotros.mision==""){
			mostrar_notificacion("Campos no validos","El campo misión debe tener contenido","warning");
			return false;
		}else if ($scope.nosotros.vision==""){
			mostrar_notificacion("Campos no validos","El campo visión debe tener contenido","warning");
			return false;
		}else if ($scope.nosotros.id_imagen=="") {
			mostrar_notificacion("Campos no validos","Debe seleccionar una imagen","warning");
			return false;
		}else{
			return true;
		}
	}
	//--

	$scope.registrarNosotros = function(){
		if ($scope.validar_form()==true) {
			if (($scope.nosotros.id!=undefined)&&($scope.nosotros.id!="")){
				console.log($scope.nosotros.id);
				$scope.modificar_nosotros();
			}else {
				console.log($scope.nosotros.id);
				$scope.insertar_nosotros();
			}
		}
	}

	$scope.insertar_nosotros=function(){
		$http.post($scope.base_url+"/Nosotros/insertarNosotros",
		{
			'id_idioma' : $scope.nosotros.idioma.id,
			'somos'  : $scope.nosotros.somos,
			'mision' : $scope.nosotros.mision,
			'vision' : $scope.nosotros.vision,
			'id_imagen' : $scope.nosotros.id_imagen,
		}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_nosotros();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una categoría con esa descripción","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
	}

	$scope.modificar_nosotros = function(){
		//console.log($scope.nosotros.idioma);
		$http.post($scope.base_url+"/Nosotros/modificar_nosotros",
		{
			'id' : $scope.nosotros.id,
			'id_idioma' : $scope.nosotros.id_idioma,
			'somos'  : $scope.nosotros.somos,
			'mision' : $scope.nosotros.mision,
			'vision' : $scope.nosotros.vision,
			'id_imagen' : $scope.nosotros.id_imagen,
			'estatus' : $scope.nosotros.estatus,
		}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","La modificación fue realizada de manera exitosa","info");
					//$scope.limpiar_cajas_nosotros();
				}else if($scope.mensajes.mensaje == "no_existe"){
					mostrar_notificacion("Mensaje","No existe una categoría con esa descripción","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
	}

	$scope.consultarNosotrosIndividual = function(){
		nosotrosFactory.asignar_valores("", $scope.id_nosotros, $scope.base_url);
		nosotrosFactory.cargar_nosotros(function(data){
			$scope.nosotros = data[0];
			//console.log($scope.nosotros);
			$("#div_somos").html($scope.nosotros.somos);
			$("#div_mision").html($scope.nosotros.mision);
			$("#div_vision").html($scope.nosotros.vision);

			$scope.borrar_imagen.push($scope.nosotros.id_imagen);
			$scope.activo_img = "activo"
			$scope.nosotros.imagen = $scope.nosotros.ruta

			$scope.titulo_reg = "Modificar";
			$scope.subtitulo_pagina = "Modificar Nosotros";

			setTimeout(function(){
				$('#idioma > option[value="'+$scope.nosotros.id_idioma+'"]').prop('selected', true);
			},300);
			$("#idioma").prop('disabled', true);
		})
	}

		$scope.consultar_idioma();
		$scope.consultar_galeria_img();

		//Consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
		$scope.id_nosotros = $("#id_nosotros").val();
			if($scope.id_nosotros){
				$scope.consultarNosotrosIndividual();
			}
			//---------------------------------------------------------
})
