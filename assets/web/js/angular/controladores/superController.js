angular.module("ZougZougApp")
	.controller("inicioController", function($scope,$http,$window,$compile,$location,direccionFactory,redessocialesFactory,upload,multIdioma){
		//Bloque de metodos
		///---
		$scope.consultar_direcciones_todas = function(){
			offset = 0
			limit = 2
			direccionFactory.asignar_valores(offset,limit,"");
			direccionFactory.cargar_direcciones_todas(function(data){
				console.log(data);
				$scope.direccion_es = data["es"];
				$scope.direccion_en=data["en"];
				//$scope.direccion_f = data["es"];
				//console.log($scope.direccion_f);
				$scope.ruta = $location.path()
				ruta = $scope.ruta
				vec_ruta = ruta.split("/")
				if(($scope.ruta== '/')|| ($scope.ruta== '/inicio')|| ($scope.ruta== '/nosotros') || ($scope.ruta== '/contactanos') || ($scope.ruta== '/servicios')|| ($scope.ruta== '/portafolio')|| ($scope.ruta== '/noticias')|| ($scope.ruta== '/contactanos') || ( vec_ruta[1]=='portafolio')||((vec_ruta[1]=="noticias")&&(vec_ruta[2]=="es")) ){
					$scope.direccion_f = data["es"];
				}
				if(($scope.ruta== '/home')|| ($scope.ruta== '/about')|| ($scope.ruta== '/services')|| ($scope.ruta== '/portfolio')||($scope.ruta== '/contact_us')||($scope.ruta== '/news')||( vec_ruta[1]=='portfolio') ||((vec_ruta[1]=="news")&&(vec_ruta[2]=="en")) ){
					$scope.direccion_f = data["en"];
				}
				console.log($scope.direccion_f);		
			});
		}	
		//---
		
		//----	
		$scope.consultar_redes_sociales = function(){

				redessocialesFactory.cargar_redes(function(data){
					$scope.redes_sociales=data[0];
					//console.log($scope.redes_sociales);
				});
			}
		//-------------------------------------------------------------
		//--Coloca la bandera segun la url: Segun lo que tenga la url se colocan los titulos del menu
		$scope.definir_url = function(){
			$scope.ruta = $location.path()
			vec_ruta = $scope.ruta.split("/")
			if(($scope.ruta== '/inicio')|| ($scope.ruta== '/nosotros') ||($scope.ruta== '/servicios')||($scope.ruta== '/portafolio')|| ( vec_ruta[1]=='/portafolio')||($scope.ruta== '/noticias')||($scope.ruta== '/contactanos')){
				res = multIdioma.cargar_inicio_espain()
			}
			if(($scope.ruta== '/')|| ($scope.ruta== '/home')|| ($scope.ruta== '/about')||($scope.ruta== '/services')||($scope.ruta== '/portfolio') || ( vec_ruta[1]=='/portfolio') ||($scope.ruta == '/news')|| ($scope.ruta== '/contact_us') ){
				res = multIdioma.cargar_inicio_uk()
			}
			$scope.menu = res.titulo
			$scope.btn = res.btn
			$scope.url = res.url
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//----------------------------------------
		$scope.cambio_idioma = function(){
			res = multIdioma.cambiar_idioma()
			$scope.menu = res.titulo
			$scope.btn = res.btn
			$scope.url = res.url
			$scope.idioma_inicio = res.idioma_inicio
			$scope.cambiar_page_idioma()
			//Cuando agregue las direcciones
			/*if($scope.idioma_inicio=="1"){
				$scope.direccion_f = $scope.direccion_es;
			}else{
				$scope.direccion_f = $scope.direccion_en;
			}*/
		}
		//--Para cambiar el path
		
		//--Para cambiar los titulos y el path
		$scope.cambiar_page_idioma = function(){
			$scope.ruta = $location.path()
			switch($scope.ruta){
				case '/':
					if($scope.idioma_inicio==1)
						$location.path("/inicio");
					else
						$location.path("/home");
					break;
				case '/inicio':
					if($scope.idioma_inicio==1)
						$location.path("/inicio");
					else
						$location.path("/home");
					break;
				case '/home':
					if($scope.idioma_inicio==1)
						$location.path("/inicio");
					else
						$location.path("/home");
					break;
				case '/nosotros':
					if($scope.idioma_inicio==1)
						$location.path("/nosotros");
					else
						$location.path("/about");
					break;
				case '/about':
					if($scope.idioma_inicio==1)
						$location.path("/nosotros");
					else
						$location.path("/about");
					break;
				case '/servicios':
					if($scope.idioma_inicio==1)
						$location.path("/servicios");
					else
						$location.path("/services");
					break;
				case '/services':
					if($scope.idioma_inicio==1)
						$location.path("/servicios");
					else
						$location.path("/services");
					break;
				case '/portafolio':
					if($scope.idioma_inicio==1)
						$location.path("/portafolio");
					else
						$location.path("/portfolio");
					break;
				case '/portfolio':
					if($scope.idioma_inicio==1)
						$location.path("/portafolio");
					else
						$location.path("/portfolio");
					break;
				case '/noticias':
					if($scope.idioma_inicio==1)
						$location.path("/noticias");
					else
						$location.path("/news");
					break;
				case '/news':
					if($scope.idioma_inicio==1)
						$location.path("/noticias");
					else
						$location.path("/news");
					break;		
				case '/contactanos':
					if($scope.idioma_inicio==1)
						$location.path("/contactanos");
					else
						$location.path("/contact_us");
					break;
				case '/contact_us':
					if($scope.idioma_inicio==1)
						$location.path("/contactanos");
					else
						$location.path("/contact_us");
					break;		
				default:
					ruta = $scope.ruta;
					vec_ruta = ruta.split("/")
					//--
					//Si es inversiones
					alert(vec_ruta[1]);
					if(vec_ruta[1]=="portfolio"){
						$location.path("/portfolio");
					}else
					if(vec_ruta[1]=="portafolio"){
						$location.path("/portafolio");
					}/*else
					if((vec_ruta[1]=="blog")&&(vec_ruta[2]=="es")){
						$location.path("/blog/en/")
					}else if((vec_ruta[1]=="blog")&&(vec_ruta[2]=="en")){
						$location.path("/blog/es/")
					}*/
					break;
					//--					
			}
			//---
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
			//---
		}
		//---------------------------------------------------------------------------------
		//--Para cambiar los titulos y el path
		$scope.cambiar_page_idioma_inicio = function(){
			$scope.ruta = $location.path()
			//alert($scope.ruta)
			//$scope.titulos_home = multIdioma.cambiar_idioma_home()
			switch($scope.ruta){
				case '/':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					break;
				case '/inicio':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					break;
				case '/home':
					$("#idioma").removeClass("esta_espaniol").addClass("esta_ingles");
					$scope.idioma_inicio = 2
					break;
				case '/nosotros':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					break;
				case '/about':
					$("#idioma").removeClass("esta_espaniol").addClass("esta_ingles");
					$scope.idioma_inicio = 2
					break;
				case '/contactanos':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					break;
				case '/contact_us':
					$("#idioma").removeClass("esta_espaniol").addClass("esta_ingles");
					$scope.idioma_inicio = 2
					break;
				case '/blog/es/':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					//alert("es")
					break;
				case '/blog/en/':
					$("#idioma").removeClass("esta_espaniol").addClass("esta_ingles");
					$scope.idioma_inicio = 2
					//alert("en")
					break;	
			}
			multIdioma.definir_idioma();
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//-------------------------------------------------------------	
		//Bloque de llamados	
		$scope.consultar_direcciones_todas();
		/*setTimeout(function(){
		  $scope.recorrer_direcciones();
		}, 4000)*/
		$scope.definir_url();
	})
	
