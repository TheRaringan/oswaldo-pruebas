angular.module("ZougZougApp")
	.controller("inicioController", function($scope,$http,$window,$compile,$location,direccionFactory,footerFactory,upload,multIdioma){
		//Bloque de metodos
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		///---
		$scope.inicio = function(id, nombre_apellido, correo, estatus)
		{
			$scope.inicio = {
				'id': id,
				'nombre_apellido':nombre_apellido,
				'correo':correo,
				'estatus':estatus,
	
			}
			$scope.login = true;

			
		/* $scope.inicio.nombre_apellido = nombre_apellido;
		$scope.inicio.id_correo = id_correo;  */
	
		console.log($scope.login);
		};
		 
		$scope.consultar_direcciones_todas = function(){
			offset = 0
			limit = 2
			direccionFactory.asignar_valores(offset,limit,"");
			direccionFactory.cargar_direcciones_todas(function(data){
				$scope.direccion_es = data["es"];
				$scope.direccion_en=data["en"];
				//$scope.direccion_f = data["es"];
				//console.log($scope.direccion_f);
				$scope.ruta = $location.path()
				ruta = $scope.ruta
				vec_ruta = ruta.split("/")
				if(($scope.ruta== '/')|| ($scope.ruta== '/inicio')|| ($scope.ruta== '/nosotros') || ($scope.ruta== '/contactanos') || ($scope.ruta== '/productos')|| ($scope.ruta== '/portafolio')|| ($scope.ruta== '/noticias')|| ($scope.ruta== '/contactanos') || ( vec_ruta[1]=='portafolio')||((vec_ruta[1]=="noticias")&&(vec_ruta[2]=="es")) ){
					$scope.direccion_f = data["es"];
				}
				if(($scope.ruta== '/home')|| ($scope.ruta== '/about')|| ($scope.ruta== '/products')|| ($scope.ruta== '/portfolio')||($scope.ruta== '/contact_us')||($scope.ruta== '/news')||( vec_ruta[1]=='portfolio') ||((vec_ruta[1]=="news")&&(vec_ruta[2]=="en")) ){
					$scope.direccion_f = data["en"];
				}
				console.log($scope.direccion_f);
			});
		}
		//----
		$scope.consultar_redes = function(){
			footerFactory.asignar_valores($scope.idioma,$scope.base_url);
			footerFactory.cargar_redes(function(data){
				$scope.redes=data;
				//console.log($scope.redes);
			});
		}
		$scope.consultar_footer = function(){
			footerFactory.asignar_valores($scope.idioma,$scope.base_url);
			footerFactory.cargar_footer(function(data){
				$scope.footer=data[0];
				//console.log($scope.footer);
			});
		}

		//-------------------------------------------------------------
		//--Coloca la bandera segun la url: Segun lo que tenga la url se colocan los titulos del menu
		$scope.definir_url = function(){
			if($scope.idioma== '1'){
				res = multIdioma.cargar_inicio_espain($scope.base_url)
			}else{
				res = multIdioma.cargar_inicio_uk($scope.base_url)
			}
			$scope.menu = res.titulo
			$scope.btn = res.btn
			$scope.url = res.url
			//console.log($scope.url)
		}
		//----------------------------------------
		$scope.cambio_idioma = function(){
			res = multIdioma.cambiar_idioma($scope.base_url)
			$scope.menu = res.titulo
			$scope.btn = res.btn
			$scope.url = res.url
			$scope.idioma_inicio = res.idioma_inicio
			$scope.cambiar_page_idioma()
			//Cuando agregue las direcciones
			/*if($scope.idioma_inicio=="1"){
				$scope.direccion_f = $scope.direccion_es;
			}else{
				$scope.direccion_f = $scope.direccion_en;
			}*/
		}
		//--Para cambiar el path

		//--Para cambiar los titulos y el path
		$scope.cambiar_page_idioma = function(){
			$scope.ruta = window.location.href
			var cadena = window.location.href
			$scope.ruta = cadena.replace($scope.base_url,"/")
			switch($scope.ruta){
				
				case '/':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url;
					else
						window.location.href=$scope.base_url+"home";
					break;

				case '/inicio':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url;
					else
						window.location.href=$scope.base_url+"home";
					break;

				case '/home':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url;
					else
						window.location.href=$scope.base_url+"home";
					break;

				case '/nosotros':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url+"nosotros";
					else
						window.location.href=$scope.base_url+"about";
					break;

				case '/about':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url+"nosotros";
					else
						window.location.href=$scope.base_url+"about";
					break;

				case '/productos':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url+"productos";
					else
						window.location.href=$scope.base_url+"products";
					break;

				case '/products':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url+"productos";
					else
						window.location.href=$scope.base_url+"products";
					break;

				case '/portafolio':
					if($scope.idioma_inicio==1)
						$location.path("/portafolio");
					else
						$location.path("/portfolio");
					break;

				case '/portfolio':
					if($scope.idioma_inicio==1)
						$location.path("/portafolio");
					else
						$location.path("/portfolio");
					break;

				case '/noticias':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url+"noticias";
					else
						window.location.href=$scope.base_url+"news";
					break;

				case '/news':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url+"noticias";
					else
						window.location.href=$scope.base_url+"news";
					break;

				case '/contactanos':
					if($scope.idioma_inicio==1)
						$location.path("/contactanos");
					else
						$location.path("/contact_us");
					break;

				case '/contact_us':
					if($scope.idioma_inicio==1)
						$location.path("/contactanos");
					else
						$location.path("/contact_us");
					break;

				case '/carrito':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url+"carrito";
					else
						window.location.href=$scope.base_url+"cart";
					break;

				case '/cart':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url+"carrito";
					else
						window.location.href=$scope.base_url+"cart";
					break;	
				case '/orden':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url+"orden";
					else
						window.location.href=$scope.base_url+"order";
					break;
				case '/order':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url+"orden";
					else
						window.location.href=$scope.base_url+"order";
					break;	
						
				case '/orden_usuario':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url+"orden_usuario";
					else
						window.location.href=$scope.base_url+"order_us";
					break;
				case '/order_us':
					if($scope.idioma_inicio==1)
						window.location.href=$scope.base_url+"orden_usuario";
					else
						window.location.href=$scope.base_url+"order_us";
					break;	
				default:
					ruta = $scope.ruta;
					vec_ruta = ruta.split("/")
					//--
					//Si es inversiones
					//---------------------------------------------
					if(vec_ruta[1]=="products")
						window.location.href=$scope.base_url+"productos";
					else if(vec_ruta[1]=="productos")
						window.location.href=$scope.base_url+"products";
					//---------------------------------------------
					if(vec_ruta[1]=="noticias"){
						window.location.href=$scope.base_url+"news";
					}else
					if(vec_ruta[1]=="news"){
						window.location.href=$scope.base_url+"noticias";
					}
					if(vec_ruta[1]=="checkout_step")
						window.location.href=$scope.base_url+"procesar_compra/"+vec_ruta[2];
					else if(vec_ruta[1]=="procesar_compra")
						window.location.href=$scope.base_url+"checkout_step/"+vec_ruta[2];
					break;	
					//--
			}
			//---
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
			//---
		}
		//---------------------------------------------------------------------------------
		//--Para cambiar los titulos y el path
		/*$scope.cambiar_page_idioma_inicio = function(){
			$scope.ruta = $location.path()
			//alert($scope.ruta)
			//$scope.titulos_home = multIdioma.cambiar_idioma_home()
			switch($scope.ruta){
				case '/':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					break;
				case '/inicio':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					break;
				case '/home':
					$("#idioma").removeClass("esta_espaniol").addClass("esta_ingles");
					$scope.idioma_inicio = 2
					break;
				case '/nosotros':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					break;
				case '/about':
					$("#idioma").removeClass("esta_espaniol").addClass("esta_ingles");
					$scope.idioma_inicio = 2
					break;
				case '/contactanos':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					break;
				case '/contact_us':
					$("#idioma").removeClass("esta_espaniol").addClass("esta_ingles");
					$scope.idioma_inicio = 2
					break;
				case '/blog/es/':
					$("#idioma").removeClass("esta_ingles").addClass("esta_espaniol");
					$scope.idioma_inicio = 1
					//alert("es")
					break;
				case '/blog/en/':
					$("#idioma").removeClass("esta_espaniol").addClass("esta_ingles");
					$scope.idioma_inicio = 2
					//alert("en")
					break;
			}
			multIdioma.definir_idioma($scope.base_url);
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}*/
		//-------------------------------------------------------------
		//Bloque de llamados
		//$scope.consultar_direcciones();
		$scope.consultar_footer();
		$scope.consultar_redes();
		$scope.definir_url();
		limpiarHash();
	});
