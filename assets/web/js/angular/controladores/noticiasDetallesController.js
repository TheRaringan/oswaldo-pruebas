angular.module("ZougZougApp")
	.controller("noticiasDetallesController", function($scope,$http,$location,serverDataMensajes,noticiaFactory,multIdioma){
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		console.log($scope.idioma);
		$(".menu_web").removeClass("active")
		$("#menu4").addClass("active")
		//------------------------------------------------------------
		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
			console.log($scope.btn);
		}
		//-----------------------------------------------------------
		$scope.consultar_noticias_slug = function(){
			$scope.definir_url_home();
			$scope.slug = $("#slug_noticia").text();
			noticiaFactory.asignar_valores($scope.idioma,$scope.base_url,$scope.slug)
			noticiaFactory.cargar_noticia_slug(function(data){
				$scope.noticia=data[0];
				console.log($scope.noticia);
				$scope.consultar_subnoticias($scope.noticia.id,3,0);
			});
		}
		//-----------------------------------------------------------
		$scope.consultar_subnoticias = function(id,limit, start){
			noticiaFactory.asignar_valores($scope.idioma,$scope.base_url,id,limit,start)
			noticiaFactory.cargar_sub_noticias(function(data){
				$scope.sub_noticias=data;
				console.log($scope.sub_noticias);
			});
		}
		//-----------------------------------------------------------
		$scope.definir_url_home();
		$scope.consultar_noticias_slug();
		//-----------------------------------------------------------
	});
