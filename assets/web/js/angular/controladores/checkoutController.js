angular.module("ZougZougApp")
	.controller("checkoutController", function($scope,$http,$sce,$compile,$location,funcionaFactory,serviciosFactory,multIdioma,carritoFactory){
		//Cuerpo declaraciones
		$scope.productos = {}
		$scope.base_url = $("#base_url").val();
		$scope.idioma = $("#id_idioma").val();
		$scope.numero_orden = $("#idOrdenCompra").html();
		$scope.correoUsuario = $("#correoUsuario").html();
		$(".menu_web").removeClass("active")
		$("#menu3").addClass("active")

		$(".primera").removeClass("side-nav")
		$("#menu3").addClass("active")

		$( "#target" ).click(function() {
		  alert( "Handler for  called." );
		});

		$scope.definir_url_home = function(){
			$scope.titulos_home = multIdioma.cambiar_idioma_home()
		}
		//----
		$scope.backpageCart = function(){
			var idioma = $scope.idioma
			if(idioma=="1")
				var url_carrito = $scope.base_url+"carrito";
			else
				var url_carrito = $scope.base_url+"cart";
			$("#formOrderProducto").attr("action",url_carrito)
			$("#formOrderProducto").attr("method","post");
			$("#formOrderProducto").submit()
		}
		//-----
		$scope.definir_url_home();
		//----
	});
