angular.module("GrupoMedicoApp",["ui.bootstrap","ngRoute","ngSanitize"])
.config(function($routeProvider){
	$routeProvider
		.when("/",{
			controller: "MainController",
			templateUrl: "site_media/templates/inicio.html.gz"
		})
});

$(window).load(function() {
    $('#preloader').fadeOut('slow');
    $('body').css({'overflow':'visible'});
})
