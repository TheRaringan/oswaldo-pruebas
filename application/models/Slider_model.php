<?php
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Slider_model extends CI_Model {

		public function iniciar_sesion($login,$clave){
			$this->db->where('login',$login);
			$this->db->where('clave',$clave);
			$this->db->where('estatus','1');
			$this->db->select('*');
			$this->db->from(' usuarios u');
			return $this->db->count_all_results();
		}

		public function guardarSlider($data){
			//print_r ($data);die;
			if($this->db->insert("slider",$data)){
				return true;
			}else{
				return false;
			}
		}

		public function consultarSlider($data){
			if($data["id_slider"]!=""){
				$this->db->where('a.id', $data["id_slider"]);
			}
			$this->db->order_by('a.id_idioma','ASC');
			$this->db->order_by('a.orden','ASC');
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
			$this->db->from('slider a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
	        $this->db->join('galeria c', 'c.id = a.id_imagen');
			$res = $this->db->get();
			//print_r($res);die;
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultarOrden($data){
			if($data["id_idioma"]!=""){
				$this->db->where('a.id_idioma', $data["id_idioma"]);
			}
			$this->db->order_by('a.orden','ASC');
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.orden');
			$this->db->from('slider a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
			$res = $this->db->get();
			//var_dump($res->num_rows());die('');
			if($res->num_rows()>0){
				return $res->result();
			}else{
				return false;
			}
		}


		public function modificarSlider($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->update("slider", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}

		public function consultarExiste($id){
			if($id!=""){
				$this->db->where('a.id', $id);
			}
			$this->db->select('a.*');
			$this->db->from('slider a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function posicionar_modulos($posicionar){
			//---
			if($posicionar['tipo'] == 'insert'){

	            $this->db->where('orden >= ' . $posicionar['posicion']);

	            $this->db->where('id_idioma = ' . $posicionar['id_idioma']);

	            $resultados = $this->db->get("slider");

       			//print_r($this->db->last_query());die;

	            if($resultados->num_rows() > 0){

	                foreach ($resultados->result() as $row){

	                    $datos=array(
	                        'orden' => $row->orden + 1,
	                    );

	                    $this->db->where('id', $row->id);

	                    $this->db->update("slider",$datos);
	                }

	            }
	        }else if($posicionar['tipo'] == 'update'){

	            if($posicionar['final'] > $posicionar['inicial']){

	                $this->db->where('orden > ' . $posicionar['inicial'] . ' AND orden <= ' . $posicionar['final']);

       	            $this->db->where('id_idioma = ' . $posicionar['id_idioma']);

	                $resultados = $this->db->get("slider");
	                if($resultados){
		                if($resultados->num_rows() > 0){
		                    foreach ($resultados->result() as $row){
		                        $datos=array(
		                            'orden' => $row->orden - 1,
		                        );
		                        $this->db->where('id', $row->id);
		                        $this->db->update("slider", $datos);
		                    }
		                }
		            }

	            }else if($posicionar['final'] < $posicionar['inicial']){

		                $this->db->where('orden >= ' . $posicionar['final'] . ' AND orden < ' . $posicionar['inicial']);

		                $this->db->where('id_idioma = ' . $posicionar['id_idioma']);

		                $resultados = $this->db->get("slider");
		                if($resultados){
		                	if($resultados->num_rows() > 0){
			                    foreach ($resultados->result() as $row){
			                    	$datos=array(
			                            'orden' => $row->orden + 1,
			                        );
			                        $this->db->where('id', $row->id);
			                        $this->db->update("slider", $datos);
			                    }
			                }
		                }

			    }
	        //---
			}
		//--
		}
}

?>
