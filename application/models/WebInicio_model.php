<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebInicio_model extends CI_Model{

	public function consultarSlider($data){
		$this->db->where('a.id_idioma', $data["id_idioma"]);
		$this->db->order_by('a.id');
        $this->db->where('a.estatus',1);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
		$this->db->from('slider a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        $this->db->join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db->get();

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarNosotros($data){
		$this->db->where('a.id_idioma', $data["id_idioma"]);
		$this->db->order_by('a.id');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma,  c.ruta as ruta, c.id as id_imagen');
		$this->db->from('empresa_nosotros a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$this->db->join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db->get();

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function guardarContactos($data){
		if($this->db->insert("contactos", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function consultarRedes($data){
		$this->db->order_by('a.id');
		$this->db->select('a.id, a.url_red, c.descripcion');
		$this->db->from('red_social a');
		$this->db->join('tipo_red c', 'c.id = a.id_tipo_red');

		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarFooter($data){
		$this->db->where('a.id_idioma', $data["id_idioma"]);
		$this->db->order_by('a.id');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.id, a.correo, a.descripcion');
		$this->db->from('footer a');

		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarproductos($datos){
		$this->db->order_by('a.id');
        $this->db->where('a.estatus!=',2);
        $this->db->where('a.id_idioma',$datos["id_idioma"]);
		$this->db->select('a.id');
		$this->db->from('detalle_productos a');
		$this->db->limit(6);
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarimg($id){
		if($id!=""){
			$this->db->where('a.id_det_prod', $id);
		}
		$this->db->select('a.id_det_prod, GROUP_CONCAT(DISTINCT a.id_imagen ORDER BY a.id ASC SEPARATOR ",") as imagen',FALSE);
		$this->db->from('detalle_productos_imag a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarimg_sola($id_final,$id_producto){
		if(($id_final!="")&&($id_final!="0")){
			$this->db->where('a.id', $id_final);
		}
		if($id_producto!=""){
			$this->db->where('c.id', $id_producto);
		}
		$x = $this->db->select('a.ruta, c.titulo, c.precio, c.id, c.slug');
		$this->db->from('galeria a');
		$this->db->join('detalle_productos_imag b', 'b.id_imagen = a.id');
		$this->db->join('detalle_productos c', 'c.id = b.id_det_prod');
		$res = $this->db->get();
		//var_dump($res->result());die('');
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarNoticiasFiltros($datos){
		/*if($data["id_noticias"]!=""){
			$this->db->where('a.id', $data["id_noticias"]);
		}*/
		$this->db->order_by('a.id');
        $this->db->where('a.estatus!=',2);
        $this->db->where('a.id_idioma',$datos["id_idioma"]);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen,d.login as usuario');
		$this->db->from('seccion_noticias a');
		//$this->db->limit($data["limit"],$data["offset"]);
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        $this->db->join('galeria c', 'c.id = a.id_imagen');
        $this->db->join('usuarios d', 'd.id = a.id_usuario');
		$res = $this->db->get();
		//print_r($res);die;
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	Consultar carrito segun usuario 
	*/
	public function consultarCarrito($datos){
		//var_dump($datos);die;
		if($datos["id_usuario"]!=""){
			$this->db->where('c.id_usuario', $datos["id_usuario"]);
		}
		$this->db->where('b.estatus !=',"2");
		$this->db->where('a.id_idioma', $datos["id_idioma"]);
		$this->db->order_by('a.id','ASC');
		$this->db->limit('2');
		$this->db->select('a.*,b.cantidad,b.monto,b.monto_total, b.id as id_producto_carrito,b.id_carrito,b.id_producto');
		$this->db->from('detalle_productos a');
		
		//Para español
		if($datos["id_idioma"]==1){
			$this->db->join('carrito_productos b', 'b.id_producto = a.id');
		}else{
		//Para ingles
			$this->db->join('carrito_productos b', 'b.id_producto = a.id_original_clonado');
		}
		$this->db->join('carrito_encabezado c', 'c.id = b.id_carrito');

		$res = $this->db->get();

		//print_r($this->db->last_query());die("x");

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/***/
}
