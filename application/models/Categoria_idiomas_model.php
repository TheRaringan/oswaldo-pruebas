<?php
	if (!defined('BASEPATH')) exit ('No direct script access allowed');

	class Categoria_idiomas_model extends CI_Model{


		public function guardar($data){
			//print_r ($data);die;
			if($this->db->insert("categorias_productos_idiomas",$data)){
				return true;
			}else{
				return false;
			}
		}

		public function consultarColoress($data){
			if($data["id_color"]!=""){
				$this->db->where('a.id', $data["id_color"]);
			}
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
			//$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
			$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma');
			$this->db->from('colores a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
	        //$this->db->join('galeria c', 'c.id = a.id_imagen');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultarExiste($es,$en){
			
			$this->db->where('a.id_categoria_es', $es);
			$this->db->where('a.id_categoria_en', $en);
			$this->db->select('a.*');
			$this->db->from('categorias_productos_idiomas a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}


		public function modificarEstatus($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->update("categorias_productos_idiomas", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		public function eliminarEstatus($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->delete("categorias_productos_idiomas")){
	        	return true;
	        }else{
	        	return false;
	        }
		}

	///////////////////	Consulta para los selects dependientes /////////////////
		// Colores
		public function consultarColores_es($data){
			$this->db->where('a.id_idioma=',1);
			$this->db->order_by('a.id');
			$this->db->select('a.id,a.descripcion');
			$this->db->from('colores a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
			
		}
		public function consultarColores_en($data){
			$this->db->where('a.id_idioma=',2);
			$this->db->order_by('a.id');
			$this->db->select('a.id,a.descripcion');
			$this->db->from('colores a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
			
		}

		// Marcas
		public function consultarMarcas_es($data){
			$this->db->where('a.id_idioma=',1);
			$this->db->order_by('a.id');
			$this->db->select('a.id,a.descripcion');
			$this->db->from('marcas a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
			
		}
		public function consultarMarcas_en($data){
			$this->db->where('a.id_idioma=',2);
			$this->db->order_by('a.id');
			$this->db->select('a.id,a.descripcion');
			$this->db->from('marcas a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
			
		}
		public function consultarCategoria_es($data){
			$this->db->order_by('a.id');
			$this->db->where('a.id_idioma=',1);
			$this->db->select('a.id,a.descripcion');
			$this->db->from('categoria_producto a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
			
		}
		public function consultarCategoria_en($data){
			$this->db->order_by('a.id');
			$this->db->where('a.id_idioma=',2);
			$this->db->select('a.id,a.descripcion');
			$this->db->from('categoria_producto a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
			
		}
		public function consultarTipos_es($data){
			$this->db->order_by('a.id');
			$this->db->where('a.id_idioma=',1);
			$this->db->select('a.id,a.descripcion');
			$this->db->from('tipo_producto a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
			
		}
		public function consultarTipos_en($data){
			$this->db->order_by('a.id');
			$this->db->where('a.id_idioma=',2);
			$this->db->select('a.id,a.descripcion');
			$this->db->from('tipo_producto a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
			
		}
		//////////// Fin Consulta para los selects dependientes /////


		////////////  Consulta para la Tabla /////
				// Colores

				public function consultaColores($data){

					$this->db->order_by('a.id');
					$this->db->where('a.nombre_tabla', 'Colores');
					$this->db->select('a.*, b.id as id_categoria_es, b.descripcion as descripcion_es, c.id as id_categoria_en, c.descripcion as descripcion_en');
					$this->db->from('categorias_productos_idiomas a');
					$this->db->join('colores b', 'b.id = a.id_categoria_es');
					$this->db->join('colores c', 'c.id = a.id_categoria_en');
					//$this->db->join('galeria c', 'c.id = a.id_imagen');
					$res = $this->db->get();
		
					if($res){
						return $res->result();
					}else{
						return false;
					}
				}
				// Marcas
				public function consultaMarcas($data){
					$this->db->order_by('a.id');
					$this->db->where('a.nombre_tabla', 'Marcas');
					$this->db->select('a.*, b.id as id_categoria_es, b.descripcion as descripcion_es, c.id as id_categoria_en, c.descripcion as descripcion_en');
					$this->db->from('categorias_productos_idiomas a');
					$this->db->join('marcas b', 'b.id = a.id_categoria_es');
					$this->db->join('marcas c', 'c.id = a.id_categoria_en');
					//$this->db->join('galeria c', 'c.id = a.id_imagen');
					$res = $this->db->get();
		
					if($res){
						return $res->result();
					}else{
						return false;
					}
				}

				public function consultaCategoria($data){
					$this->db->order_by('a.id');
					$this->db->where('a.nombre_tabla', 'Categoría de productos');
					$this->db->select('a.*, b.id as id_categoria_es, b.descripcion as descripcion_es, c.id as id_categoria_en, c.descripcion as descripcion_en');
					$this->db->from('categorias_productos_idiomas a');
					$this->db->join('categoria_producto b', 'b.id = a.id_categoria_es');
					$this->db->join('categoria_producto c', 'c.id = a.id_categoria_en');
					//$this->db->join('galeria c', 'c.id = a.id_imagen');
					$res = $this->db->get();
				
					if($res){
						return $res->result();
					}else{
						return false;
					}
				}

				public function consultaTipos($data){
					$this->db->order_by('a.id');
					$this->db->where('a.nombre_tabla', 'Tipo Producto');
					$this->db->select('a.*, b.id as id_categoria_es, b.descripcion as descripcion_es, c.id as id_categoria_en, c.descripcion as descripcion_en');
					$this->db->from('categorias_productos_idiomas a');
					$this->db->join('tipo_producto b', 'b.id = a.id_categoria_es');
					$this->db->join('tipo_producto c', 'c.id = a.id_categoria_en');
					//$this->db->join('galeria c', 'c.id = a.id_imagen');
					$res = $this->db->get();
		
					if($res){
						return $res->result();
					}else{
						return false;
					}
				}
	}

?>
