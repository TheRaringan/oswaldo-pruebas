<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebCart_model extends CI_Model{
		/*
		*	Consultar info del carrito de compra para x usuario
		*/
		public function consultarCarrito($datos = ""){
			if($datos["id_usuario"]!=""){
				$this->db->where('a.id_usuario', $datos["id_usuario"]);
			}
			$this->db->where('b.estatus !=',"2");
			$this->db->where('a.id_idioma', $datos["id_idioma"]);
			$this->db->order_by('a.id','DESC');
			$this->db->select('a.*,b.cantidad,b.monto,b.monto_total, b.id as id_producto_carrito,b.id_carrito');
			$this->db->from('detalle_productos a');
			//$this->db->join('carrito_productos b', 'b.id_producto = a.id');
			
			//Para español
			if($datos["id_idioma"]==1){
				$this->db->join('carrito_productos b', 'b.id_producto = a.id');
			}else{
			//Para ingles
				$this->db->join('carrito_productos b', 'b.id_producto = a.id_original_clonado');
			}

			$res = $this->db->get();

			//print_r($this->db->last_query());die("x");

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		/*
		*
		*/
		public function eliminarProducto($id,$data){
			$this->db->where('id', $id);
			if($this->db->update("carrito_productos", $data)){
				return true;
			}else{
				return false;
			}
		}
		/*
		*	Modificar cantidad de productos
		*/
		public function modificarCantidad($id,$data){
			$this->db->where('id', $id);
			if($this->db->update("carrito_productos", $data)){
				return true;
			}else{
				return false;
			}
		}
		/*
		*	consultarimgdetalleProd
		*/
		public function consultarimgdetalleProd($id){
			if($id!=""){
				$this->db->where('g.id_det_prod', $id);
			}
			$this->db->order_by('a.id','DESC');
			$this->db->select('g.id_det_prod,a.id, a.ruta');
			$this->db->from('galeria a');
			$this->db->join('detalle_productos_imag g', 'g.id_imagen = a.id');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		/***/
		public function consultarDatosCarrito($id){
			$this->db->where('a.id',$id);
			$this->db->select('a.*');
			$this->db->from('carrito_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		/*
		*	Cantidad de productos
		*/
		public function cantidad_productos($id,$cantidad){
			$this->db->where('a.id_det_prod',$id);
			$this->db->select('*');
			$this->db->from('cantidad_producto a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
}