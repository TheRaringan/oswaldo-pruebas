<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class RedesSociales_model extends CI_Model{
	/*
	*	Consultar redes url
	*/
	public function consultar_redes_url($data){
		$this->db->order_by('a.id');
        $this->db->where('a.id_tipo_red',$data["id_tipo_red"]);
		$this->db->select('a.id,a.url_red,b.descripcion');
		$this->db->from('red_social a');
		$this->db->join('tipo_red b', 'b.id = a.id_tipo_red');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
		
	}
	/*
	*	Consultar Redes
	*/
	public function consultar_redes($data){
		$this->db->order_by('a.descripcion','DESC');
		$this->db->select('a.id,a.descripcion');
		$this->db->from('tipo_red a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	Registrar redes
	*/
	public function guardarRedes($data){
		if($this->db->insert("red_social", $data)){
			return true;
		}else{
			return false;
		}
	}
	/*
	*	Modificar redes
	*/
	public function modificarRedes($data){
		$this->db->where('a.id', $data["id"]);
		$this->db->update('red_social a', $data);
		return true;
	}
}