<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebNosotros_model extends CI_Model{
	public function consultarNosotros($data){
		$this->db->where('a.id_idioma', $data["id_idioma"]);
		$this->db->order_by('a.id');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma,  c.ruta as ruta, c.id as id_imagen');
		$this->db->from('empresa_nosotros a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$this->db->join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db->get();

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
}
