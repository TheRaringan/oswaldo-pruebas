<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebProductos_model extends CI_Model{
	public function consultarproductos($datos){
		$this->db->order_by('a.id', 'DESC');
        $this->db->where('a.estatus!=',2);
        $this->db->where('a.id_idioma',$datos["id_idioma"]);
		//$this->db->select('a.id, a.precio, a.titulo, a.slug, c.cantidad');
		$this->db->select('a.id, a.precio, a.titulo, a.slug');
		$this->db->from('detalle_productos a');
		$this->db->limit('6');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		//$this->db->join('cantidad_producto c', 'c.id_det_prod = a.id');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarimg($id){
		if($id!=""){
			$this->db->where('a.id_det_prod', $id);
		}
		$this->db->select('a.id_det_prod, GROUP_CONCAT(DISTINCT a.id_imagen ORDER BY a.id ASC SEPARATOR ",") as imagen',FALSE);
		$this->db->from('detalle_productos_imag a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}



	public function consultarimg_sola($id_final,$id_producto){
		if($id_final!=""){
			$this->db->where('a.id', $id_final);
		}
		if($id_producto!=""){
			$this->db->where('c.id', $id_producto);
		}
		$x = $this->db->select('a.ruta, c.titulo, c.precio, c.id');
		$this->db->from('galeria a');
		$this->db->join('detalle_productos_imag b', 'b.id_imagen = a.id');
		$this->db->join('detalle_productos c', 'c.id = b.id_det_prod');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarmasProductos($datos){
		//print_r($datos);die;
		$this->db->order_by('a.id desc');

		if($datos["start"]==''){
			$datos["start"] = 0;
		}

		if($datos["limit"]!=''){
	       $this->db->limit($datos["limit"], $datos["start"]);
	    }
		$this->db->order_by('a.id', 'DESC');

		$this->db->where('a.id!=',$datos["id_producto"]);
		$this->db->where('a.id<',$datos["id_producto"]);
		//$this->db->where('c.id_imagen!=',$datos["id_imag"]);
        $this->db->where('a.estatus!=',2);
        $this->db->where('a.id_idioma',$datos["id_idioma"]);
		$this->db->select('a.id,a.precio, a.titulo');
		$this->db->from('detalle_productos a');
		//$this->db->limit($data["limit"],$data["offset"]);
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        // $this->db->join('detalle_productos_imag c', 'c.	id_det_prod = a.id');
		// $this->db->join('galeria g', 'g.id = c.id_imagen');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultar_linea($datos){
		if($datos['id_idioma']!=""){
			$this->db->where('a.id_idioma', $datos['id_idioma']);
		}
		$this->db->order_by('a.id', 'DESC');
		$this->db->select('a.id, a.titulo');
		$this->db->from('marcas a');;
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultar_generos($datos){
		if($datos['id_idioma']!=""){
			$this->db->where('a.id_idioma', $datos['id_idioma']);
		}
		$this->db->order_by('a.id', 'DESC');
		$this->db->select('a.id, a.titulo');
		$this->db->from('categoria_producto a');;
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultar_color($datos){
		if($datos['id_idioma']!=""){
			$this->db->where('a.id_idioma', $datos['id_idioma']);
		}
		$this->db->order_by('a.id', 'DESC');
		$this->db->select('a.id, a.descripcion');
		$this->db->from('colores a');;
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultar_talla($datos){
		$this->db->order_by('a.id', 'DESC');
		$this->db->select('a.id, a.descripcion');
		$this->db->from('tallas a');;
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	//
	public function consultar_img_linea($datos){
		if($datos['id_idioma']!=""){
			$this->db->where('a.id_idioma', $datos['id_idioma']);
		}
		$this->db->order_by('a.id', 'DESC');
		$this->db->where('a.id_marca', $datos['id_linea']);
		$this->db->select('a.id, a.titulo, a.precio, a.slug');
		$this->db->from('detalle_productos a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultar_img_genero($datos){
		if($datos['id_idioma']!=""){
			$this->db->where('a.id_idioma', $datos['id_idioma']);
		}
		$this->db->order_by('a.id', 'DESC');
		$this->db->where('a.id_categoria_prod', $datos['id_genero']);
		$this->db->select('a.id, a.titulo, a.precio, a.slug');
		$this->db->from('detalle_productos a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultar_img_precio($datos){
		//print_r($datos);die;
		if($datos['id_idioma']!=""){
			$this->db->where('a.id_idioma', $datos['id_idioma']);
		}
		$this->db->order_by('a.id', 'DESC');
		$this->db->where('a.precio >=', $datos['id_precio_uno']);
		$this->db->where('a.precio <=', $datos['id_precio_dos']);
		$this->db->select('a.id, a.titulo, a.precio, a.slug');
		$this->db->from('detalle_productos a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	//
	public function consultar_img_color($datos){
		if($datos['id_idioma']!=""){
			$this->db->where('a.id_idioma', $datos['id_idioma']);
		}
		$this->db->order_by('a.id', 'DESC');
		$this->db->where('a.id_color', $datos['id_color']);
		$this->db->select('a.id, a.titulo, a.precio, a.slug');
		$this->db->from('detalle_productos a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	//
	public function consultar_img_talla($datos){
		if($datos['id_idioma']!=""){
			$this->db->where('a.id_idioma', $datos['id_idioma']);
		}
		$this->db->order_by('a.id', 'DESC');
		$this->db->where('a.id_talla', $datos['id_talla']);
		$this->db->select('a.id, a.titulo, a.precio, a.slug');
		$this->db->from('detalle_productos a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	//

	public function consultar_genero2($id,$id_idioma){
		if($id_idioma!=""){
			$this->db->where('a.id_idioma', $id_idioma);
		}
		$this->db->order_by('a.id', 'DESC');
		$this->db->where('a.id_categoria_prod', $id);
		$this->db->select('a.id, a.titulo, a.precio, g.ruta, a.slug');
		$this->db->from('detalle_productos a');
		$this->db->join('detalle_productos_imag c', 'c.	id_det_prod = a.id');
		$this->db->join('galeria g', 'g.id = c.id_imagen');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}



		public function consultar_val1($id_idioma, $id_linea,$id_genero){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_categoria_prod', $id_genero);
			$this->db->where('a.id_marca', $id_linea);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}


		public function consultar_FIL2($id_idioma, $id_linea,$id_precio1, $id_precio2){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_marca', $id_linea);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_FIL3($id_idioma, $id_genero,$id_precio1, $id_precio2){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_categoria_prod', $id_genero);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_FIL4($id_idioma, $id_linea, $id_color){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_marca', $id_linea);
			$this->db->where('a.id_color', $id_color);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_FIL5($id_idioma, $id_linea, $id_talla){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_marca', $id_linea);
			$this->db->where('a.id_talla', $id_talla);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_FIL6($id_idioma, $id_genero, $id_color){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_categoria_prod', $id_genero);
			$this->db->where('a.id_color', $id_color);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_FIL7($id_idioma, $id_genero, $id_talla){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_categoria_prod', $id_genero);
			$this->db->where('a.id_talla', $id_talla);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		public function consultar_FIL8($id_idioma, $id_color,$id_precio1, $id_precio2){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_color', $id_color);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		public function consultar_FIL9($id_idioma, $id_talla,$id_precio1, $id_precio2){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_talla', $id_talla);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_FIL10($id_idioma, $id_color, $id_talla){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_color', $id_color);
			$this->db->where('a.id_talla', $id_talla);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val2($id_idioma, $id_linea,$id_genero, $id_precio1, $id_precio2){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_categoria_prod', $id_genero);
			$this->db->where('a.id_marca', $id_linea);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio,a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val3($id_idioma, $id_linea, $id_genero, $id_color){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_color', $id_color);
			$this->db->where('a.id_categoria_prod', $id_genero);
			$this->db->where('a.id_marca', $id_linea);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val4($id_idioma, $id_linea,$id_genero, $id_talla){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_talla', $id_talla);
			$this->db->where('a.id_categoria_prod', $id_genero);
			$this->db->where('a.id_marca', $id_linea);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val5($id_idioma, $id_linea,$id_color, $id_precio1, $id_precio2){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_color', $id_color);
			$this->db->where('a.id_marca', $id_linea);
			$this->db->order_by('a.id', 'DESC');
			//$this->db->where('a.id_marca', $id);
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val6($id_idioma, $id_linea,$id_talla, $id_precio1, $id_precio2){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_talla', $id_talla);
			$this->db->where('a.id_marca', $id_linea);
			$this->db->order_by('a.id', 'DESC');
			//$this->db->where('a.id_marca', $id);
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val7($id_idioma, $id_linea,$id_color,$id_talla){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_marca', $id_linea);
			$this->db->where('a.id_color', $id_color);
			$this->db->where('a.id_talla', $id_talla);
			$this->db->order_by('a.id', 'DESC');
			//$this->db->where('a.id_marca', $id);
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val8($id_idioma, $id_genero,$id_precio1,$id_precio2,$id_color){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_categoria_prod', $id_genero);
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_color', $id_color);
			$this->db->order_by('a.id', 'DESC');
			//$this->db->where('a.id_marca', $id);
			$this->db->select('a.id, a.titulo, a.precio, g.ruta, a.slug');
			$this->db->from('detalle_productos a');
			$this->db->join('detalle_productos_imag c', 'c.	id_det_prod = a.id');
			$this->db->join('galeria g', 'g.id = c.id_imagen');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val9($id_idioma, $id_genero,$id_precio1,$id_precio2,$id_talla){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_categoria_prod', $id_genero);
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_talla', $id_talla);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val10($id_idioma,$id_precio1,$id_precio2,$id_color,$id_talla){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_color', $id_color);
			$this->db->where('a.id_talla', $id_talla);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val11($id_idioma,$id_linea,$id_genero,$id_precio1,$id_precio2,$id_color){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_marca', $id_linea);
			$this->db->where('a.id_categoria_prod', $id_genero);
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_color', $id_color);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val12($id_idioma,$id_linea,$id_genero,$id_precio1,$id_precio2,$id_talla){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_marca', $id_linea);
			$this->db->where('a.id_categoria_prod', $id_genero);
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_talla', $id_talla);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio , a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val13($id_idioma,$id_linea,$id_precio1,$id_precio2,$id_talla,$id_color){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_marca', $id_linea);
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_talla', $id_talla);
			$this->db->where('a.id_color', $id_color);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val14($id_idioma,$id_precio1,$id_precio2,$id_talla,$id_color,$id_genero){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_talla', $id_talla);
			$this->db->where('a.id_color', $id_color);
			$this->db->where('a.id_categoria_prod', $id_genero);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultar_val15($id_idioma,$id_linea,$id_genero,$id_precio1,$id_precio2,$id_color,$id_talla){
			if($id_idioma!=""){
				$this->db->where('a.id_idioma', $id_idioma);
			}
			$this->db->where('a.id_marca', $id_linea);
			$this->db->where('a.id_categoria_prod', $id_genero);
			$this->db->where('a.precio >=', $id_precio1);
			$this->db->where('a.precio <=',$id_precio2);
			$this->db->where('a.id_color', $id_color);
			$this->db->where('a.id_talla', $id_talla);
			$this->db->order_by('a.id', 'DESC');
			$this->db->select('a.id, a.titulo, a.precio, a.slug');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultarProductoSlug($datos){
			//print_r($datos);die;
		    $this->db->where('a.slug=',$datos["id_producto"]);
			/*$this->db->select('a.*, b.id as id_idioma,
							   b.descripcion as descripcion_idioma,
							   c.ruta as ruta,
							   c.id as id_imagen,
							   f.titulo as categoria,
							   g.titulo as tipo_producto,
							   h.titulo as marca,
							   i.descripcion as color,
							   j.descripcion as talla,
							   k.cantidad as cantidad'
							);*/
			$this->db->select('a.*, b.id as id_idioma,
							   b.descripcion as descripcion_idioma,
							   c.ruta as ruta,
							   c.id as id_imagen,
							   f.titulo as categoria,
							   g.titulo as tipo_producto,
							   h.titulo as marca,
							   i.descripcion as color,
							   j.descripcion as talla,
							   a.id_original_clonado'
							);				
			$this->db->from('detalle_productos a');
			$this->db->join('detalle_productos_imag d', 'd.id_det_prod = a.id');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
	        $this->db->join('galeria c', 'c.id = d.id_imagen');
			$this->db->join('categoria_producto f', 'f.id = a.id_categoria_prod');
			$this->db->join('tipo_producto g', 'g.id = a.id_tipo_prod');
			$this->db->join('marcas h', 'h.id = a.id_marca');
			$this->db->join('colores i', 'i.id = a.id_color');
			$this->db->join('tallas j', 'j.id = a.id_talla');
			//$this->db->join('cantidad_producto k', 'k.id_det_prod = a.id');

			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		// public function consultarSubProductos($datos){
		//
		//
		// 	$this->db->order_by('a.id desc');
		//
		// 	if($datos["start"]==''){
		// 		$datos["start"] = 0;
		// 	}
		//
		// 	if($datos["limit"]!=''){
		//
		//        $this->db->limit($datos["limit"], $datos["start"]);
		//     }
		//
		//     $this->db->where('a.id!=',$datos["id_producto"]);
	    //     $this->db->where('a.estatus!=',2);
	    //     $this->db->where('a.id_idioma',$datos["id_idioma"]);
		// 	$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
		// 	$this->db->from('detalle_productos a');
		// 	//$this->db->limit($data["limit"],$data["offset"]);
		// 	$this->db->from('detalle_productos_imag d', 'd.id_det_prod = a.id');
		// 	$this->db->join('idioma b', 'b.id = a.id_idioma');
	    //     $this->db->join('galeria c', 'c.id = d.id_imagen');
		// 	$res = $this->db->get();
		// 	if($res){
		// 		return $res->result();
		// 	}else{
		// 		return false;
		// 	}
		// }

		public function consultarSubProductos($datos){
			//print_r($datos);die;

			$this->db->order_by('a.id desc');

			if($datos["start"]==''){
				$datos["start"] = 0;
			}

			if($datos["limit"]!=''){

			   $this->db->limit($datos["limit"], $datos["start"]);
			}

			$this->db->where('a.id',$datos["id_producto"]);
			$this->db->where('a.estatus!=',2);
			$this->db->where('a.id_idioma',$datos["id_idioma"]);
			$this->db->select('a.*');
			$this->db->from('detalle_productos a');

			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultarimgdetalleProd($id){
			if($id!=""){
				$this->db->where('g.id_det_prod', $id);
			}
			$this->db->order_by('a.id','DESC');
			$this->db->select('g.id_det_prod,a.id, a.ruta');
			$this->db->from('galeria a');
			$this->db->join('detalle_productos_imag g', 'g.id_imagen = a.id');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		/*
		*	existeEnCarrito
		*/
		public function existeEnCarrito($id_producto,$id_carrito){
			$this->db->where('a.id_carrito', $id_carrito);
			$this->db->where('a.id_producto', $id_producto);
			$this->db->order_by('a.id','DESC');
			$this->db->select('a.cantidad');
			$this->db->from('carrito_productos a');
			$res = $this->db->get();

			//print_r($this->db->last_query());die("x");

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		/*
		*	Registro en carrito de comprar
		*/
		public function agregarCarrito($datos){
			if ($this->db->insert("carrito_productos", $datos)){
				return true;
			}else{
			 	return false;
			}
		}
		/*
		*	Consultar info del carrito de compra para x usuario
		*/
		public function consultarCarrito($datos = ""){
			if($datos["id_usuario"]!=""){
				$this->db->where('a.id_usuario', $datos["id_usuario"]);
			}
			$this->db->where('b.estatus !=',"2");
			$this->db->where('a.id_idioma', $datos["id_idioma"]);
			$this->db->order_by('a.id','DESC');
			$this->db->select('a.*,b.cantidad,b.monto,b.monto_total, b.id as id_producto_carrito');
			$this->db->from('detalle_productos a');
			//Para español
			if($datos["id_idioma"]==1){
				$this->db->join('carrito_productos b', 'b.id_producto = a.id');
			}else{
			//Para ingles
				$this->db->join('carrito_productos b', 'b.id_producto = a.id');
			}
			
			$res = $this->db->get();
			
			//print_r($this->db->last_query());die("x");
			
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		/*
		*
		*/
		public function eliminarProducto($id,$data){
			$this->db->where('id', $id);
			if($this->db->update("carrito_productos", $data)){
				return true;
			}else{
				return false;
			}
		}
		/*
		*	Modificar cantidad de productos
		*/
		public function modificarCantidad($id,$data){
			$this->db->where('id', $id);
			if($this->db->update("carrito_productos", $data)){
				return true;
			}else{
				return false;
			}
		}
		/*
		*	Modificar cantidad de productos2
		*/
		public function modificarCantidad2($id_producto,$id_carrito,$data){
			$this->db->where('id_producto', $id_producto);
			$this->db->where('id_carrito', $id_carrito);
			if($this->db->update("carrito_productos", $data)){
				return true;
			}else{
				return false;
			}
		}
		/*
		*	Metodo que busca las cantidades segun el producto este clonado o no
		*/
		public function consultarCantidad($id){
			$this->db->where('a.id_det_prod', $id);
	        $this->db->select('a.cantidad');
	        $this->db->from('cantidad_producto a');
	        $res = $this->db->get();
			$recordset = $res->result();
			return $recordset[0]->cantidad;
		}
		/*
		*	Verificar si existe el carrito
		*/
		public function consultarCarritoExiste($id_usuario){

			$this->db->limit('1');
			if($id_usuario!="")
				$this->db->where('id_usuario', $id_usuario);
			$this->db->where('estatus', '1');
	        $this->db->select('id');
	        $this->db->from('carrito_encabezado');
	        $res = $this->db->get();
			$recordset = $res->result();
			if(count($recordset)>0)
				return $recordset[0]->id;
			else
				return 0;
		}
		/*
		*	crearCarrito
		*/
		public function crearCarrito($datosCarrito){
			///----
			if ($this->db->insert("carrito_encabezado", $datosCarrito)){
				$id_carrito = $this->db->insert_id();
				return $id_carrito;
			}
			//-----
		}

		/*
		*
		*/
}
