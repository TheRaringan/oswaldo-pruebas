<?php
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Categorias_Prod_model extends CI_Model{

	public function iniciar_sesion($login,$clave){
		$this->db->where('login',$login);
		$this->db->where('clave',$clave);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from(' usuarios u');
		return $this->db->count_all_results();
	}

	public function guardarCategoria_Prod($data){
		//print_r($data);die;
		if($this->db->insert("categoria_producto", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function consultarCategoriaProd($data){

		if($data["id_categoria_prod"]!=""){
			$this->db->where('a.id', $data["id_categoria_prod"]);
		}
		$this->db->order_by('a.id', 'DESC');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*,b.id as id_idioma, b.descripcion as descripcion_idioma');
		$this->db->from('categoria_producto a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarExiste($id){
		if($id!=""){
			$this->db->where('a.id', $id);
		}
		$this->db->select('a.*');
		$this->db->from('categoria_producto a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function modificarCategoriasProd($data){
		$this->db->where('id', $data["id"]);
		if($this->db->update("categoria_producto", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function modificarCatProdEstatus($data){
		$this->db->where('id', $data["id"]);
		if($this->db->update("categoria_producto", $data)){
			return true;
		}else{
			return false;
		}
	}
}

?>
