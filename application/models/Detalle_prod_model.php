<?php
	if (!defined('BASEPATH')) exit ('No direct script access allowed');

	class Detalle_prod_model extends CI_Model{

		public function iniciar_sesion($login,$clave){
			$this->db->where('login',$login);
			$this->db->where('clave',$clave);
			$this->db->where('estatus','1');
			$this->db->select('*');
			$this->db->from(' usuarios u');
			return $this->db->count_all_results();
		}
		/*
		*	Guardar Detalle de producto
		*/
		public function guardardetalleProd($datos,$id_imagen,$cantidad){

			if ($this->db->insert("detalle_productos", $datos)){
				$id_det_prod = $this->db->insert_id();
				$id_imagenes = $id_imagen;
				for ($i=0;$i<count($id_imagenes);$i++){
					$arreglo = ["id_det_prod"=>$id_det_prod, "id_imagen" => $id_imagenes[$i]];
					$arreglo1 = ["id_det_prod"=>$id_det_prod, "cantidad" => $cantidad];
					$this->db->insert("detalle_productos_imag", $arreglo);
					$this->db->insert("cantidad_producto", $arreglo1);
				}
				return true;
			}else{
			 	return false;
			}
		}
		/*
		*	clonar Detalle Producto
		*/
		public function clonarDetalleProd($datos,$id_imagen,$id_producto_original){

			if ($this->db->insert("detalle_productos", $datos)){
				$id_det_prod = $this->db->insert_id();
				$id_imagenes = $id_imagen;
				$data_clonado = array("clonado"=>1);
				for ($i=0;$i<count($id_imagenes);$i++){
					$arreglo = ["id_det_prod"=>$id_det_prod, "id_imagen" => $id_imagenes[$i]];
					$this->db->insert("detalle_productos_imag", $arreglo);
				}
				//--
				$arreglo2 = ["id_det_prod"=>$id_det_prod,"id_producto_original"=>$id_producto_original];
				#Marco como clonado los productos en cuestion
				$this->db->where('id', $id_producto_original);
				$modificacion = $this->db->update("detalle_productos", $data_clonado);
				$this->db->insert("productos_clonados",$arreglo2);
				//--
				return true;
			}else{
			 	return false;
			}
		}
		/*
		*
		*/
		public function consultardetalleProd($data){
			if($data["id_detalle_prod"]!=""){
				$this->db->where('a.id', $data["id_detalle_prod"]);
			}
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
	        $this->db->select('a.id, a.titulo, a.descripcion,
							   b.id as id_idioma, b.descripcion as descripcion_idioma,
							   c.id as categoria_prod, c.titulo as titulo_catprod,
							   t.id as tipo_prod, t.titulo as titulo_tipoprod,
							   m.id as marca, m.titulo as titulo_marca,
							   co.id as color, co.descripcion as descripcion_color,
							   ta.id as talla, ta.descripcion as descripcion_talla,
							   a.precio,a.estatus, a.clonado,a.id_original_clonado');
			/*$this->db->select('a.id, a.titulo, a.descripcion,
							   b.id as id_idioma, b.descripcion as descripcion_idioma,
							   c.id as categoria_prod, c.titulo as titulo_catprod,
							   t.id as tipo_prod, t.titulo as titulo_tipoprod,
							   m.id as marca, m.titulo as titulo_marca,
							   co.id as color, co.descripcion as descripcion_color,
							   ta.id as talla, ta.descripcion as descripcion_talla,
							   a.precio,a.estatus, ct.cantidad,a.clonado');*/
			$this->db->from('detalle_productos a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
			$this->db->join('categoria_producto c', 'c.id = a.id_categoria_prod');
			$this->db->join('tipo_producto t', 't.id = a.id_tipo_prod');
			$this->db->join('marcas m', 'm.id = a.id_marca');
			$this->db->join('colores co', 'co.id = a.id_color');
			$this->db->join('tallas ta', 'ta.id = a.id_talla');
			//$this->db->join('cantidad_producto ct', 'ct.id_det_prod = a.id');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		/*
		*	Metodo que busca las cantidades segun el producto este clonado o no
		*/
		public function consultarCantidad($id){
			$this->db->where('a.id_det_prod', $id);
	        $this->db->select('a.cantidad');
	        $this->db->from('cantidad_producto a');
	        $res = $this->db->get();
			$recordset = $res->result();
			return $recordset[0]->cantidad;
		}
		public function consultarimgdetalleProd($id){
			if($id!=""){
				$this->db->where('g.id_det_prod', $id);
			}
			$this->db->order_by('a.id','DESC');
			$this->db->select('g.id_det_prod,a.id, a.ruta');
			$this->db->from('galeria a');
			$this->db->join('detalle_productos_imag g', 'g.id_imagen = a.id');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}


		public function consultarExiste($id){
			if($id!=""){
				$this->db->where('a.id', $id);
			}
			$this->db->select('a.*');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function modificarDetalleProd($data,$id_imagen,$cantidad){
			$this->db->where('id', $data["id"]);
			if($this->db->update("detalle_productos", $data)){
					$this->db->delete('detalle_productos_imag', array('id_det_prod' =>$data["id"]));
					$this->db->delete('cantidad_producto', array('id_det_prod' =>$data["id"]));
					$id_imagenes = $id_imagen;
					for ($i=0;$i<count($id_imagenes);$i++) {
					$arreglo = ["id_det_prod"=>$data["id"], "id_imagen" => $id_imagenes[$i]];
					$arreglo1 = ["id_det_prod"=>$data["id"], "cantidad" => $cantidad];
					$this->db->insert("detalle_productos_imag", $arreglo);
					$this->db->insert("cantidad_producto", $arreglo1);
				}
				return true;
			}else{
			 	return false;
			}
		}

		public function consultarTallas($data){
			$this->db->select('a.id,a.descripcion');
			$this->db->from('tallas a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultarcolor($data){
			if($data["id_idioma"]!=""){
				$this->db->where('a.id_idioma', $data["id_idioma"]);
			}
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.id, a.descripcion');
			$this->db->from('colores a');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		//--
		public function verificarCarrito($id){
			$this->db->where('a.id_producto',$id);
			$this->db->select('a.id');
			$this->db->from('carrito_productos a');
			return $this->db->count_all_results();
		}
		//--
		public function modificarDetalleEstatus($data){
			//----------------------------------------------------------
			$data_arr = array(
			  'estatus' => $data['estatus'],
			);
			//----------------------------------------------------------
			///--Verifico si el registro cuenta con producto clonado
			$this->db->where('a.id',$data["id"]);
			$this->db->select('a.id,a.clonado,a.id_original_clonado');
			$this->db->from('detalle_productos a');
			$res = $this->db->get();
			$detalle_productos = $res->result();
			if( $detalle_productos[0]->clonado!=0){
				//--caso 1 cambio de estatus del registro hijo teniendo los datos del padre
				if( $detalle_productos[0]->id_original_clonado==0){
					//--Consulto con el  id clonado del padre al hijo
					$this->db->where('a.id_original_clonado',$detalle_productos[0]->id);
					$this->db->select('a.id');
					$this->db->from('detalle_productos a');
					$res_hijo = $this->db->get();
					$detalle_productos_hijo = $res_hijo->result();
					$data2["id"] = $detalle_productos_hijo[0]->id;
				}else{
				//---Caso 2 cambio de estatus del padre teniendo los datos del hijo
					$data2["id"] = $detalle_productos[0]->id_original_clonado;
				}
				$this->db->where('id', $data2["id"]);
				$rs0 = $this->db->update("detalle_productos", $data_arr); 
			}
			///---------------------------------------------------------------
			$this->db->where('id', $data["id"]);
	        if($this->db->update("detalle_productos", $data_arr)){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		//--Verifica si existe un registro con ese titulo
		public function consultarExisteTitulo($id,$titulo){
			if($id!=""){
				$this->db->where('d.id !=',$id);
			}
			$this->db->where('d.titulo',$titulo);
			$this->db->select('*');
			$this->db->from('detalle_productos d');
			//$res = $this->db->get();
			//print_r($this->db->last_query());die;
			return $this->db->count_all_results();
		}
		//---
		public function consultarVAloresOtroIdioma($tabla,$id){
			$this->db->where('a.id_categoria_es', $id);
			$this->db->where('a.nombre_tabla', $tabla);
			$this->db->select('a.id_categoria_en');
			$this->db->from('categorias_productos_idiomas a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

	}

?>
