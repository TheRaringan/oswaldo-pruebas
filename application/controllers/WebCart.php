<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebCart extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('WebCart_model');
      $this->load->model('WebProductos_model');
      $this->load->model('WebInicio_model');
      $this->load->helper('meses');
      $this->load->library('google');
      $this->load->library('facebook');
    }
    /*
    *
    */
    public function consultarCarritoUsuario($id_idioma){
        $listado = [];
        if ($this->session->userdata("id")) {
            $id_suario = $this->session->userdata("id");
            $datos = array(
                                "id_idioma"=>$id_idioma,
                                "id_usuario"=>$id_suario
            );
            $respuesta = $this->WebInicio_model->consultarCarrito($datos);
            foreach ($respuesta as $clave_carrito => $valor_carrito) {
                $registro_img = $this->WebInicio_model->consultarimg_sola("",$valor_carrito->id_producto);
                //var_dump($registro_img);die('');
                $listado[] = $registro_img[0] ;
            }
        }
        //---
        //var_dump($listado);die;
        return $listado;
        //---

    }
    /*
    *
    */
    public function verCarrito($idioma=1){
        $datos["idioma"] = $idioma;
        //Aqui debo pasarle el id del cliente...
        //$datos = $this->WebProductos_model->consultarCarrito();
        //-------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        //-------------------------------------------------------
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/cart',$datos);
        $this->load->view('web/footer');
    }

    public function consultarCarrito($value=''){
        $res = [];
        $listado = [];
        $acumulador= 0;
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebCart_model->consultarCarrito($datos);
        if(count($respuesta)>0){
          //---
          foreach ($respuesta as $clave => $valor) {
             $valores = $valor;
             $acumulador = $acumulador+$valores->monto_total;
             $valores->cantidad_objetos = count($respuesta);
             $valores->monto_oculto = $valores->monto;
             $valores->monto_total_oculto = $valores->monto_total;
             $valores->monto_individual= number_format($valores->monto,2);
             $valores->monto_total = number_format($valores->monto_total,2);
             //--Para consultar las imagenes
             //var_dump($valor->id);die();
             $cons_img = $this->WebCart_model->consultarimgdetalleProd($valor->id);
              foreach ($cons_img as $key => $value) {
                  $valorImg = $value;
                  $res[]=$valorImg;
              }
             $valores->imagenes= $res;
             $res = [];
             $listado[] = $valores;
          }
          $listado[0]->monto_global_total_oculto = $acumulador;
          $listado[0]->monto_global_total = number_format($acumulador,2);
          $listado = (object)$respuesta;
          //----
        }
       
        die(json_encode($listado));
    }

    public function eliminar_producto(){
        $mensajes = array("mensaje"=>"");
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
                        'estatus' =>  '2',
                    ); 
        $res_eliminar = $this->WebCart_model->eliminarProducto($datos["id"],$data);
        if($res_eliminar){
            $mensajes["mensaje"] = "modificacion_procesada";
        }else{
             $mensajes["mensaje"] = "error";
        }
        die(json_encode($mensajes));
    }

    public function modificar_cantidad_carrito_productos(){
        
        $mensajes = array("mensaje"=>"");
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $id = $datos["id"];
        $cantidad = $datos["cantidad"];
        $monto_total = $datos["monto_total"];

        $c = 0;
        
        foreach ($id as $clave => $valor) {
            
            //--Obtengo el id del producto
            $rs_carrito = $this->WebCart_model->consultarDatosCarrito($valor);
            $id_producto = $rs_carrito[0]->id_producto;
            
            //Verifico que no exceda la cantidad
            $rs_cantidad = $this->WebCart_model->cantidad_productos($id_producto,$cantidad[$c]);

            if($cantidad[$c]>$rs_cantidad[0]->cantidad){
                $mensajes["mensaje"] = "excede_cantidad";
                die(json_encode($mensajes));
            }
            
            //armo datos a modificar...
            $data = array(
                            'cantidad' =>  $cantidad[$c],
                            'monto_total'=> $monto_total[$c]
                        ); 
            $res_modificar_cantidad = $this->WebCart_model->modificarCantidad($valor,$data); 
            $c++;      
        }

        if($c>0){
            $mensajes["mensaje"] = "modificacion_procesada";
        }else{
            $mensajes["mensaje"] = "error";
        }
        die(json_encode($mensajes));
    }
}    