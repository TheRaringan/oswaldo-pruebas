<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Slider extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Slider_model');
			if (!$this->session->userdata("login")){
				redirect(base_url());
			}
		}

		public function index(){
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/slider/slider');
	        $this->load->view('cpanel/footer');
	    }

		public function registrarSlider(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$posicionar = array(
              'posicion' => $datos["orden"],
              'tipo' => 'insert',
              'id_idioma' =>  $datos['id_idioma'],
            );

            $this->Slider_model->posicionar_modulos($posicionar);

			//print_r($datos);die;
			$data = array(
			  'titulo' => trim(mb_strtoupper($datos['titulo'])),
			  'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
			  'id_idioma' => $datos['id_idioma'],
			  'boton' => trim(mb_strtoupper($datos['boton'])),
			  'url' =>trim(mb_strtoupper($datos['url'])),
			  'id_imagen' => trim(mb_strtoupper($datos['id_imagen'])),
			  'estatus' => '1',
  			  'direccion' => trim(mb_strtoupper($datos['direccion'])),
  			  'orden' =>  $datos["orden"],83
			);
			$respuesta = $this->Slider_model->guardarslider($data);
			if($respuesta==true){
				$mensajes["mensaje"] = "registro_procesado";
			}else{
				$mensajes["mensaje"] = "no_registro";
			}
			die(json_encode($mensajes));
		}
		/*
		*	Modificar Slider
		*/
		public function modificarSlider(){
	        $datos = json_decode(file_get_contents('php://input'), TRUE);
	        //var_dump($datos);die('');
	        $posicionar = array(
              'inicial' => $datos["inicial"],
              'tipo' => 'update',
              'id_idioma' =>  $datos['id_idioma'],
              'final' => $datos["orden"]
            );
            //var_dump($posicionar);die('');
	        //-Verifico si existe una noticia con ese titulo....
	        $existe = $this->Slider_model->consultarExiste($datos["id"]);
	        if($existe>0){
	            $data = array(
	              'id' =>  $datos['id'],
	              'titulo' => trim(mb_strtoupper($datos['titulo'])),
				  'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
				  'id_idioma' => $datos['id_idioma'],
				  'boton' => trim(mb_strtoupper($datos['boton'])),
				  'url' =>trim(mb_strtoupper($datos['url'])),
				  'id_imagen' => $datos['id_imagen'],
    			  'direccion' => trim(mb_strtoupper($datos['direccion'])),
    			  'orden'=>  $posicionar["final"]
	            );
	            //var_dump($data);die('');
	            $this->Slider_model->posicionar_modulos($posicionar);

	            $respuesta = $this->Slider_model->modificarSlider($data);
	            if($respuesta==true){
	                $mensajes["mensaje"] = "modificacion_procesada";
	            }else{
	                $mensajes["mensaje"] = "no_registro";
	            }
	        }else{
	             $mensajes["mensaje"] = "existe";
	        }
	        //--
	        die(json_encode($mensajes));
	    }
		/*
		*	Consultar slider
		*/

	    public function consultar_slider(){
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/slider/consultar_slider');
	        $this->load->view('cpanel/footer');
	    }

	    public function consultarSliderTodas(){
	        $res = [];
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Slider_model->consultarSlider($datos);
	        $a = 1;
	        foreach ($respuesta as $key => $value) {
	            $valor = $value;
	            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
	            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

	            $res[] = $valor;
	            $a++;
	        }
	        $listado = (object)$res;
	        die(json_encode($listado));
	    }

	    public function modificarSliderEstatus(){
	    	$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $data = array(
	          'id' =>$datos['id'],
	          'estatus' => $datos['estatus'],
	        );
	        $respuesta = $this->Slider_model->modificarSlider($data);

	        if($respuesta==true){
	            $mensajes["mensaje"] = "modificacion_procesada";
	        }else{
	            $mensajes["mensaje"] = "no_modifico";
	        }

	        die(json_encode($mensajes));
		}

		public function sliderVer(){
	        $datos["id"] = $this->input->post('id_slider');
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/slider/slider',$datos);
	        $this->load->view('cpanel/footer');
	    }

	    public function consultar_orden(){
	    	$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Slider_model->consultarOrden($datos);
	        //var_dump($respuesta);die('');
	        if(!$respuesta){
	        	$listado2["orden"] = array( "orden"=>1 );
	        	$listado = (object)$listado2;
	        }else{
	        	$c = 1;
	        	//var_dump($respuesta);die('');
	        	foreach($respuesta as $clave => $valor) {
	        		$respuesta2[] = array( "orden"=>$c );
	        		$c++;
	        	}
	        	$listado  = (object)$respuesta2;

	        }
	        die(json_encode($listado));
	    }
	}
?>
