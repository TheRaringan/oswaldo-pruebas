<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Categoria_prod extends CI_Controller
{

	function __construct(){
      	parent::__construct();
      	$this->load->database();
      	$this->load->library('session');
      	$this->load->model('Categorias_Prod_model');
      	if (!$this->session->userdata("login")) {
        	redirect(base_url());
      	}
    }

	public function index(){
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard');
        $this->load->view('cpanel/menu');
        $this->load->view('modulos/productos/categoria_prod');
        $this->load->view('cpanel/footer');
    }

	public function consultar_categoriasProd(){
		$this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard');
        $this->load->view('cpanel/menu');
		$this->load->view('modulos/productos/consultar_categoriasProd');
		$this->load->view('cpanel/footer');
	}

	public function registrarCategoriasProd(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
		  'titulo' => trim(mb_strtoupper($datos['titulo'])),
		  'id_idioma' => $datos['id_idioma'],
          'estatus' => '1'
        );
		//print_r($data);die;
        $respuesta = $this->Categorias_Prod_model->guardarCategoria_Prod($data);
        if($respuesta==true){
            $mensajes["mensaje"] = "registro_procesado";
        }else{
            $mensajes["mensaje"] = "no_registro";
        }
        die(json_encode($mensajes));
    }

	public function consultarCategoriaProdTodas(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->Categorias_Prod_model->consultarCategoriaProd($datos);
		//print_r($respuesta);die;
        foreach ($respuesta as $key => $value) {
            $valor = $value;
			$valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }

	public function categoriaProdVer(){

		$datos["id"] = $this->input->post('id_categoria_prod');
		//print_r($datos);die;
		$this->load->view('cpanel/header');
		$this->load->view('cpanel/dashBoard');
		$this->load->view('cpanel/menu');
		$this->load->view('modulos/productos/categoria_prod', $datos);
		$this->load->view('cpanel/footer');
	}

	public function modificarCategoriasProd(){
		$datos = json_decode(file_get_contents('php://input'), TRUE);
		//print_r($datos);die;
		//-Verifico si existe una noticia con ese titulo....
		$existe = $this->Categorias_Prod_model->consultarExiste($datos["id"]);
		if($existe>0){
			$data = array(
			  'id' =>  $datos['id'],
			  'titulo' => trim(mb_strtoupper($datos['titulo'])),
			  'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
			  'id_idioma' => $datos['id_idioma'],
			);
			//var_dump($data);die('');
			$respuesta = $this->Categorias_Prod_model->modificarCategoriasProd($data);
			if($respuesta==true){
				$mensajes["mensaje"] = "modificacion_procesada";
			}else{
				$mensajes["mensaje"] = "no_registro";
			}
		}else{
			 $mensajes["mensaje"] = "existe";
		}
		//--
		die(json_encode($mensajes));
	}

	public function modificarCatProdEstatus(){
		$datos= json_decode(file_get_contents('php://input'), TRUE);
		$data = array(
		  'id' =>$datos['id'],
		  'estatus' => $datos['estatus'],
		);
		$respuesta = $this->Categorias_Prod_model->modificarCatProdEstatus($data);

		if($respuesta==true){
			$mensajes["mensaje"] = "modificacion_procesada";
		}else{
			$mensajes["mensaje"] = "no_modifico";
		}

		die(json_encode($mensajes));
	}
}
?>
