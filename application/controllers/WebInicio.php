<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebInicio extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('Login_model');
      $this->load->model('WebInicio_model');
      $this->load->helper('meses');
      $this->load->library('google');
      $this->load->library('facebook');

    }

    public function index($idioma=1){
 
        $datos["idioma"] = $idioma;
        $datos["slider"] = $this->consultarSliderPhp($idioma);
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/home',$datos);
        $this->load->view('web/footer');
    }
    
    public function consultarCarritoUsuario($id_idioma){
        $listado = [];
        if ($this->session->userdata("id")) {
            $id_suario = $this->session->userdata("id");
            $datos = array(
                                "id_idioma"=>$id_idioma,
                                "id_usuario"=>$id_suario
            );
            $respuesta = $this->WebInicio_model->consultarCarrito($datos);
            foreach ($respuesta as $clave_carrito => $valor_carrito) {
                $registro_img = $this->WebInicio_model->consultarimg_sola("",$valor_carrito->id_producto);
                //var_dump($registro_img);die('');
                $listado[] = $registro_img[0] ;
            }
        }/*else{
            echo "No ha iniciado sesion!";
        }*/
        //---
        //var_dump($listado);die;
        return $listado;
        //---

    }

    public function consultarSliderPhp($id_idioma=1){
        $res = [];
        $datos["id_idioma"] = $id_idioma;
        $respuesta = $this->WebInicio_model->consultarSlider($datos);
        $numero = 1;
        $numero2 = 0;
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $calculo_par = $numero%2;

            if($numero=='1'){
                $par='derecha';
            }else{
                ($calculo_par=='0')?$par='derecha':$par="izquierda";
            }

            $valor->par = $par;
            $valor->num = $numero2;
            $numero++;
            $numero2++;
            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
        }
        $listado = (object)$res;
        return $listado;
    }

    public function consultarSlider(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarSlider($datos);
        $numero = 1;
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $calculo_par = $numero%2;

            if($numero=='1'){
                $par='derecha';
            }else{
                ($calculo_par=='0')?$par='derecha':$par="izquierda";
            }

            $valor->par = $par;
            $valor->num = $numero;
            $numero++;

            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }

    public function consultarNosotros(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarNosotros($datos);
        foreach ($respuesta as $key => $value) {
              $valor[] = array(
                "id" => $value->id,
                "id_idioma" => $value->id_idioma,
                "somos" => $value->somos,
                "mision" => $value->mision,
                "vision" => $value->vision,
                "id_imagen" => $value->id_imagen,
                "estatus" => $value->estatus,
                "descripcion_idioma" => $value->descripcion_idioma,
                "ruta" => $value->ruta,
                "somos1" => strip_tags($value->somos),
                "mision1" => strip_tags($value->mision),
                "vision1" => strip_tags($value->vision)
              );
        }
        $listado = (object)$valor;
        die(json_encode($listado));
    }

    public function registrarContactos(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        //var_dump($datos);die('');
        $data = array(
            "nombres" => $datos["nombres"],
            "telefono" => $datos["telefono"],
            "email" => $datos["email"],
            "mensaje" => $datos["mensaje"]
        );
        $respuesta = $this->WebInicio_model->guardarContactos($data);
        if($respuesta==true){
            $mensajes["mensaje"] = "registro_procesado";
        }else{
            $mensajes["mensaje"] = "no_registro";
        }
        die(json_encode($mensajes));
    }

    public function consultarRedes(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarRedes($datos);
        $listado = (object)$respuesta;
        die(json_encode($listado));
    }

    public function consultarFooter(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarFooter($datos);
        $listado = (object)$respuesta;
        die(json_encode($listado));
    }

    public function consultarproductos(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $res = [];
        $respuesta = $this->WebInicio_model->consultarproductos($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $img = $this->WebInicio_model->consultarimg($value->id);
            $ruta_imagen = "";
            $precio = "";
            $titulo = "";
                foreach ($img as $campod) {
                    $valord = $campod;
                    $id_producto = $valord->id_det_prod;
                    $capturar_id = $valord->imagen;
                    $separar_id = explode(",", $capturar_id);
                    $id_final = $separar_id[0];
                    $una_img = $this->WebInicio_model->consultarimg_sola($id_final,$id_producto);
                    foreach ($una_img as $value2) {
                        $ruta_imagen = $value2->ruta;
                        $precio =  $value2->precio;
                        $titulo =  $value2->titulo;
                        $slug = $value2->slug;
                    }
                }
            $valor->ruta = $ruta_imagen;
            $valor->precio = $precio;
            $valor->titulo = $titulo;
            $valor->slug = $slug;
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }

    public function consultarNoticiasFiltros(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarNoticiasFiltros($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $fecha = $value->fecha;
            $vector_fecha = explode("-",$fecha);
            $valor->dias = $vector_fecha[2];
            $valor->mes = strtoupper(meses($vector_fecha[1]));
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }
}
