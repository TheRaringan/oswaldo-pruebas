<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebNosotros extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('WebNosotros_model');
      $this->load->model('WebInicio_model');
      $this->load->helper('meses');
      $this->load->library('google');
      $this->load->library('facebook');
    }
    /*
    *
    */
    public function consultarCarritoUsuario($id_idioma){
        $listado = [];
        if ($this->session->userdata("id")) {
            $id_suario = $this->session->userdata("id");
            $datos = array(
                                "id_idioma"=>$id_idioma,
                                "id_usuario"=>$id_suario
            );
            $respuesta = $this->WebInicio_model->consultarCarrito($datos);
            foreach ($respuesta as $clave_carrito => $valor_carrito) {
                $registro_img = $this->WebInicio_model->consultarimg_sola("",$valor_carrito->id_producto);
                //var_dump($registro_img);die('');
                $listado[] = $registro_img[0] ;
            }
        }
        //---
        //var_dump($listado);die;
        return $listado;
        //---

    }
    public function index($idioma=1){
        $datos["idioma"] = $idioma;
        //-------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        //-------------------------------------------------------
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/nosotros',$datos);
        $this->load->view('web/footer');
    }

    public function consultarNosotrosFiltro(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarNosotros($datos);
        foreach ($respuesta as $key => $value) {
              $valor[] = array(
                "id" => $value->id,
                "id_idioma" => $value->id_idioma,
                "somos" => $value->somos,
                "mision" => $value->mision,
                "vision" => $value->vision,
                "id_imagen" => $value->id_imagen,
                "estatus" => $value->estatus,
                "descripcion_idioma" => $value->descripcion_idioma,
                "ruta" => $value->ruta,
                "somos1" => strip_tags($value->somos),
                "mision1" => strip_tags($value->mision),
                "vision1" => strip_tags($value->vision)
              );
        }
        $listado = (object)$valor;
        die(json_encode($listado));
    }
}
