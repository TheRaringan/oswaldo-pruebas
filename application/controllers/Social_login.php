<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Social_login extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('Social_login_model');
      $this->load->library('facebook');
      $this->load->library('google');
    }

    
    
    

    public function google(){

        $google_data=$this->google->validate();
		$datas =[
				'nombre_apellido'=>$google_data['name'],
                'correo'=>$google_data['email'],
                'estatus'=> '1'

                ];
        $recordset = $this->Social_login_model->existe($google_data['email']);

        if($recordset > 0){
            #Consulto al usuario
            $record_usuario = $this->Social_login_model->login_social($google_data['name'],$google_data['email']);
            //
            $mensajes["mensajes"] = "inicio_exitoso";
            $data = array(
                'login' => true,
                'id' => $record_usuario[0]->id,
                'nombre_persona' => $record_usuario[0]->nombre_apellido,
                'correo' => $record_usuario[0]->correo,
                'estatus' => $record_usuario[0]->estatus


            );
           // var_dump($data);die;

            $this->session->set_userdata($data);
           // var_dump($_SESSION);die;

            redirect(base_url()."");

          
            }
        else{
            //var_dump($datas);die;
            $record_usuario = $this->Social_login_model->registro($datas);
            //var_dump($record_usuario);die;
            $record_usuario = $this->Social_login_model->login_social($google_data['name'],$google_data['email']);
            $mensajes["mensajes"] = "inicio_exitoso";
            $data = array(
                'login' => true,
                'id' => $record_usuario[0]->id,
                'nombre_persona' => $record_usuario[0]->nombre_apellido,
                'correo' => $record_usuario[0]->correo,
                'estatus' => $record_usuario[0]->estatus
            );
            $this->session->set_userdata($data);
            redirect(base_url()."");
        }
    }
    public function facebook(){

        $userData = array();
		if($this->facebook->is_authenticated()){
			$userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');
		}
      

        $nombre= $userProfile['first_name']." ".$userProfile['last_name'];
		$datas =[
				'nombre_apellido'=>$nombre,
                'correo'=>$userProfile['email'],
                'estatus'=> '1'
                ];
        //var_dump($datas);die;

        $recordset = $this->Social_login_model->existe($userProfile['email']);

        if($recordset > 0){
            #Consulto al usuario
            $record_usuario = $this->Social_login_model->login_social($nombre,$userProfile['email']);
            //
            $mensajes["mensajes"] = "inicio_exitoso";
            $data = array(
                'login' => true,
                'id' => $record_usuario[0]->id,
                'nombre_persona' => $record_usuario[0]->nombre_apellido,
                'correo' => $record_usuario[0]->correo,
                'estatus' => $record_usuario[0]->estatus


            );
           // var_dump($data);die;

            $this->session->set_userdata($data);
           // var_dump($_SESSION);die;

            //redirect(base_url()."");
            redirect(base_url());          
            }
        else{
            //var_dump($datas);die;
            $record_usuario = $this->Social_login_model->registro($datas);
            //var_dump($record_usuario);die;
            $record_usuario = $this->Social_login_model->login_social($nombre,$userProfile['email']);
            $mensajes["mensajes"] = "inicio_exitoso";
            $data = array(
                'login' => true,
                'id' => $record_usuario[0]->id,
                'nombre_persona' => $record_usuario[0]->nombre_apellido,
                'correo' => $record_usuario[0]->correo,
                'estatus' => $record_usuario[0]->estatus
            );
            $this->session->set_userdata($data);
            redirect(base_url()."");
        }
    }
    public function logout()
        {
            $this->session->sess_destroy();
            redirect(base_url()."");

        }
    
}  