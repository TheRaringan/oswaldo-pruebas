<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Tipo_prod extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Tipo_prod_model');
			if (!$this->session->userdata("login")){
				redirect(base_url());
			}
		}

		public function index(){
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/productos/tipo_prod');
	        $this->load->view('cpanel/footer');
	    }

		public function consultarCategoriaProd(){
	        $res = [];
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Tipo_prod_model->consultarCatProd($datos);
	        $a = 1;
	        foreach ($respuesta as $key => $value) {
	            $valor = $value;
	            $res[] = $valor;
	            $a++;
	        }
	        $listado = (object)$res;
	        die(json_encode($listado));
	    }

		public function insertar_tipoProd(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$data = array(
				'titulo' => trim(mb_strtoupper($datos['titulo'])),
			 	'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
				'id_idioma' 		=> $datos['id_idioma'],
				'id_categoria_prod' => $datos['categoria_prod']
		 	);
			$respuesta = $this->Tipo_prod_model->guardartipoProd($data);
			if($respuesta==true){
				$mensajes["mensaje"] = "registro_procesado";
			}else{
				$mensajes["mensaje"] = "no_registro";
			}
			die(json_encode($mensajes));
		}

		public function consultar_tipoProd(){
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/productos/consultar_tipoProd');
	        $this->load->view('cpanel/footer');
	    }

		public function consultarTipoProdTodas(){
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Tipo_prod_model->consultartipoProd($datos);
			//print_r($respuesta);die;
	        foreach ($respuesta as $key => $value) {
	            $valor = $value;
				$valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
	            $res[] = $valor;
	        }
	        $listado = (object)$res;
	        die(json_encode($listado));
	    }

		public function tipoProdVer(){
			$datos["id"] = $this->input->post('id_tipo_prod');
			//print_r($datos);die;
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/productos/tipo_prod', $datos);
	        $this->load->view('cpanel/footer');
	    }


		public function modificar_tipoProd(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			//print_r($datos);die;
			$existe = $this->Tipo_prod_model->consultarExiste($datos["id"]);
			if($existe>0){
				$data = array(
					'id' =>  $datos['id'],
					'titulo' => trim(mb_strtoupper($datos['titulo'])),
				 	'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
					'id_idioma' 		=> $datos['id_idioma'],
					'id_categoria_prod' => $datos['categoria_prod']
			 	);
				$respuesta = $this->Tipo_prod_model->modificarTipoProd($data);
				if($respuesta==true){
					$mensajes["mensaje"] = "modificacion_procesada";
				}else{
					$mensajes["mensaje"] = "no_registro";
				}
			}else{
				 $mensajes["mensaje"] = "existe";
			}
			//--
			die(json_encode($mensajes));
		}

		public function modificar_estatus(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$data = array(
			  'id' =>$datos['id'],
			  'estatus' => $datos['estatus'],
			);
			$respuesta = $this->Tipo_prod_model->modificar_estatus($data);

			if($respuesta==true){
				$mensajes["mensaje"] = "modificacion_procesada";
			}else{
				$mensajes["mensaje"] = "no_modifico";
			}

			die(json_encode($mensajes));
		}

		public function consultarDetalleProd(){
	        $res = [];
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Tipo_prod_model->consultarDetProd($datos);
	        $a = 1;
	        foreach ($respuesta as $key => $value) {
	            $valor = $value;
	            $res[] = $valor;
	            $a++;
	        }
	        $listado = (object)$res;
	        die(json_encode($listado));
	    }
	}

?>
