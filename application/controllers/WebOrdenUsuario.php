<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebOrdenUSuario extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('WebOrdenUsuario_model');
      $this->load->model('WebCart_model');
      $this->load->model('WebInicio_model');
      $this->load->helper('meses');
      $this->load->library('google');
      $this->load->library('facebook');
    }
    /*
    *
    */
    public function consultarCarritoUsuario($id_idioma){
        $listado = [];
        if ($this->session->userdata("id")) {
            $id_suario = $this->session->userdata("id");
            $datos = array(
                                "id_idioma"=>$id_idioma,
                                "id_usuario"=>$id_suario
            );
            $respuesta = $this->WebInicio_model->consultarCarrito($datos);
            foreach ($respuesta as $clave_carrito => $valor_carrito) {
                $registro_img = $this->WebInicio_model->consultarimg_sola("",$valor_carrito->id_producto);
                //var_dump($registro_img);die('');
                $listado[] = $registro_img[0] ;
            }
        }
        //---
        //var_dump($listado);die;
        return $listado;
        //---

    }
    /*
    *   ver orden
    */
    public function verOrdenUsuario($idioma=1){
        $datos["idioma"] = $idioma;
        $datos["id_usuario"] = $this->session->userdata("id");
        //-------------------------------------------------------------------------
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        //---------------------------------------------------------------------------
        //Aqui debo pasarle el id del cliente...
        //$datos = $this->WebProductos_model->consultarCarrito();
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/orderUsuario',$datos);
        $this->load->view('web/footer');
    }
    /*
    *
    */
    public function consultarProductosOrden(){
        $res = [];
        $listado = [];
        $acumulador= 0;
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $id_suario = $this->session->userdata("id");
        //---
        #Consulto la orden
        $ordenEncabezado = $this->WebOrdenUsuario_model->consultarOrden($datos,$id_suario);
        //---
        $acum_monto = 0;
        if(count($ordenEncabezado)>0){
            foreach ($ordenEncabezado as $clave_enc => $valor_enc) {
              //---
              $valores = $valor_enc;
              $respuesta = $this->WebOrdenUsuario_model->consultarCarrito($datos,$valor_enc->id);
              $valores->orden_detalle = $respuesta;
              $res_productos=[];
              foreach ($respuesta as $clave_producto => $valor_producto) {
                  $valores_productos = $valor_producto;
                  $cons_img = $this->WebCart_model->consultarimgdetalleProd($valor_producto->id_producto_real);
                  $acum_monto = $acum_monto+$valor_producto->monto_total;
                  $valor_producto->monto = number_format($valor_producto->monto,2);
                  $valor_producto->monto_total = number_format($valor_producto->monto_total,2);
                  $valores_productos->ruta = $cons_img[0]->ruta;
                  $res_productos[] = $valores_productos;
              }
              $valores->orden_detalle = $res_productos;
              $valores->orden_monto = number_format($acum_monto,2);
              $acum_monto = 0;
              $super_valor[] = $valores;
              //---
            }
            $listado = (object)$super_valor;
        }
        //---
        /*$respuesta = $this->WebOrdenUsuario_model->consultarCarrito($datos,$id_suario);
        if(count($respuesta)>0){
          //---
          foreach ($respuesta as $clave => $valor) {
             $valores = $valor;
             $acumulador = $acumulador+$valores->monto_total;
             $valores->cantidad_objetos = count($respuesta);
             $valores->monto_oculto = $valores->monto;
             $valores->monto_total_oculto = $valores->monto_total;
             $valores->monto_individual= number_format($valores->monto,2);
             $valores->monto_total = number_format($valores->monto_total,2);
             //--Para consultar las imagenes
             //var_dump($valor->id);die();
             $cons_img = $this->WebCart_model->consultarimgdetalleProd($valor->id_producto_real);
              foreach ($cons_img as $key => $value) {
                  $valorImg = $value;
                  $res[]=$valorImg;
              }
             $valores->imagenes= $res;
             $res = [];
             $listado[] = $valores;
          }
          $listado[0]->monto_global_total_oculto = $acumulador;
          $listado[0]->monto_global_total = number_format($acumulador,2);
          $listado = (object)$respuesta;
          //----
        }*/
       
        die(json_encode($listado));
    }
}    