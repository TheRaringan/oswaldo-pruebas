<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebCheckOut extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('WebCheckOut_model');
      $this->load->model('WebInicio_model');
      $this->load->helper('meses');
      $this->load->library('google');
      $this->load->library('facebook');
    }
    /*
    *
    */
    public function consultarCarritoUsuario($id_idioma){
        $listado = [];
        if ($this->session->userdata("id")) {
            $id_suario = $this->session->userdata("id");
            $datos = array(
                                "id_idioma"=>$id_idioma,
                                "id_usuario"=>$id_suario
            );
            $respuesta = $this->WebInicio_model->consultarCarrito($datos);
            foreach ($respuesta as $clave_carrito => $valor_carrito) {
                $registro_img = $this->WebInicio_model->consultarimg_sola("",$valor_carrito->id_producto);
                //var_dump($registro_img);die('');
                $listado[] = $registro_img[0] ;
            }
        }
        //---
        //var_dump($listado);die;
        return $listado;
        //---

    }
    /*
    *   ver orden
    */
    public function verCheckOut($idioma="",$id_carrito){
        //--
        $datos["idioma"] = $idioma;
        $id_usuario = $this->session->userdata("id");
        //-----------------------------------------------------
        #Genero regitro de la orden--------------------------------------------------
        #Consulto los datos del carrito de compra

        $res_info_carrito = $this->WebCheckOut_model->consultarCarritoInfo($id_usuario);

        $existe_orden = $this->WebCheckOut_model->existeOrden($id_usuario,$id_carrito);
        if($existe_orden==0){
            //----------------------------------
            #Registro orden encabezado
            $datosOrdenCompra = $this->WebCheckOut_model->generarOrdenEncabezado($id_usuario,$id_carrito);
            #registro orden detalle
            $res_save_carrito = $this->WebCheckOut_model->generarOrdenDetalle($datosOrdenCompra["id_orden_compra"],$res_info_carrito);
            //----------------------------------
        }else{
            $datosOrdenCompra = $this->WebCheckOut_model->consultarOrden($id_carrito);
        }
        
        $datos["numero_orden_compra"] = $datosOrdenCompra["numero_orden_compra"];
        $datos["correo"] = $this->session->userdata("correo");
        //----
        $datos_menu["productos"] = $this->consultarCarritoUsuario($datos["idioma"]);
        $datos_menu["cuantos"] = count($datos_menu["productos"]);
        if($datos["idioma"] =="1"){
            $datos_menu["ir_cart"] ="carrito";
            $datos_menu["ir_orden"] ="orden_usuario";  
        }else{
            $datos_menu["ir_cart"] ="cart";
            $datos_menu["ir_orden"] ="order_us"; 
        }
        //----
        $this->load->view('web/header');
        $this->load->view('web/menu',$datos_menu);
        $this->load->view('web/checkout',$datos);
        $this->load->view('web/footer');
    }
}    