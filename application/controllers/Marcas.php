<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Marcas extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Marcas_model');
			if (!$this->session->userdata("login")){
				redirect(base_url());
			}
		}

		public function index(){
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/marcas/marcas');
	        $this->load->view('cpanel/footer');
	    }

		public function registrarMarcas(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			//print_r($datos);die;
			$data = array(
			  'titulo' => trim(mb_strtoupper($datos['titulo'])),
			  'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
			  'id_idioma' => $datos['id_idioma'],
			  'id_imagen' => trim(mb_strtoupper($datos['id_imagen'])),
			  'estatus' => '1'
		  	);
			$respuesta = $this->Marcas_model->guardarMarcas($data);
			if($respuesta==true){
				$mensajes["mensaje"] = "registro_procesado";
			}else{
				$mensajes["mensaje"] = "no_registro";
			}
			die(json_encode($mensajes));
		}

		public function consultar_marcas(){
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/marcas/consultar_marcas');
	        $this->load->view('cpanel/footer');
	    }

		public function consultarMarcasTodas(){
		   $res = [];
		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   $respuesta = $this->Marcas_model->consultarMarcas($datos);
		   $a = 1;
		   foreach ($respuesta as $key => $value) {
			   $valor = $value;
			   //$valor->descripcion_sin_html = strip_tags($value->descripcion);
			   $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

			   $res[] = $valor;
			   $a++;
		   }
		   $listado = (object)$res;
		   die(json_encode($listado));
	   }

	   public function marcasVer(){
		   $datos["id"] = $this->input->post('id_marca');
		   $this->load->view('cpanel/header');
		   $this->load->view('cpanel/dashBoard');
		   $this->load->view('cpanel/menu');
		   $this->load->view('modulos/marcas/marcas',$datos);
		   $this->load->view('cpanel/footer');
	   }

	   public function modificarMarcas(){
		   $datos = json_decode(file_get_contents('php://input'), TRUE);
		   //-Verifico si existe una noticia con ese titulo....
		   $existe = $this->Marcas_model->consultarExiste($datos["id"]);
		   if($existe>0){
			   $data = array(
				 'id' =>  $datos['id'],
				 'titulo' => trim(mb_strtoupper($datos['titulo'])),
				 'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
				 'id_idioma' => $datos['id_idioma'],
				 'id_imagen' => $datos['id_imagen']
			   );
			   //var_dump($data);die('');
			   $respuesta = $this->Marcas_model->modificarMarcas($data);
			   if($respuesta==true){
				   $mensajes["mensaje"] = "modificacion_procesada";
			   }else{
				   $mensajes["mensaje"] = "no_registro";
			   }
		   }else{
				$mensajes["mensaje"] = "existe";
		   }
		   //--
		   die(json_encode($mensajes));
	   }

	   public function modificarMarcasEstatus(){
		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   $data = array(
			 'id' =>$datos['id'],
			 'estatus' => $datos['estatus'],
		   );
		   $respuesta = $this->Marcas_model->modificarMarcasEstatus($data);

		   if($respuesta==true){
			   $mensajes["mensaje"] = "modificacion_procesada";
		   }else{
			   $mensajes["mensaje"] = "no_modifico";
		   }

		   die(json_encode($mensajes));
	   }

	   public function consultarmarca_idioma(){
		   //print_r("hola");
		   $res = [];
		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   $respuesta = $this->Marcas_model->consultarMarcas_idioma($datos);
		   //print_r($respuesta);die;
		   $a = 1;
		   foreach ($respuesta as $key => $value) {
			   $valor = $value;
			   $res[] = $valor;
			   $a++;
		   }
		   $listado = (object)$res;
		   die(json_encode($listado));
	   }

	}
?>
