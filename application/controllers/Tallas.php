<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Tallas extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Tallas_model');
			if (!$this->session->userdata("login")){
				redirect(base_url());
			}
		}

		public function index(){
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/tallas/tallas');
	        $this->load->view('cpanel/footer');
	    }

		public function registrarTallas(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			//print_r($datos);die;
			$data = array(
                'id' => '',
			  'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
			  'estatus' => '1'
              );
            
			$respuesta = $this->Tallas_model->guardarTallas($data);
			if($respuesta==true){
				$mensajes["mensaje"] = "registro_procesado";
			}else{
				$mensajes["mensaje"] = "no_registro";
			}
			die(json_encode($mensajes));
		}

		public function consultar_Tallas(){
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/tallas/consultar_tallas');
	        $this->load->view('cpanel/footer');
	    }

		public function consultarTallasTodos(){
		   //$res = [];

		   $datos= json_decode(file_get_contents('php://input'), TRUE);
           $respuesta = $this->Tallas_model->consultarTallas($datos);
           //var_dump($respuesta);die;
		   $a = 1;
		   foreach ($respuesta as $key => $value) {
			   $valor = $value;
			   //$valor->descripcion_sin_html = strip_tags($value->descripcion);
			   //$valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

			   $res[] = $valor;
			   $a++;
		   }

		   $listado = (object)$res;

		   die(json_encode($listado));
	   }

	   public function TallasVer(){
           $datos["id"] = $this->input->post('id_talla');
            //var_dump($datos);die;

		   $this->load->view('cpanel/header');
		   $this->load->view('cpanel/dashBoard');
		   $this->load->view('cpanel/menu');
		   $this->load->view('modulos/tallas/tallas',$datos);
		   $this->load->view('cpanel/footer');
	   }

	   public function modificarTallas(){
		   $datos = json_decode(file_get_contents('php://input'), TRUE);

		   //-Verifico si existe una noticia con ese titulo....
		   $existe = $this->Tallas_model->consultarExiste($datos["id"]);
		   if($existe>0){
			   $data = array(
				 'id' =>  $datos['id'],
				 'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
				 'estatus' => '1'
			   );
			  // var_dump($data);die('');
			   $respuesta = $this->Tallas_model->modificarTallas($data);
			   if($respuesta==true){
				   $mensajes["mensaje"] = "modificacion_procesada";
			   }else{
				   $mensajes["mensaje"] = "no_registro";
			   }
		   }else{
				$mensajes["mensaje"] = "existe";
		   }
		   //--
		   die(json_encode($mensajes));
	   }

	   public function modificarTallasEstatus(){
		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   $data = array(
			 'id' =>$datos['id'],
			 'estatus' => $datos['estatus'],
		   );
		   $respuesta = $this->Tallas_model->modificarTallasEstatus($data);

		   if($respuesta==true){
			   $mensajes["mensaje"] = "modificacion_procesada";
		   }else{
			   $mensajes["mensaje"] = "no_modifico";
		   }

		   die(json_encode($mensajes));
	   }

	   public function consultarmarca_idioma(){
		   //print_r("hola");
		   $res = [];
		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   $respuesta = $this->Marcas_model->consultarMarcas_idioma($datos);
		   //print_r($respuesta);die;
		   $a = 1;
		   foreach ($respuesta as $key => $value) {
			   $valor = $value;
			   $res[] = $valor;
			   $a++;
		   }
		   $listado = (object)$res;
		   die(json_encode($listado));
	   }

	}
?>
