<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Categoria_idiomas extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Categoria_idiomas_model');
			if (!$this->session->userdata("login")){
				redirect(base_url());
			}
		}

		public function index(){
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/categoria_idioma/categoria_idioma');
	        $this->load->view('cpanel/footer');
		}

	//	Colores
		function consultar_colores_es(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Categoria_idiomas_model->consultarColores_es($datos);
	        foreach ($respuesta as $key => $value) {
				$valor = $value;
				//$valor->descripcion_sin_html = strip_tags($value->descripcion);
				$valor->descripcion = substr(strip_tags($value->descripcion),0,150);
				$res[] = $valor;
			}
			$listado = (object)$res;
			die(json_encode($listado));
		}
		function consultar_colores_en(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Categoria_idiomas_model->consultarColores_en($datos);
	        foreach ($respuesta as $key => $value) {
				$valor = $value;
				//$valor->descripcion_sin_html = strip_tags($value->descripcion);
				$valor->descripcion = substr(strip_tags($value->descripcion),0,150);
				$res[] = $valor;
			}
			$listado = (object)$res;
			die(json_encode($listado));
		}
	//	Marcas
		function consultar_marcas_es(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Categoria_idiomas_model->consultarMarcas_es($datos);
	        foreach ($respuesta as $key => $value) {
				$valor = $value;
				//$valor->descripcion_sin_html = strip_tags($value->descripcion);
				$valor->descripcion = substr(strip_tags($value->descripcion),0,150);
				$res[] = $valor;
			}
			$listado = (object)$res;
			die(json_encode($listado));
		}
		function consultar_marcas_en(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Categoria_idiomas_model->consultarMarcas_en($datos);
	        foreach ($respuesta as $key => $value) {
				$valor = $value;
				//$valor->descripcion_sin_html = strip_tags($value->descripcion);
				$valor->descripcion = substr(strip_tags($value->descripcion),0,150);
				$res[] = $valor;
			}
			$listado = (object)$res;
			die(json_encode($listado));
		}
		function consultar_categoria_producto_es(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Categoria_idiomas_model->consultarCategoria_es($datos);
	        foreach ($respuesta as $key => $value) {
				$valor = $value;
				//$valor->descripcion_sin_html = strip_tags($value->descripcion);
				$valor->descripcion = substr(strip_tags($value->descripcion),0,150);
				$res[] = $valor;
			}
			$listado = (object)$res;
			die(json_encode($listado));
		}
		function consultar_categoria_producto_en(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Categoria_idiomas_model->consultarCategoria_en($datos);
			foreach ($respuesta as $key => $value) {
				$valor = $value;
				//$valor->descripcion_sin_html = strip_tags($value->descripcion);
				$valor->descripcion = substr(strip_tags($value->descripcion),0,150);
				$res[] = $valor;
			}
			$listado = (object)$res;
			die(json_encode($listado));
		}
		function consultar_tipo_productos_es(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Categoria_idiomas_model->consultarTipos_es($datos);
	        foreach ($respuesta as $key => $value) {
				$valor = $value;
				//$valor->descripcion_sin_html = strip_tags($value->descripcion);
				$valor->descripcion = substr(strip_tags($value->descripcion),0,150);
				$res[] = $valor;
			}
			$listado = (object)$res;
			die(json_encode($listado));
		}
		function consultar_tipo_productos_en(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Categoria_idiomas_model->consultarTipos_en($datos);
	        foreach ($respuesta as $key => $value) {
				$valor = $value;
				//$valor->descripcion_sin_html = strip_tags($value->descripcion);
				$valor->descripcion = substr(strip_tags($value->descripcion),0,150);
				$res[] = $valor;
			}
			$listado = (object)$res;
			die(json_encode($listado));
		}


		public function registrar(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			//print_r($datos);die;
			$existe = $this->Categoria_idiomas_model->consultarExiste($datos['id_categoria_es'],$datos['id_categoria_en']);
			//var_dump($existe);die;
			if(!$existe){
			$data = array(
			  'nombre_tabla' => $datos['nombre_tabla'],
			  'id_categoria_es' => $datos['id_categoria_es'],
			  'id_categoria_en' => $datos['id_categoria_en'],
			  'estatus' => 1
              );

			  $respuesta = $this->Categoria_idiomas_model->guardar($data);
			  if($respuesta==true){
				  $mensajes["mensaje"] = "registro_procesado";
			  }else{
				  $mensajes["mensaje"] = "no_registro";
			  }
				}
			if($existe){
			$mensajes["mensaje"] = "existe";}
			die(json_encode($mensajes));
		}

		public function consultar_categoria_idiomas(){
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/categoria_idioma/consultar_categoria_idioma');
	        $this->load->view('cpanel/footer');
	    }

		public function consultar_CategoriaIdiomasTodos(){

		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   $colores = $this->Categoria_idiomas_model->consultaColores($datos);
		   $marcas = $this->Categoria_idiomas_model->consultaMarcas($datos);
		   $tipos = $this->Categoria_idiomas_model->consultaTipos($datos);
		   $categoria = $this->Categoria_idiomas_model->consultaCategoria($datos);
		   $arreglo = array_merge($colores,$marcas,$tipos,$categoria);
	      // print_r($arreglo);die;
		   foreach ($arreglo as $key => $value) {
			   $valor = $value;
			   $valor->descripcion_es = substr(strip_tags($value->descripcion_es),0,150);
			   $valor->descripcion_en = substr(strip_tags($value->descripcion_en),0,150);

			   $res[] = $valor;
		   }
		   $listado = (object)$res;
	       //print_r($listado);die;
		   die(json_encode($listado));
	   }
		public function modificar(){
		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   //print_r($datos['estatus']);die;
		   $data = array(
			 'id' =>$datos['id'],
			 'estatus' => $datos['estatus'],
		   );
			   $respuesta = $this->Categoria_idiomas_model->modificarEstatus($data);
			if($respuesta==true){
			   $mensajes["mensaje"] = "modificacion_procesada";
		   }else{
			   $mensajes["mensaje"] = "no_modifico";
		   }
		   die(json_encode($mensajes));
	   }
	   public function eliminar(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			//print_r($datos['estatus']);die;
			$data = array(
			'id' =>$datos['id'],
			'estatus' => $datos['estatus'],
			);
				$respuesta = $this->Categoria_idiomas_model->eliminarEstatus($data);
			if($respuesta==true){
				$mensajes["mensaje"] = "modificacion_procesada";
			}else{
				$mensajes["mensaje"] = "no_modifico";
			}
			die(json_encode($mensajes));
		}

	   

	}
?>
