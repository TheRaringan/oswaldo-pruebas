<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class RedesSociales extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('RedesSociales_model');
      if (!$this->session->userdata("login")) {
        redirect(base_url());
      }
    }

    public function index(){
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard');
        $this->load->view('cpanel/menu');
        $this->load->view('modulos/redesSociales/RedesSociales');
        $this->load->view('cpanel/footer');
    }

    public function consultar_redes(){
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      $respuesta = $this->RedesSociales_model->consultar_redes($datos);
      die(json_encode($respuesta));
    }

    public function consultar_redes_url(){
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      $respuesta = $this->RedesSociales_model->consultar_redes_url($datos);
      die(json_encode($respuesta));
    }

    public function registrarRedes(){
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      //var_dump($datos);
      $data = array(
        'url_red' => trim(mb_strtoupper($datos['url_red'])),
        'id_tipo_red'=>$datos['id_tipo_red']
      );
      $respuesta = $this->RedesSociales_model->guardarRedes($data);
      if($respuesta==true){
          $mensajes["mensaje"] = "registro_procesado";
      }else{
          $mensajes["mensaje"] = "no_registro";
      }
      die(json_encode($mensajes));
    }

    public function modificarRedes(){
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      //var_dump($datos);
      $data = array(
        'id'=>$datos["id_red"],
        'url_red' => trim(mb_strtoupper($datos['url_red'])),
        'id_tipo_red'=>$datos['id_tipo_red']
      );

      $respuesta = $this->RedesSociales_model->modificarRedes($data); 

      if($respuesta==true){
          $mensajes["mensaje"] = "modificacion_procesada";
      }else{
          $mensajes["mensaje"] = "no_modifico";
      }  
       
      die(json_encode($mensajes));
    }
}    