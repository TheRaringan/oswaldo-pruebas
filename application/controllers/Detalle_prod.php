<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Detalle_prod extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Detalle_prod_model');
			if (!$this->session->userdata("login")){
				redirect(base_url());
			}
		}

		public function index(){
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/productos/detalle_prod');
	        $this->load->view('cpanel/footer');
	    }
	    /*
	    *	Metodo que clona un prodcuto en ingles
	    */
	    public function clonar_detalleProd(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$id_producto_original = $datos["id"];
			#Modificacion gsantucci 19-06-2019
			$existe_titulo = $this->Detalle_prod_model->consultarExisteTitulo("",$datos["titulo"]);
        	if($existe_titulo==0){
				$data = array(
					'titulo' 			=> trim($datos['titulo']),
				 	'descripcion'		=> trim($datos['descripcion']),
					'id_idioma' 		=> $datos['id_idioma'],
					'id_categoria_prod' => $datos['categoria_prod'],
					'id_tipo_prod'		=> $datos['tipo_prod'],
					'id_marca'			=> $datos['marca'],
					'id_color'			=> $datos['color'],
					'id_talla'			=> $datos['talla'],
					'precio'			=> $datos['precio'],
					'slug' 				=> $this->generarSlug($datos["titulo"]),
					'id_original_clonado'=>$id_producto_original,
					'clonado'=>true,
			 	);

				$cantidad  = $datos['cantidad'];
				$id_imagen = $datos['id_imagen'];
				$respuesta = $this->Detalle_prod_model->clonarDetalleProd($data,$id_imagen,$id_producto_original);
				if($respuesta==true){
					$mensajes["mensaje"] = "registro_procesado";
				}else{
					$mensajes["mensaje"] = "no_registro";
				}
			}else{
             $mensajes["mensaje"] = "existe";
        	}
			die(json_encode($mensajes));
		}
		/*
		*
		*/
		public function insertar_detalleProd(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			#Modificacion gsantucci 19-06-2019
			$existe_titulo = $this->Detalle_prod_model->consultarExisteTitulo("",$datos["titulo"]);
        	if($existe_titulo==0){
				$data = array(
					'titulo' 			=> trim($datos['titulo']),
				 	'descripcion'		=> trim($datos['descripcion']),
					'id_idioma' 		=> $datos['id_idioma'],
					'id_categoria_prod' => $datos['categoria_prod'],
					'id_tipo_prod'		=> $datos['tipo_prod'],
					'id_marca'			=> $datos['marca'],
					'id_color'			=> $datos['color'],
					'id_talla'			=> $datos['talla'],
					'precio'			=> $datos['precio'],
					'slug' 				=> $this->generarSlug($datos["titulo"]),
			 	);

				$cantidad  = $datos['cantidad'];
				$id_imagen = $datos['id_imagen'];
				$respuesta = $this->Detalle_prod_model->guardardetalleProd($data,$id_imagen,$cantidad);
				if($respuesta==true){
					$mensajes["mensaje"] = "registro_procesado";
				}else{
					$mensajes["mensaje"] = "no_registro";
				}
			}else{
             $mensajes["mensaje"] = "existe";
        	}
			die(json_encode($mensajes));
		}

		public function consultar_detProd(){
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/productos/consultar_detProd');
	        $this->load->view('cpanel/footer');
	    }

		public function consultarDetalleProdTodas(){
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $res = [];
	        $respuesta = $this->Detalle_prod_model->consultardetalleProd($datos);
			//print_r($respuesta);die;
	        foreach ($respuesta as $key => $value){
				$valor = $value;
				//---------------------------------------
				($value->id_original_clonado==0) ? $idBuscarCantidad=$value->id: $idBuscarCantidad=$value->id_original_clonado;
				$value->cantidad = $this->Detalle_prod_model->consultarCantidad($idBuscarCantidad);
				//---------------------------------------
				$cons_img = $this->Detalle_prod_model->consultarimgdetalleProd($value->id);//CONSULTA SECUNDARIA
				$x = 0;
				$imagen = "";
				$id_imagen = "";
				foreach ($cons_img as $campo2){
					if ($x==0) {
						$imagen.=$campo2->ruta;
						$id_imagen.=$campo2->id;
					}else{
						$imagen.="|".$campo2->ruta;
						$id_imagen.="|".$campo2->id;
					}
					$x++;
				}
				$valor->imagen[]= array('ruta' => $imagen , 'id_imagen'=> $id_imagen);
				$valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
	            $res[] = $valor;
	        }
	        $listado = (object)$res;
	        die(json_encode($listado));
	    }

		public function detalleProdVer(){
			$datos["id"] = $this->input->post('id_detalle_prod');
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/productos/detalle_prod', $datos);
	        $this->load->view('cpanel/footer');
	    }
	    public function detalleProdVerClonar(){
			$datos["id"] = $this->input->post('id_detalle_prod');
			$datos["clonar"] = "1";
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard');
	        $this->load->view('cpanel/menu');
	        $this->load->view('modulos/productos/detalle_prod', $datos);
	        $this->load->view('cpanel/footer');
	    }
		public function modificar_detalleProd(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$existe = $this->Detalle_prod_model->consultarExiste($datos["id"]);
			if($existe>0){
				//--Verifico si existe un registro con ese titulo diferente a ese id
				$existe_titulo = $this->Detalle_prod_model->consultarExisteTitulo($datos["id"],$datos["titulo"]);
        		if($existe_titulo==0){
					$data = array(
						'id' 				=>  $datos['id'],
						'titulo' 			=> trim($datos['titulo']),
					 	'descripcion' 		=> trim($datos['descripcion']),
						'id_idioma' 		=> $datos['id_idioma'],
						'id_categoria_prod' => $datos['categoria_prod'],
						'id_tipo_prod'		=> $datos['tipo_prod'],
						'id_marca'			=> $datos['marca'],
						'id_color'			=> $datos['color'],
						'id_talla'			=> $datos['talla'],
						'precio'			=> $datos['precio'],
						'slug' 				=>$this->generarSlug($datos["titulo"]),

				 	);
					$cantidad  = $datos['cantidad'];
					$id_imagen = $datos['id_imagen'];

					$respuesta = $this->Detalle_prod_model->modificarDetalleProd($data,$id_imagen,$cantidad);

					if($respuesta==true){
						$mensajes["mensaje"] = "modificacion_procesada";
					}else{
						$mensajes["mensaje"] = "no_registro";
						//print_r($mensajes);die;
					}
				}else{
					$mensajes["mensaje"] = "existe_titulo";
				}
			}else{
				 $mensajes["mensaje"] = "existe";
			}
			die(json_encode($mensajes));
		}

		function consultar_talla(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Detalle_prod_model->consultarTallas($datos);
	        die(json_encode($respuesta));
		}

		public function consultar_color(){
	        $res = [];
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Detalle_prod_model->consultarcolor($datos);
	        $a = 1;
	        foreach ($respuesta as $key => $value) {
	            $valor = $value;
	            $res[] = $valor;
	            $a++;
	        }
	        $listado = (object)$res;
	        die(json_encode($listado));
	    }

		public function modificarDetalleEstatus(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			//print_r($datos);die;
			$data = array(
			  'id' =>$datos['id'],
			  'estatus' => $datos['estatus'],
			);
			//--
			//Verificar carrito
			$existe_carrito = $this->Detalle_prod_model->verificarCarrito($datos['id']);
			if($existe_carrito==0){
				$respuesta = $this->Detalle_prod_model->modificarDetalleEstatus($data);
				if($respuesta==true){
					$mensajes["mensaje"] = "modificacion_procesada";
				}else{
					$mensajes["mensaje"] = "no_modifico";
				}
			}else{
				$mensajes["mensaje"] = "existe_carrito";
			}
			//--
			

			die(json_encode($mensajes));
		}

		public function generarSlug($cadena){
	        $titulo_min = strtolower($this->normaliza($cadena));
	        $slug_noticias = str_replace(" ","-",$titulo_min);
	        $slug_noticias = preg_replace("/[^a-zA-Z0-9_-]+/", "", $slug_noticias);
	        return $slug_noticias;
	    }

	    public function normaliza ($cadena){
	        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
	        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
	        //$cadena = utf8_decode($cadena);
	        //$cadena = strtr($cadena, utf8_decode($originales), $modificadas);
	        $cadena = strtr($cadena, $originales, $modificadas);
	        $cadena = strtolower($cadena);
	        //return utf8_encode($cadena);
	        return $cadena;
	    }
	    /*
	    *	consultarValoresOtroIdioma
	    */
	    public function consultarValoresOtroIdioma(){
	    	$datos= json_decode(file_get_contents('php://input'), TRUE);
	    	$categorias = $datos["categoria"];
	    	$tipo_producto = $datos["tipo_producto"];
	    	$marca = $datos["marca"];
	    	$color = $datos["color"];
	    	$talla = $datos["talla"];
	    	//----
	    	#Marca producto ///linea producto
	    	//$marca_en = $this->Detalle_prod_model->consultarVAloresOtroIdioma("categoria_producto",$marca);
	    	#marca 
	    	$marca_en = $this->Detalle_prod_model->consultarVAloresOtroIdioma("marca",$marca);
	    	#categoria producto ///genero
	    	$categoria_en = $this->Detalle_prod_model->consultarVAloresOtroIdioma("categoria_producto",$categorias);
	    	#colores 
	    	$colores_en = $this->Detalle_prod_model->consultarVAloresOtroIdioma("colores",$color);
	    	#tipo_producto  
	    	$tipo_producto = $this->Detalle_prod_model->consultarVAloresOtroIdioma("tipo_producto",$tipo_producto);
	    	$data = array(
	    					"marca"=> $marca_en["0"]->id_categoria_en,
	    					"categoria"=> $categoria_en["0"]->id_categoria_en,
	    					"colores"=>$colores_en["0"]->id_categoria_en,
	    					"tipo_producto"=>$tipo_producto["0"]->id_categoria_en
	    	);
	    	//----
	    	die(json_encode($data));
	    }
	}

?>
