<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes with
| underscores in the controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'WebInicio';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
/*--------- Rutas del cms ------------------*/
/* Para el cms*/
$route['cms'] = 'Login';
$route['cms/dashboard'] = 'Inicio';

/*---------Categorias------------------------*/
$route['cms/categorias'] = 'Categorias';
$route['cms/categorias/consultarCategorias']= 'Categorias/consultarCategorias';
$route['cms/categorias/categoriasVer']= 'categorias/categoriasVer';

/*---------Nosotros------------------------*/
$route['cms/nosotros'] = 'Nosotros';
$route['cms/nosotros/consultar_nosotros']= 'nosotros/consultar_nosotros';
$route['cms/nosotros/nosotrosVer']= 'nosotros/nosotrosVer';

/*---------Slider------------------------*/
$route['cms/slider'] = 'Slider';
$route['cms/slider/consultar_slider']= 'slider/consultar_slider';
$route['cms/slider/sliderVer']= 'slider/sliderVer';

/*---------Noticias------------------------*/
$route['cms/noticias'] = 'noticias';
$route['cms/noticias/consultarNoticias']= 'noticias/consultarNoticias';
$route['cms/noticias/noticiasVer']= 'noticias/noticiasVer';

/*-------- Contactos -----------------------*/
$route['cms/redes_sociales'] = 'RedesSociales';
$route['cms/contactos'] = 'Contactos';
$route['cms/footer'] = 'Footer';
$route['cms/footer/consultarFooter'] = 'footer/consultarFooter';
$route['cms/footer/footerVer']= 'footer/footerVer';

/*---------Productos------------------------*/
	/*---------Marcas----------*/
	$route['cms/linea'] = 'Marcas';
	$route['cms/linea/consultar_linea']= 'Marcas/consultar_marcas';
	$route['cms/linea/lineaVer']= 'Marcas/marcasVer';

	/*---------Categorias de Productos----------*/
	$route['cms/categoria_prod'] = 'Categoria_prod';
	$route['cms/categoria_prod/consultar_categoriasProd']= 'Categoria_prod/consultar_categoriasProd';
	$route['cms/categoria_prod/categoriaProdVer']= 'Categoria_prod/categoriaProdVer';

	/*---------Tipos de Productos----------*/
	$route['cms/tipo_prod'] = 'Tipo_prod';
	$route['cms/tipo_prod/consultar_tipoProd']= 'Tipo_prod/consultar_tipoProd';
	$route['cms/tipo_prod/tipoProdVer']= 'Tipo_prod/tipoProdVer';

	/*---------Tipos de Productos----------*/
	$route['cms/detalle_prod'] = 'Detalle_prod';
	$route['cms/detalle_prod/consultar_detProd']= 'Detalle_prod/consultar_detProd';
	$route['cms/detalle_prod/detalleProdVer']= 'Detalle_prod/detalleProdVer';
	$route['cms/detalle_prod/detalleProdVerClonar']= 'Detalle_prod/detalleProdVerClonar';
	
	/*---------Galeria Multimedia------------------------*/
	$route['cms/galeriaMultimedia'] = 'GaleriaMultimedia';
	/*---------Color------------------------*/
	$route['cms/color_prod'] = 'Colores';
	$route['cms/color_prod/consultar_color'] = 'Colores/consultar_colores';
	$route['cms/color_prod/colorVer']= 'Colores/coloresVer';
	/*---------Tallas------------------------*/
	$route['cms/tallas_prod'] = 'Tallas';
	$route['cms/tallas_prod/consultar_tallas'] = 'Tallas/consultar_tallas';
	$route['cms/tallas_prod/tallasVer']= 'Tallas/tallasVer';
	/*---------categoria_idiomas------------------------*/
	$route['cms/categoria_idiomas'] = 'Categoria_idiomas';
	$route['cms/categoria_idiomas/consultar'] = 'Categoria_idiomas/consultar_categoria_idiomas';

/*------------------------------------------*/
/*----------Rutas de la web ----------------*/
/*------------------------------------------*/
$route['/'] = 'WebInicio/index/1';
$route['inicio'] = 'WebInicio/index/1';
$route['home'] = 'WebInicio/index/2';

/* Social login */
$route['login/inicio_google'] = 'Social_login/google';
$route['login/inicio_facebook'] = 'Social_login/facebook';
$route['salir'] = 'Social_login/logout';


$route['nosotros'] = 'WebNosotros/index/1';
$route['about'] = 'WebNosotros/index/2';

$route['productos'] = 'WebProductos/index/1';
$route['productos/(:any)'] = 'WebProductos/verProductoDetalle/1/$1';

$route['products'] = 'WebProductos/index/2';
$route['products/(:any)'] = 'WebProductos/verProductoDetalle/2/$1';

$route['carrito'] = 'WebCart/verCarrito/1';
$route['cart'] = 'WebCart/verCarrito/2';

$route['orden'] = 'WebOrden/verOrden/1';
$route['order'] = 'WebOrden/verOrden/2';

$route['orden_usuario'] = 'WebOrdenUsuario/verOrdenUsuario/1';
$route['order_us'] = 'WebOrdenUsuario/verOrdenUsuario/2';

$route['procesar_compra/(:any)'] = 'WebCheckOut/verCheckOut/1/$1';
$route['checkout_step/(:any)'] = 'WebCheckOut/verCheckOut/2/$1';

$route['noticias'] = 'WebNoticias/index/1';
$route['noticias/(:any)'] = 'WebNoticias/verNoticiaDetalle/1/$1';
$route['news'] = 'WebNoticias/index/2';
$route['news/(:any)'] = 'WebNoticias/verNoticiaDetalle/2/$1';

$route['contactanos'] = 'WebContactos/index/2';
$route['contact_us'] = 'WebContactos/index/2';
$route['verproducto'] = 'WebNoticias/index/1';
