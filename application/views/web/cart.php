<div id="cuerpoProducto" ng-controller="carritoController">
  <!--PARALLAX-->
  <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
    <div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax1.jpg);"></div>

    <div class="container g-pt-100 g-pb-70">
      <div class="row">
        <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
          <div class="text-center">
            <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">{{titulos_home.cart}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
	<!-- MAIN CONTENT SECTION -->
  <section class="mainContent clearfix cartListWrapper">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="cartListInner">
            <div class="col-lg-12">
              <div id="campo_mensaje_productos" name="campo_mensaje_productos" ></div>
            </div>
            <form action="" id="formCarrito" name="formCarrito" ng-if="cuantos_productos>0">
                <!-- -->
                
                <!-- -->
                <div class="table-responsive">
                  <table class="table table-productos">
                    <thead>
                        <th></th>
                        <th>{{titulos_home.producto_nombre}}</th>
                        <th>{{titulos_home.producto_precio}}</th>
                        <th>{{titulos_home.producto_cantidad}}</th>
                        <th>Sub Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="producto in productos track by $index" class="tabla_producto" data="{{$index}}">
                        <td class="">
                          <div id="eliminar_{{$index}}" data-ng-click="eliminarProducto($event)"  data="{{producto.id_producto_carrito}}" class="close close-eliminar" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                          <span class="cartImage"><img src="{{base_url}}{{producto.imagenes[0].ruta}}" alt="image" class="img-carrito"></span>
                        </td>
                        <td class="">{{producto.titulo}}</td>
                        <td class="">
                            $ {{producto.monto_individual}}
                            <div id="monto_oculto_{{$index}}" class="invisible">{{producto.monto}}</div>
                        </td>

                        <td class="count-input">
                          <a class="incr-btn" data-action="decrease" href=""><i class="fa fa-minus"></i></a>
                          <input class="quantity" type="text" value="{{producto.cantidad}}" ng-model="producto.cantidad" id="cantidad_{{$index}}" data="{{$index}}" data-ng-change="calcularMontoTotal({{$index}})" onKeyPress="return valida(event,this,10,3)" onBlur="valida2(this,10,3);">
                          <a class="incr-btn" data-action="increase" href=""><i class="fa fa-plus"></i></a>
                           <input type="hidden" name="id_producto_carrito" name="id_producto_carrito" value="{{producto.id_producto_carrito}}">
                        </td>
                        <td class="">
                            <div>
                                <div class="simbolo_dolar">$</div>
                                <div id="monto_total_{{$index}}">
                                   {{producto.monto_total}}
                                </div>
                                <div style="clear: both;"></div>
                            </div>     
                             <div id="monto_total_oculto_{{$index}}"  class="invisible">{{producto.monto_total_oculto}}</div>
                             <div id="id_{{$index}}"  class="invisible">{{producto.id}}</div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="updateArea">
                  <div class="input-group">
                  </div>
                  <div class="float-right btn btn-secondary-outlined" ng-click="update_cart()">{{btn.update_cart}}</div>
                </div>
                <div class="row totalAmountArea">
                  <div class="col-sm-4 ml-sm-auto">
                    <ul class="list-unstyled">
                      <li>
                        <span class="tituloTotales">
                        Sub Total:
                        </span> 
                        <span>  
                            <div class="simbolo_dolar2">$</div>
                            <div id="subtotalMonto">{{productos[0].monto_global_total}}</div>
                            <div style="clear: both"></div>
                        </span>    
                      </li>
                      <li style="display: none">UK Vat <span>$ 18.00</span></li>
                      <li>
                        <span class="tituloTotales">
                        Total:
                        </span>
                        <span class="grandTotal">
                            <div class="simbolo_dolar2">$</div>
                            <div id="totalMonto">{{productos[0].monto_global_total}}</div>
                            <div style="clear: both"></div>
                        </span>
                            <div id="monto_total_oculto"  class="invisible">{{producto[0].monto_global_total_oculto}}</div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="checkBtnArea">
                  <div  class="btn btn-primary btn-default" ng-click="irOrden()">
                      {{btn.checkout}}<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                  </div>
                </div>
            </form>
           
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- -->
</div>