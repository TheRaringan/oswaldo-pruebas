<div id="cuerpoProducto" ng-controller="productoDetallesController">
  <!--PARALLAX-->
  <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
    <div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax1.jpg);"></div>

    <div class="container g-pt-100 g-pb-70">
      <div class="row">
        <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
          <div class="text-center">
            <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">{{titulos_home.productos}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
	<!-- MAIN CONTENT SECTION -->
  	<section class="mainContent clearfix">
    	<div class="container">
      		<div class="row singleProduct">
        		<div class="col-md-12">
          			<div class="media flex-wrap">
                  
                  <div class="media-left productSlider">
                      <!--<div id="carousel" class="carousel slide" data-ride="carousel">
                          <div class="carousel-inner">
                              <div class="carousel-item active" ng-repeat="imagen in sub_producto track by $index" data-thumb="{{$index}}">
                                  <img ng-src="{{base_url}}{{imagen.ruta}}" width="460" height="500">
                              </div>    
                          </div>
                      </div>
                      <div class="clearfix">
                          <div id="thumbcarousel" class="carousel slide" data-interval="false">
                              <div class="carousel-inner">
                                <div ng-repeat="imagen in sub_producto track by $index">  
                                  <div data-target="#carousel" data-slide-to="{{$index}}" class="thumb">
                                      <img ng-src="{{base_url}}{{imagen.ruta}}" width="460" height="500">
                                  </div>
                                </div>    
                              </div>
                              <a class="left carousel-control" href="single-product.html#thumbcarousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                              </a>
                              <a class="right carousel-control" href="single-product.html#thumbcarousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                              </a>
                          </div>
                      </div>-->
                      <!-- Imagenes principal -->
                      <div id="slider-ppal">
                      <!-- -->
                          <div class="row">
                              <div class="large-12 columns">
                                <div class="col-lg-12">
                                  <div id="slider" class="">
                                      <div ng-repeat="imagen in sub_producto track by $index" class="item img-slider-ppal" id="div_imagen{{$index}}" onmouseover="super_hover()">
                                          <img id="imagen{{$index}}" ng-src="{{base_url}}{{imagen.ruta}}" style="width:80%;height:501px;margin-right:2px">
                                      </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                      <!-- -->  
                      </div>
                      <hr>
                      <!--Slider imagenes inferior -->
                      <div id="slider-inf" class="col-lg-12">
                          <!-- -->
                          <div class="row">
                              <div class="col-xs-2" style="padding:2px;" ng-repeat="imagen in sub_producto track by $index">
                                  <img id="imagen_selector{{$index}}" class="imagen_galeria imagen_selector img-barra-slider" dat="#div_imagen{{$index}}" ng-src="{{base_url}}{{imagen.ruta}}" >
                              </div>
                              <div style="clear: both"></div>
                          </div> 
                          <hr>   
                          <!-- -->
                      </div>
                  </div>
                   
                  <br>
              		<div class="media-body cuerpo-detalle-producto">
                			<form id="formProducto" name="formProducto" target="_self">
                				<h2>{{producto.titulo}}</h2>
                				<h3>${{producto.precio}}</h3>
                				<h4 ng-bind-html="producto.descripcion"></h4>
                        <br>
                        <div>
                            <div id="cantidadDiv" name="cantidadDiv" style="display: none">{{producto.cantidad}}
                            </div>

                            <select id="cantidadProducto" name="cantidadProducto" class="form-control" placeholder="Cantidad">
                                <option>0</option>
                            </select>
                        </div>
                				<div class="">
                  				<div ng-click="agregarCarrito()" class="btn btn-primary btn-default">
                              Agregar
                              <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                          </div>
                				</div>
                				<div class="tabArea">
                    				<ul class="nav nav-tabs bar-tabs">
                      					<li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="single-product.html#details">Detalles</a>
                                </li>
                    				</ul>
                    				<div class="tab-content">
                      					<div id="details" class="tab-pane fade show active">
                      						<ul class="list-unstyled">
                        							<li>CATEGORIA: {{producto.categoria}}</li>
                        							<li>TIPO DE PRODUCTO: {{producto.tipo_producto}}</li>
                        							<li>COLOR: {{producto.color}}</li>
                        							<li>TALLA: {{producto.talla}}</li>
                                      <li>PRECIO: ${{producto.precio}}</li>

                      						</ul>
                      					</div>
                    				</div>
                            <div class="col-lg-12">
                                <div id="campo_mensaje_productos" name="campo_mensaje_productos" ></div>
                            </div>
                				</div>
                      </form>  
              		</div>
          			</div>
        		</div>
      		</div>

    	</div>
		<div id="slug_producto" name="slug_producto" style="display: none;"><?php echo $slug_producto; ?></div>
    <div id="id_producto" name="id_producto" style="display: none;">{{producto.id}}</div>
  	</section>
</div>