
<div ng-controller="MainController">
      <!-- BANNER -->
      <!-- -->
      <!-- Nuevo slider-->
      <div class="bd-example">
          <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
              <?php foreach ($slider as $clave => $valor) { ?>
                <li id="slide_<?php echo $valor->num;?>" data-target="#carouselExampleCaptions" data-slide-to="<?php echo $valor->num;?>"></li>
              <?php } ?>
            </ol>
            <div class="carousel-inner">
              <?php foreach ($slider as $clave => $valor) { ?>
                <div id="div_carousel_<?php echo $valor->num;?>" class="item carousel-item g-bg-cover g-bg-black-opacity-0_3--after">
                  <img src="<?php echo $valor->ruta;?>" class="d-block w-100 img-slider" alt="...">
                  <div class="carousel-caption d-none d-md-block">
                    <h5 class="title_carrousel"><?php echo $valor->titulo;?></h5>
                    <!--<p class="sub_title_carrousel"><?php echo $valor->descripcion;?></p>-->
                  </div>
                </div>
              <?php } ?>  
            </div>
            <a class="carousel-control-prev" data-target="#carouselExampleCaptions" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" data-target="#carouselExampleCaptions" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
      </div>
      <!-- MAIN CONTENT SECTION -->
      <section class="mainContent clearfix aboutUsInfo">
        <div class="container">
            <div class="page-header texto-about-us fadeInUp wow">
              <h1>{{titulos_home.somos1}}</h1>
              <h3>{{titulos_home.mision1}}</h3>
              <h5>{{nombre_apellido}}</h5>
            </div>
            <div class="row">
              <div class="col-md-6 order-sm-12 fadeInRight wow">
                <img ng-src="{{base_url}}{{nosotros.ruta}}" alt="about-us-img">
              </div>
              <div class="col-md-6 order-sm-1 fadeInLeft wow">
                <div class="lead parrafos" ng-bind-html="nosotros.somos"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mgtop-25">
                <div class="lead parrafos" ng-bind-html="nosotros.digital_agency"></div>
              </div>
            </div>
            <div class="centrar-div fadeInUp wow">
                <button type="button" class="btn btn-primary btn-rounded centrar-div btn-about ">{{btn.contacto}}</button>
            </div>
        </div>
    </section>
      <!-- Seccion de productos-->
      <!--PARALLAX-->
    <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
        <div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax1.jpg);"></div>
        <div class="container g-pt-100 g-pb-70">
            <div class="row">
                <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
                    <div class="text-center">
                        <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">{{titulos_home.productos}} </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- -->
      <!-- seccion productos -->
    <section class="mainContent clearfix">
        <div class="container">
            <div class="page-header">
                <h4>{{titulos_home.productos2}}</h4>
            </div>
            <div class="row featuredCollection margin-bottom fadeInUp wow">
                <div class="col-md-4 col-12" ng-repeat="notice in productos track by $index">
                    <div class="thumbnail">
                        <div class="imageWrapper">
                            <img src="<?=base_url();?>{{notice.ruta}}" alt="feature-collection-image" class="img-producto ">
                            <div class="caption">
                                <h3 class="color-producto">{{notice.titulo}}</h3>
                                <small class="color-producto">Desde ${{notice.precio}}</small>
                            </div>
                            <div class="masking">
                                <a href="{{base_url}}{{url.menu3}}/{{notice.slug}}" class="btn viewBtn">{{btn.ver_producto}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="centrar-div">
                <a href="{{base_url}}{{url.menu3}}" target="_self" style="display: flex; margin: 0 auto;">
                    <button type="button" class="btn btn-primary btn-rounded centrar-div btn-about">{{btn.ver_mas}}</button>
              </a>
            </div>
        </div>
    </section>
      <!--Fin de seccion productos -->

      <!--PARALLAX-->
      <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
      <!-- stylesheets -->
        <div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax2.jpg);"></div>

        <div class="container g-pt-100 g-pb-70">
          <div class="row">
            <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
              <div class="text-center">
                <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff"> {{titulos_home.noticias}} </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- -->
      <!-- seccion noticias -->
      <section class="mainContent clearfix">
        <div class="container">
          <div class="page-header">
            <h4>{{titulos_home.noticias2}}</h4>
          </div>
          <div class="row latestArticles fadeInRight wow">
            <div class="col-md-4" ng-repeat="notice in sub_noticias track by $index">
              <div class="thumbnail">
                <a href="blog-single-right-sidebar.html">
                  <img src="<?=base_url();?>{{notice.ruta}}" alt="article-image" style="height: 233px;">
                </a>
                <div class="date-holder">
                  <p>{{notice.dias}}</p>
                  <span>{{notice.mes}}</span>
                </div>
                <h5><a href="{{base_url}}{{url.menu4}}/{{notice.slug}}">{{notice.titulo}}</a></h5>
                <span class="meta"> by <a class="pr-1" href="{{notice.slug}}">{{notice.usuario}}</a> </span>
                <div class="caption">
                  <p>{{notice.descripcion_sin_html}}</p>
                </div>
              </div>
            </div>
          </div>
          <div class="centrar-div fadeInUp wow">
              <button type="button" class="btn btn-primary btn-rounded centrar-div btn-about">{{btn.leer_mas}}</button>
          </div>
        </div>
      </section>
      <!-- -->
      <!--PARALLAX-->
      <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
      <!-- stylesheets -->
        <div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax4.jpg);"></div>

        <div class="container g-pt-100 g-pb-70">
          <div class="row">
            <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
              <div class="text-center">
                <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff"> {{titulos_home.redes_sociales}} </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section class="mainContent clearfix">
          <div class="container">
            <div class="page-header fadeInUp wow">
              <h4> {{titulos_home.redes_sociales2}} </h4>
            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 padding0 fadeInUp wow">
            <div style="" class="cuerpo_redes col-lg-12 col-md-12 col-xs-12 col-sm-12">
              <div class="col-4  padding0 redes_individual" style="float:left;display: flex; margin: 0 auto;" title="Facebook">
                <a href="{{redes[1].url_red}}" target="_blank"  class="redes_individual" style="display: flex;margin: 0 auto">
                  <div class="contenedor_icono_pasos">
                    <div class="iconos_pasos iconos_redes">
                      <i class="fa fa-facebook icono-zoug" aria-hidden="true"></i>
                    </div>
                  </div>
                </a>
                <div style="clear:both"></div>
              </div>

              <div class="col-4 padding0 redes_individual" style="float:left;display: flex; margin: 0 auto;">
                <a href="{{redes[0].url_red}}"  target="_blank" class="redes_individual" style="display: flex;margin: 0 auto">
                  <div class="contenedor_icono_pasos">
                    <div class="iconos_pasos iconos_redes">
                      <i class="fa fa-instagram icono-zoug" aria-hidden="true" ></i>
                    </div>
                  </div>
                </a>
                <div style="clear:both"></div>
              </div>

              <div class="col-4 padding0 redes_individual"  style="float:left;display: flex; margin: 0 auto;">
                <a href="{{redes[2].url_red}}" target="_blank" class="redes_individual" style="float:left;display: flex; margin: 0 auto;">
                  <div class="contenedor_icono_pasos">
                    <div class="iconos_pasos iconos_redes icono-zoug" >
                      <i class="fa fa-twitter" aria-hidden="true"></i>
                    </div>
                  </div>
                </a>
                <div style="clear:both"></div>
              </div>

              <div style="clear:both"></div>
            </div>
            <!-- Bloque de facebook -->
            <div class="container">
            <!-- -->
                <div class="row">
                  <!-- LightWidget WIDGET --><script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/7f838065d00c58daa16b68fcb0bec567.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
                  <!-- -->
                </div>  
            </div>
            <div style="clear:both"></div>
      </section>
      <!--PARALLAX-->
      <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
      <!-- stylesheets -->
        <div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax3.jpg);"></div>

        <div class="container g-pt-100 g-pb-70">
          <div class="row">
            <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
              <div class="text-center">
                <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">{{titulos_home.contactanos}}</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section class="mainContent clearfix">
          <div class="container">
            <div class="page-header fadeInUp wow">
              <h4>{{titulos_home.contactanos2}}</h4>
            </div>
            <div class="display-single_element fadeInDown wow">
                <form >
                  <div class="row">
                    <div class="form-group col-md-6">
                      <input type="text" class="form-control" id="exampleInputName" aria-describedby="userName" placeholder="{{btn.place_h1}}" ng-model="contactos.nombres" >
                    </div>
                    <div class="form-group col-md-6">
                      <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="{{btn.place_h2}}" ng-model="contactos.email"  onKeyPress="return valida(event,this,5,50)" onBlur="valida2(this,5,50);correo(this,'campo_mensaje_clientes')" maxlength="50" id="contactos_email" name="contactos_email">
                    </div>
                    <div class="form-group col-md-12">
                      <input type="number" class="form-control" id="contactos_telefono" name="contactos_telefono" placeholder="{{btn.place_h3}}" ng-model="contactos.telefono" onKeyPress="return valida(event,this,21,14)" onBlur="valida2(this,21,14);formato_tlf(this,'campo_mensaje_clientes');" >
                    </div>
                    <div class="form-group col-md-12">
                       <textarea class="form-control" id="exampleTextarea" rows="5" placeholder="{{btn.place_h4}}" ng-model="contactos.mensaje" id="mensaje" name="mensaje"></textarea>
                    </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12" style="">
                      <div id="campo_mensaje_clientes" ></div>
                  </div>
                  <button type="submit" class="btn btn-default btn-primary fadeInUp wow" ng-click="registrar_contactos()">{{btn.enviar}}</button>
                </form>
            </div>
          </div>
      </section>
      <!-- -->
</div>


