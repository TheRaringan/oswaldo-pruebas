<input type="hidden" name="id_idioma" id="id_idioma" value="<?php if(isset($idioma)){echo $idioma;}?>">
<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
        <!-- FOOTER -->
        <div id="footer" style="display: none">
            <div class="footer clearfix">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-12">
                            <div class="newsletter clearfix">
                              <div>
                                  <img src="<?=base_url();?>/assets/web/img/logo.png" class="logo-footer">
                              </div>
                              <!--<p>{{footer.descripcion}}</p>-->
                            </div>
                        </div>
                        <div class="col-md-2 col-12">
                            <div class="footerLink">
                                <h5>{{btn.navegacion}}</h5>
                                <ul class="list-unstyled">
                                    <li><a href="{{base_url}}{{url.menu1}}">{{menu.menu1}} </a></li>
                                    <li><a href="{{base_url}}{{url.menu2}}">{{menu.menu2}} </a></li>
                                    <li><a href="{{base_url}}{{url.menu3}}">{{menu.menu3}}</a></li>
                                    <li><a href="{{base_url}}{{url.menu4}}">{{menu.menu4}}</a></li>
                                    <li><a href="{{base_url}}{{url.menu5}}">{{menu.menu5}}</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2 col-12">
                            <div class="footerLink">
                              <h5>Productos</h5>
                              <ul class="list-unstyled">
                                <li><a href="index.html#">Lorem ipsum dolor </a></li>
                                <li><a href="index.html#">Lorem ipsum dolor </a></li>
                                <li><a href="index.html#">Lorem ipsum dolor </a></li>
                                <li><a href="index.html#">Lorem ipsum dolor </a></li>
                              </ul>
                            </div>
                        </div>

                        <div class="col-md-2 col-12">
                            <div class="footerLink">
                                <h5>{{btn.pagos}}</h5>
                                <ul class="list-inline">
                                    <li>
                                      <a href="index.html#">
                                        <i class="fa fa-paypal"></i><span style="margin-left: 5px">Paypal</span>
                                      </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2 col-12">
                            <div class="footerLink">
                                <h5>{{menu.menu5}}</h5>
                                <ul class="list-unstyled">
                                    <li><a href="mailto:{{footer.correo}}">
                                        <span class="email-zugzug">{{footer.correo}}</span></a>
                                    </li>
                                </ul>
                                <ul class="list-inline">
                                    <li><a href="{{redes[1].url_red}}"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="{{redes[0].url_red}}"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="{{redes[2].url_red}}"><i class="fa fa-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- COPY RIGHT -->
            <div class="copyRight clearfix">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-7 col-12">
                                <p>{{btn.footer}}</p>
                            </div>
                            <div class="col-md-5 col-12">
                                <ul class="list-inline">
                                </ul>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <!-- LOGIN MODAL -->
    <div class="modal fade login-modal" id="login" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header justify-content-center">
                    <h3 class="modal-title">log in</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="index.html" method="POST" role="form">
                        <div class="form-group">
                            <label for="">Enter Email</label>
                            <input type="email" class="form-control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" class="form-control" id="">
                        </div>
                        <div class="checkbox">
                            <input id="checkbox-1" class="checkbox-custom form-check-input" name="checkbox-1" type="checkbox" checked>
                            <label for="checkbox-1" class="checkbox-custom-label form-check-label">Remember me</label>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">log in</button>
                        <button type="button" class="btn btn-link btn-block">Forgot Password?</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- SIGN UP MODAL -->
    <div class="modal fade " id="signup" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header justify-content-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title">Create an account</h3>
                </div>
                <div class="modal-body">
                    <form action="index.html" method="POST" role="form">
                        <div class="form-group">
                            <label for="">Enter Email</label>
                            <input type="email" class="form-control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" class="form-control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Confirm Password</label>
                            <input type="password" class="form-control" id="">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Sign up</button>
                        <button type="button" class="btn btn-link btn-block">New User?</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- PORDUCT QUICK VIEW MODAL -->
    <div class="modal fade quick-view" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="media flex-wrap">
                        <div class="media-left px-0">
                            <img class="media-object" src="<?=base_url();?>/assets/web/img/products/quick-view/quick-view-01.jpg" alt="Image">
                        </div>
                        <div class="media-body">
                            <h2>Old Skool Navy</h2>
                            <h3>$149</h3>
                            <p>Classic sneaker from Vans. Cotton canvas and suede upper. Textile lining with heel stabilizer and ankle support. Contrasting laces. Sits on a classic waffle rubber sole.</p>
                            <span class="quick-drop">
                                <select name="guiest_id3" id="guiest_id3" class="select-drop">
                                    <option value="0">Size</option>
                                    <option value="1">Size 1</option>
                                    <option value="2">Size 2</option>
                                    <option value="3">Size 3</option>
                                </select>
                            </span>
                            <span class="quick-drop resizeWidth">
                                <select name="guiest_id4" id="guiest_id4" class="select-drop">
                                    <option value="0">Qty</option>
                                    <option value="1">Qty 1</option>
                                    <option value="2">Qty 2</option>
                                    <option value="3">Qty 3</option>
                                </select>
                            </span>
                            <div class="btn-area">
                                <a href="index.html#" class="btn btn-primary btn-block">Add to cart <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
        <script src="<?=base_url();?>assets/web/plugins/jquery/jquery.min.js"></script>
        <script src="<?=base_url();?>assets/web/plugins/jquery/jquery-migrate-3.0.0.min.js"></script>
        <!-- -->
        <script src="<?=base_url();?>assets/web/plugins/zoom-master/jquery-zoom.js"></script>
        <!-- -->
        <script src="<?=base_url();?>assets/web/plugins/jquery-ui/jquery-ui.js"></script>
        <script src="<?=base_url();?>assets/web/plugins/bootstrap/js/popper.min.js"></script>
        <script src="<?=base_url();?>assets/web/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?=base_url();?>assets/web/plugins/wow/wow.min.js"></script>
        <script src="<?=base_url();?>assets/web/plugins/carrousel/carrousel.js"></script>     
        <script src="<?=base_url();?>assets/web/plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?=base_url();?>assets/web/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?=base_url();?>assets/web/plugins/slick/slick.js"></script>
        <script src="<?=base_url();?>assets/web/plugins/fancybox/jquery.fancybox.min.js"></script>
        <script src="<?=base_url();?>assets/web/plugins/iziToast/js/iziToast.js"></script>
        <script src="<?=base_url();?>assets/web/plugins/prismjs/prism.js"></script>
        <script src="<?=base_url();?>assets/web/plugins/selectbox/jquery.selectbox-0.1.3.min.js"></script>
        <script src="<?=base_url();?>assets/web/plugins/countdown/jquery.syotimer.js"></script>
        <script src="<?=base_url();?>assets/web/plugins/velocity/velocity.min.js"></script>
    <!-- JS -->
    <script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsparallaxer.js"></script>
    <script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsscroller/scroller.js"></script>
    <script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/advancedscroller/plugin.js"></script>
   
    <script src="<?=base_url();?>assets/web/js/custom.js"></script>
    <script src="<?=base_url();?>assets/web/js/fbasic.js"></script>
    <!-- 
    <script src="<?=base_url();?>assets/web/js/owl.carousel.min.js"></script>
    -->
    <!--Core Angular JS -->
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/angular.min.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/angular-route.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/angular-sanitize.min.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/app.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/directivas/directives.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/servicios/services.js"></script>
    <!--Controladores Ng -->
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/inicioController.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/MainController.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/nosotrosController.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/noticiasController.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/noticiasDetallesController.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/contactanosController.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/productosController.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/productoDetallesController.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/carritoController.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/orderController.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/orderUsuarioController.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/checkoutController.js"></script>
    <!-- -->
</html>
