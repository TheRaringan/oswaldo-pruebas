 <!--<div id="rev_slider_1" class="bannercontainer bannerV1">
        <div class="fullscreenbanner-container">
          <div class="fullscreenbanner">
            <ul>
                <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700" data-title="Slide 1">
                  <div class="slider-caption slider-captionV1 container">
                      <div class="tp-caption rs-caption-1 sft start" style="left:0px;"
                        data-hoffset="0"
                        data-x="0px"
                        data-y="0"
                        data-speed="800"
                        data-start="1500"
                        data-easing="Back.easeInOut"
                        data-endspeed="300" >
                        <img src="<?=base_url();?>/assets/web/img/slider/slider1.jpg" alt="slider-image" style="width: auto; height: auto;left:0px;">
                      </div>

                      <div class="tp-caption rs-caption-2 sft"
                        data-hoffset="0"
                        data-y="100"
                        data-x="600"
                        data-speed="800"
                        data-start="2000"
                        data-easing="Back.easeInOut"
                        data-endspeed="300">
                        <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;">Nuevo producto</span>
                      </div>

                      <div class="tp-caption rs-caption-3 sft"
                        data-hoffset="0"
                       data-y="100"
                        data-x="600"
                        data-speed="1000"
                        data-start="3000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off" style="width: 90vh;">
                        <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;">Desde - $59.00</span><br>
                        <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque convallis turpis pharetra pretium nec eu sem.</span>
                      </div>
                      <div class="tp-caption rs-caption-4 sft"
                        data-hoffset="0"
                        data-y="310"
                        data-x="600"
                        data-speed="800"
                        data-start="3500"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off">
                        <span class="page-scroll"><a href="index.html#" class="btn primary-btn" style="border: solid #b9ca46;margin-top:20px;">Comprar<i class="fa fa-chevron-right"></i></a></span>
                      </div>
                  </div>
                </li>
                slider con Angular Js
                <li ng-repeat="slide in slider track by $index" data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700"  data-title="Slide {{slide.num}}">
                    <div class="slider-caption slider-captionV1 container">
                        <div class="tp-caption rs-caption-1 sft start" style="left:0px;"
                          data-hoffset="0"
                          data-x="0px"
                          data-y="0"
                          data-speed="800"
                          data-start="1500"
                          data-easing="Back.easeInOut"
                          data-endspeed="300">
                          <img ng-src="{{base_url}}{{slide.ruta}}" alt="slider-image" style="width: auto; height: auto;left:0px;">
                        </div>
                        <div class="tp-caption rs-caption-2 sft"
                          data-hoffset="0"
                          data-y="100"
                          data-x="600"
                          data-speed="800"
                          data-start="2000"
                          data-easing="Back.easeInOut"
                          data-endspeed="300">
                          <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;">{{slide.titulo}}</span>
                        </div>
                        <div class="tp-caption rs-caption-3 sft"
                          data-hoffset="0"
                          data-y="100"
                          data-x="600"
                          data-speed="1000"
                          data-start="3000"
                          data-easing="Power4.easeOut"
                          data-endspeed="300"
                          data-endeasing="Power1.easeIn"
                          data-captionhidden="off" style="width: 90vh;">
                          <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;" ng-bind-html="slide.descripcion"></span>
                        </div>
                        <div class="tp-caption rs-caption-4 sft"
                          data-hoffset="0"
                          data-y="310"
                          data-x="600"
                          data-speed="800"
                          data-start="3500"
                          data-easing="Power4.easeOut"
                          data-endspeed="300"
                          data-endeasing="Power1.easeIn"
                          data-captionhidden="off">
                          <span class="page-scroll"><a href="{{base_url}}{{slide.url}}" class="btn primary-btn" style="border: solid #b9ca46;margin-top:20px;" ng-if="slide.boton!=''">{{slide.boton}}<i class="fa fa-chevron-right"></i></a></span>
                        </div>
                    </div>
                </li>
                <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700"  data-title="Slide 2">
                  <div class="slider-caption slider-captionV1 container">
                    <div class="tp-caption rs-caption-1 sft start" style="left:0px;"
                      data-hoffset="0"
                      data-x="0px"
                      data-y="0"
                      data-speed="800"
                      data-start="1500"
                      data-easing="Back.easeInOut"
                      data-endspeed="300">
                      <img src="<?=base_url();?>/assets/web/img/slider/slider2.jpg" alt="slider-image" style="width: auto; height: auto;left:0px;">
                    </div>

                    <div class="tp-caption rs-caption-2 sft "
                      data-hoffset="0"
                      data-y="100"
                      data-x="600"
                      data-speed="800"
                      data-start="2000"
                      data-easing="Back.easeInOut"
                      data-endspeed="300">
                      <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;">Pharetra pretium</span>
                    </div>

                    <div class="tp-caption rs-caption-3 sft"
                      data-hoffset="0"
                      data-y="175"
                      data-x="600"
                      data-speed="1000"
                      data-start="3000"
                      data-easing="Power4.easeOut"
                      data-endspeed="300"
                      data-endeasing="Power1.easeIn"
                      data-captionhidden="off" style="width: 90vh;">
                      <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;">Desde - $19.00</span><br>
                      <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque convallis turpis pharetra pretium nec eu sem.</span>
                    </div>

                    <div class="tp-caption rs-caption-4 sft"
                      data-hoffset="0"
                      data-y="310"
                      data-x="600"
                      data-speed="800"
                      data-start="3500"
                      data-easing="Power4.easeOut"
                      data-endspeed="300"
                      data-endeasing="Power1.easeIn"
                      data-captionhidden="off">
                      <span class="page-scroll"><a href="index.html#" class="btn primary-btn" style="border: solid #b9ca46;margin-top:20px;">Comprar<i class="fa fa-chevron-right"></i></a></span>
                    </div>
                  </div>
                </li>
                <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700" data-title="Slide 3">
                    <div class="slider-caption slider-captionV1 container">
    
                      <div class="tp-caption rs-caption-1 sft start"
                       data-hoffset="0"
                      data-x="0px"
                      data-y="0"
                      data-speed="800"
                      data-start="1500"
                      data-easing="Back.easeInOut"
                      data-endspeed="300" style="left:0px;">
                         <img src="<?=base_url();?>/assets/web/img/slider/slider3.jpg" alt="slider-image" style="width: auto; height: auto;left:0px;">
                      </div>
    
                      <div class="tp-caption rs-caption-2 sft"
                        data-hoffset="0"
                        data-y="100"
                        data-x="[15,15,42,15]"
                        data-speed="800"
                        data-start="2000"
                        data-easing="Back.easeInOut"
                        data-endspeed="300">
                         <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;"> Quisque convallis</span>
                      </div>
    
                      <div class="tp-caption rs-caption-3 sft"
                        data-hoffset="0"
                        data-y="175"
                        data-x="[15,15,42,15]"
                        data-speed="1000"
                        data-start="3000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off" style="width: 90vh;"s>
                        <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;">Desde - $259.00</span><br>
                        <span style="color:#fff;font-weight: bold;text-shadow: 2px 2px #000;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque convallis turpis pharetra pretium nec eu sem.</span>
                      </div>
                      <div class="tp-caption rs-caption-4 sft"
                        data-hoffset="0"
                        data-y="310"
                        data-x="[15,15,42,15]"
                        data-speed="800"
                        data-start="3500"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off">
                       <span class="page-scroll"><a href="index.html#" class="btn primary-btn" style="border: solid #b9ca46;margin-top:20px;">Comprar<i class="fa fa-chevron-right"></i></a></span>
                      </div>
                    </div>
                </li>
            </ul>
          </div>
        </div>
      </div>-->