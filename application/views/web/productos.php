<div id="cuerpoProductos" ng-controller="productosController">
	<!--PARALLAX-->
	<div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
		<div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax1.jpg);"></div>

		<div class="container g-pt-100 g-pb-70">
		  <div class="row">
		    <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
		      <div class="text-center">
		        <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">{{titulos_home.productos}}</h1>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
	<!-- MAIN CONTENT SECTION -->
	<section class="mainContent clearfix productsContent">
		<div class="container">
		  	<div class="row">
		    	<div class="col-lg-3 col-md-4 sideBar">
					<div class="panel panel-default">
		        		<div class="panel-heading panel-productos"><i class="fa fa-user" style="font-size: 15px"></i> - {{titulo_linea}}</div>
	        			<div class="panel-body">
	          				<div class="collapse navbar-collapse navbar-ex1-collapse navbar-side-collapse">
	            				<ul class="nav navbar-nav side-nav primero" ng-repeat="l in linea track by $index">
	              					<li id="li_linea_{{$index}}" onclick="marcarLineaProducto(this,'.linea')" class="linea">
	                					<a ng-click="filtro_linea(l.id);esconder();">{{l.titulo}}</a>
	              					</li>
	            				</ul>
	          				</div>
	        			</div>
		      		</div>
		      		<div class="panel panel-default">
		        		<div class="panel-heading panel-productos"><i class="fas fa-venus-mars" style="font-size: 12px"></i> - {{titulo_genero}}</div>
	        			<div class="panel-body">
	          				<div class="collapse navbar-collapse navbar-ex1-collapse navbar-side-collapse">
	            				<ul class="nav navbar-nav side-nav primero"  ng-repeat="g in genero track by $index">
	              					<li id="li_genero_{{$index}}" onclick="marcarLineaProducto(this,'.genero')" class="genero">
	                					<a ng-click="filtro_genero(g.id); esconder();">{{g.titulo}}</a>
	              					</li>
	            				</ul>
	          				</div>
	        			</div>
		      		</div>
	      			<div class="panel panel-default priceRange">
	        			<div class="panel-heading panel-productos"><i class="fas fa-dollar-sign" style="font-size: 15px" ></i> - {{titulo_precio}}</div>
        				<div class="panel-body clearfix">
          					<div class="price-slider-inner">
            					<h2 class="amount-wrapper">
              						<input type="text" id="price-amount-1" readonly>$
              						<strong>-</strong>
              						<input type="text" id="price-amount-2" readonly>$
									<strong>{{nombre_rango}}</strong>
            					</h2>
            					<div id="price-range"></div>
          					</div>
          						<input class="btn-default btn-primary " type="submit" value="Filter" ng-click="filtro_precio(); esconder();">
        				</div>
	      			</div>

					<div class="panel panel-default">
		        		<div class="panel-heading panel-productos"><i class="fas fa-swatchbook" style="font-size: 13px"></i> - {{titulo_color}}</div>
	        			<div class="panel-body">
	          				<div class="collapse navbar-collapse navbar-ex1-collapse navbar-side-collapse">
	            				<ul class="nav navbar-nav side-nav"  ng-repeat="c in color track by $index">
	              					<li id="li_color_{{$index}}" onclick="marcarLineaProducto(this,'.color')" class="color">
	                					<a ng-click="filtro_color(c.id); esconder();">{{c.descripcion}}</a>
	              					</li>
	            				</ul>
	          				</div>
	        			</div>
		      		</div>

					<div class="panel panel-default">
		        		<div class="panel-heading panel-productos"><i class="fas fa-ruler-combined" style="font-size: 13px"></i> - {{titulo_talla}}</div>
	        			<div class="panel-body">
	          				<div class="collapse navbar-collapse navbar-ex1-collapse navbar-side-collapse">
	            				<ul class="nav navbar-nav side-nav"  ng-repeat="t in talla track by $index">
	              					<li id="li_talla_{{$index}}" onclick="marcarLineaProducto(this,'.talla')" class="talla">
	                					<a ng-click="filtro_talla(t.id); esconder();">{{t.descripcion}}</a>
	              					</li>
	            				</ul>
	          				</div>
	        			</div>
		      		</div>

		      		<div>
	      				<a href="{{base_url}}{{url.menu3}}" target="_self" style="display: flex; margin: 0 auto;">
		                  <button type="button" class="btn btn-primary btn-rounded centrar-div btn-about">{{btn.limpiar_filtro}}</button>
		              	</a>
		      		</div>
	    		</div>

		    	<div class="col-lg-9 col-md-8">
					<div class="row" ng-hide="variable">
		        		<div class="col-md-6 col-lg-4" ng-repeat="producto in sub_productos track by $index">
		          			<div class="productBox">
		            			<div class="productImage clearfix">
		              				<img src="<?=base_url();?>{{producto.ruta}}" alt="products-img">
	              					<div class="productMasking">
						                <ul class="list-inline btn-group" role="group">
	                  						<li><a href="javascript:void(0)" class="btn btn-default" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!" class="btn btn-default"><i class="fa fa-shopping-bag icono-productos"></i></a></li>
	                  						<li><a href="{{base_url}}{{url.menu3}}/{{producto.slug}}"><i class="fa fa-eye icono-productos"></i></a></li>
	                					</ul>
	              					</div>
		            			</div>
		            			<div class="productCaption clearfix">
		              				<a href="{{base_url}}{{url.menu3}}/{{producto.slug}}">
		                				<h5>{{producto.titulo}}</h5>
		              				</a>
		              				<h3>Desde ${{producto.precio}}</h3>
		            			</div>
		          			</div>
		        		</div>
						<div class="col-lg-12">
							<div id="campo_mensaje_productos" name="campo_mensaje_productos" ></div>
						</div>
						<div class="centrar-div fadeInUp wow" >
						    <button type="button" id="cargar_mas" class="btn btn-primary btn-rounded centrar-div btn-about" ng-click="cargar_mas_productos()">{{btn.ver_mas}}</button>
						</div>
		      		</div>

					<div class="row" ng-hide="variable2">
						<div class="col-lg-12">
							<div id="campo_mensaje" name="campo_mensaje" ></div>
						</div>
		        		<div class="col-md-6 col-lg-4" ng-repeat="producto in sub_productos_filtro track by $index">
		          			<div class="productBox">
		            			<div class="productImage clearfix">
		              				<img src="<?=base_url();?>{{producto.ruta}}" alt="products-img">
	              					<div class="productMasking">
						                <ul class="list-inline btn-group" role="group">
	                  						<li><a href="javascript:void(0)" class="btn btn-default" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!" class="btn btn-default"><i class="fa fa-shopping-bag icono-productos"></i></a></li>
	                  						<li><a class="btn btn-default" onclick="prueba();" data-toggle="modal" href="{{base_url}}{{url.menu3}}/{{producto.slug}}"><i class="fa fa-eye icono-productos"></i></a></li>
	                					</ul>
	              					</div>
		            			</div>
		            			<div class="productCaption clearfix">
		              				<a href="{{base_url}}{{url.menu3}}/{{producto.slug}}">
		                				<h5>{{producto.titulo}}</h5>
		              				</a>
		              				<h3>Desde ${{producto.precio}}</h3>
		            			</div>
		          			</div>
		        		</div>
		      		</div>
		    	</div>
		  	</div>
		</div>
	</section>
</div>
