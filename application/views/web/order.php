<div id="cuerpoProducto" ng-controller="orderController">
  <!--PARALLAX-->
  <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
    <div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax1.jpg);"></div>

    <div class="container g-pt-100 g-pb-70">
      <div class="row">
        <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
          <div class="text-center">
            <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">{{titulos_home.order_titulo}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
	<!-- MAIN CONTENT SECTION -->
  <section class="mainContent clearfix cartListWrapper">
    <div class="container">
      <!-- -->
      <div class="row">
        <div class="col-md-8">
          <div class="innerWrapper clearfix stepsPage">              
            
            <div class="page-header mb-4">
              <h4>{{titulos_home.vista_orden}}</h4>
            </div>

            <div class="cartListInner review-inner row">
              <form action=""  id="formOrderProducto" name="formOrderProducto" class="col-sm-12">
                <input  type="text" id="idCarrito" name="idCarrito" ng-model="id_carrito">
                <div class="table-responsive">
                  <table class="table table-productos">
                    <thead>
                      <tr>
                        <th></th>
                        <th>{{titulos_home.producto_nombre}}</th>
                        <th>{{titulos_home.producto_precio}}</th>
                        <th>{{titulos_home.producto_cantidad}}</th>
                        <th>Sub Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="producto in productos track by $index" class="tabla_producto" data="{{$index}}">
                          <td class="">
                            <span class="cartImage">
                                <img src="{{base_url}}{{producto.imagenes[0].ruta}}" alt="image" class="img-carrito">
                            </span>
                          </td>
                          <td class="">{{producto.titulo}}</td>
                          <td class=""> ${{producto.monto_individual}}</td>
                          <td class="count-input">
                            <input class="quantity" type="text" value="{{producto.cantidad}}" disabled>
                          </td>
                          <td class="">
                              ${{producto.monto_total}}
                              <div id="monto_total_oculto_{{$index}}"  class="invisible">{{producto.monto_total_oculto}}</div>
                          </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </form>
            </div>

            <div class="row shipping-info metodo-pago">
                <div class="col-md-4">
                  <h5>{{titulos_home.producto_metodo_pago}}:</h5>
                   Paypal
                </div>
            </div>
            
            <div class="well well-lg clearfix">
              <ul class="pager">
              <li class="previous float-left">
                  <div class="btn btn-secondary btn-default float-left" ng-click="backpageCart()">back</div>
              </li>
                <li class="next">
                    <div class="btn btn-primary btn-default float-right" ng-click="FrompageCheckout()">
                        Continue <i class="fa fa-angle-right"></i>
                    </div>
                </li>
              </ul>
            </div>

          </div>
        </div>
        <div class="col-md-4">
          <div class="summery-box">
            <h4>{{titulos_home.resumen_orden}}</h4>
            <p></p>
            <ul class="list-unstyled">
              <li class="d-flex justify-content-between li-subtotal">
                <span class="tag"></span>
                <span class="val">${{productos[0].monto_global_total}}</span>
              </li>
              <!--<li class="d-flex justify-content-between">
                <span class="tag">Shipping & Handling</span>
                <span class="val">$12.00 </span>
              </li>
              <li class="d-flex justify-content-between">
                <span class="tag">Estimated Tax</span>
                <span class="val">$0.00 </span>
              </li>-->
              <li class="d-flex justify-content-between total-summary">
                <span class="tag">Total</span>
                <span class="val">USD  ${{productos[0].monto_global_total}} </span>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- -->	
    </div>
  </section>
  <!-- -->
</div>
