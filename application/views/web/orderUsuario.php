<div id="cuerpoProducto" ng-controller="orderUsuarioController">
  <!--PARALLAX-->
  <div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
    <div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax1.jpg);"></div>

    <div class="container g-pt-100 g-pb-70">
      <div class="row">
        <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
          <div class="text-center">
            <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">{{titulos_home.order_usuario_titulo}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
	<!-- MAIN CONTENT SECTION -->
  <section class="mainContent clearfix cartListWrapper">
    <div class="container">
      <!-- -->
      <div class="row">
          <div class="col-lg-12">
            <div id="campo_mensaje_productos" name="campo_mensaje_productos" ></div>
          </div>
          <div class="col-md-8" ng-if="id_usuario!=''">
              <div class="innerWrapper clearfix stepsPage">              
                <div class="page-header mb-4">
                  <h4>{{titulos_home.vista_orden_usuario}}</h4>
                </div>
              </div>
              <div class="cartListInner review-inner row">
                  <form action=""  id="formOrderProducto" name="formOrderProducto" class="col-sm-12">
                    <input  type="text" id="idCarrito" name="idCarrito" ng-model="id_carrito">
                    <div ng-repeat="orden in ordenes track by $index" class="tabla_producto">
                        <span class="titulo_order">{{titulos_home.orderOrder}}: </span> {{orden.numero_orden_compra}}<br>
                        <span class="titulo_order">{{titulos_home.fechaOrder}}: </span> {{orden.fecha}}
                        <div class="table-responsive"> 
                          <table class="table table-productos">
                            <thead>
                              <tr>
                                <th></th>
                                <th>{{titulos_home.producto_nombre}}</th>
                                <th>{{titulos_home.producto_precio}}</th>
                                <th>{{titulos_home.producto_cantidad}}</th>
                                <th>Sub Total</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr ng-repeat="producto in orden.orden_detalle track by $index" class="tabla_producto" data="{{$index}}">
                                  <td class="">
                                    <span class="cartImage">
                                        <img src="{{base_url}}{{producto.ruta}}" alt="image" class="img-carrito">
                                    </span>
                                  </td>
                                  <td class="">{{producto.titulo}}</td>
                                  <td class=""> ${{producto.monto}}</td>
                                  <td class="count-input">
                                    <input class="quantity" type="text" value="{{producto.cantidad}}" disabled>
                                  </td>
                                  <td class="">
                                      ${{producto.monto_total}}
                                      <div id="monto_total_oculto_{{$index}}"  class="invisible">{{producto.monto_total_oculto}}</div>
                                  </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div class="orden_total">
                          <span class="titulo_order">Total:</span>
                          <span class="">${{orden.orden_monto}}</span>
                        </div>
                    </div>    
                  </form>
              </div>
          </div>
          <div class="col-md-4" ng-if="id_usuario!=''">
              <div class="summery-box">
                <h4>{{titulos_home.datos_orden}}</h4>
                <p></p>
                <ul class="list-unstyled">
                  <li class="d-flex justify-content-between li-subtotal">
                    <span class="tag">{{titulos_home.usuarioOrden}}</span>
                    <span class="val">{{usuarioOrden}}</span>
                  </li>
                  <li class="d-flex justify-content-between li-subtotal">
                    <span class="tag">{{titulos_home.usuarioEmail}}</span>
                    <span class="val">{{emailOrden}}</span>
                  </li>
                </ul>
              </div>
        </div>
      </div>
      <!-- -->	
    </div>
  </section>
  <!-- -->
</div>
<div id="idUsuarioorden" name="idUsuarioorden"><?=$id_usuario;?></div>
