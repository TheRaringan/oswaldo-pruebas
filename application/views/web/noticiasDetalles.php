<div id="cuerpoNoticias" ng-controller="noticiasDetallesController">
	<!--PARALLAX-->
	<div id="prlx_como_funciona" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
	<!-- stylesheets -->
	<div class="super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after" style="height: 130%; background-image: url(<?=base_url();?>assets/web/img/parallax/parallax7.jpg);"></div>

	<div class="container g-pt-100 g-pb-70">
	  <div class="row">
	    <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
	      <div class="text-center">
	        <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1" style="color:#fff">{{titulos_home.noticias}} </h1>
	      </div>
	    </div>
	  </div>
	</div>
	</div>
	<!-- -->
	<!-- MAIN CONTENT SECTION -->
	<section class="mainContent clearfix aboutUsInfo">
        <div class="container">
			<div class="row">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 ">
					<div class="card" style="width: 45rem;">
						<img class="card-img-top fadeInLeft wow"  ng-src="{{base_url}}{{noticia.ruta}}" alt="Card image cap">
						<div class="card-body fadeInLeft wow">
							<h3 class="card-title">{{noticia.titulo}}</h3>
							<span class="sub-notice col-xs-12">  
								<i class="fa fa-user info-news" aria-hidden="true"></i>
				    			<label class="pr-1 parrafos">{{noticia.usuario}}</label> 
				    	    </span>
				    	    <span class="sub-notice col-xs-12">  
								<i class="fa fa-calendar info-news" aria-hidden="true"></i>
				    			<label class="pr-1 parrafos">{{noticia.fecha}}</label> 
				    	    </span>
							<p class="card-text parrafos" ng-bind-html="noticia.descripcion"></p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
				<!-- -->	
					<div class="panel panel-default recentBlogPosts">
		                <div class="card-header letra">{{titulos_home.otras_noticias}}</div>
		                <div class="panel-body">
			                <div class="media fadeInUp wow" ng-repeat="notice in sub_noticias track by $index">
			                    <a class="media-left" href="blog-single-right-sidebar.html">
			                      <img class="media-object" src="<?=base_url();?>{{notice.ruta}}" alt="">
			                    </a>
			                    <div class="media-body">
			                      <h4 class="media-heading"><a href="{{base_url}}{{url.menu4}}/{{notice.slug}}">{{notice.titulo}}</a></h4>
			                      
			                      <p><i class="fa fa-calendar" aria-hidden="true"></i>{{notice.dias}} {{notice.mes}}</p>
			                    </div>
			                </div>
			            </div>
		            </div>
		        <!-- -->    
				</div>
			</div>
        </div>
       	<div id="slug_noticia" name="slug_noticia" style="display: none;"><?php echo $slug_noticia; ?></div>
    </section>
</div>
