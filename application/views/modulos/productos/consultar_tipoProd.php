<div class="page-wrapper">
	<div class="container-fluid" ng-controller="tipoProd_consultaController">
		<div class="row page-titles">
			<div class="col-md-6 col-8 align-self-center titulo-dashboard">
				<h3 class="text-themecolor">Productos</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Productos</li>
					<li class="breadcrumb-item active">Tipo de Producto</li>
					<li class="breadcrumb-item active">Consultar</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">{{titulo_pagina}}</h4>
						<div class="table-responsive m-t-40">
							<form id="formConsultaTipoProd" method="POST" target="_self">
								<input type="hidden" id="id_tipo_prod" name="id_tipo_prod" ng-model="id_tipo_prod" >
								<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
								<div class="col-lg-12 mg-b30">
									<a href="{{base_url}}cms/tipo_prod">
										<div class="btn btn-success btn-new" >Nuevo</div>
									</a>
								</div>
								<table id="myTable" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>#</th>
											<th>Idioma</th>
											<th>Categoría Producto</th>
											<th>Título</th>
											<th>Descripción</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat = "tipo_prod in tipo_prod track by $index">
											<td class="">{{tipo_prod.id}}</td>
											<td class="">{{tipo_prod.descripcion_idioma}}</td>
											<td class="">{{tipo_prod.titulo_catprod}}</td>
											<td class="">{{tipo_prod.titulo}}</td>
											<td class="">{{tipo_prod.descripcion_sin_html}}{{tipo_prod.estatus}}</td>
											<td class="centrado">
												<div class="form-group flotar_izquierda">
													<div id="ver{{$index}}" ng-click="ver_tipoprod($index)" data="{{tipo_prod.id}}" class="btn btn-primary flotar_izquierda" title="modificar">
														<i class="fas fa-pencil-alt" aria-hidden="true"></i>
													</div>
													<div id="btn_activar{{$index}}_a" name="btn_activar{{$index}}_a" class="btn btn-warning flotar_izquierda" ng-class="{visible:tipo_prod.estatus=='1',invisible:tipo_prod.estatus!=1}" title="Inactivar" data-ng-click="activar_registro($event)" data="{{tipo_prod.id}}|{{tipo_prod.estatus}}">
													<i class="fa fa-lock" aria-hidden="true"></i>
													</div>
													<div id="btn_activar{{$index}}_b" name="btn_activar{{$index}}_b"  class="btn btn-success flotar_izquierda" ng-class="{visible:tipo_prod.estatus!='1',invisible:tipo_prod.estatus==1}" title="Publicar" data-ng-click="activar_registro($event)" data="{{tipo_prod.id}}|{{tipo_prod.estatus}}">
													<i class="fa fa-check" aria-hidden="true"></i>
													</div>
													<div id="btn_eliminar{{$index}}" name="btn_eliminar{{$index}}"  class="btn btn-danger flotar_izquierda" title="Eliminar" data-ng-click="eliminar_registro($event)" data="{{tipo_prod.id}}|2">
													<i class="fa fa-window-close" aria-hidden="true"></i>
													</div>
												</div>
											</td>
									</tbody>
								</table>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
