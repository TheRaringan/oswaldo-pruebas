<div class="page-wrapper">
	<div class="container-fluid" ng-controller="marcaConsultasController">
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center titulo-dashboard">
				<h3 class="text-themecolor">Productos</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Productos</li>
					<li class="breadcrumb-item active">Consultar Líneas</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">{{titulo_pagina}}</h4>
						<div class="table-responsive m-t-40">
                            <form id="formConsultaMarca" name="formConsultaMarca" method="POST" target="_self">
                                <input type="hidden" id="id_marca" name="id_marca" ng-model="id_marca" >
                                 <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                <div class="col-lg-12 mg-b30">
                                    <a href="{{base_url}}cms/linea">
                                        <div class="btn btn-success btn-new">Nuevo</div>
                                    </a>
                                </div>
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Idioma</th>
                                            <th>Título</th>
                                            <th style="width:200px;">Descripción</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat = "marcas in marcas track by $index">
                                            <td class="">{{marcas.id}}</td>
                                            <td class="">{{marcas.descripcion_idioma}}</td>
                                            <td class="">{{marcas.titulo}}</td>
                                            <td class="">{{marcas.descripcion_sin_html}}</td>
                                            <td class="centrado">
                                                <div class="form-group flotar_izquierda">
                                                    <div id="ver{{$index}}" ng-click="ver_marcas($index)" data="{{marcas.id}}" class="btn btn-primary flotar_izquierda" title="modificar">
                                                        <i class="fas fa-pencil-alt" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_activar{{$index}}_a" name="btn_activar{{$index}}_a" class="btn btn-warning flotar_izquierda" ng-class="{visible:marcas.estatus=='1',invisible:marcas.estatus!=1}" title="Inactivar" data-ng-click="activar_registro($event)" data="{{marcas.id}}|{{marcas.estatus}}">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_activar{{$index}}_b" name="btn_activar{{$index}}_b"  class="btn btn-success flotar_izquierda" ng-class="{visible:marcas.estatus!='1',invisible:marcas.estatus==1}" title="Publicar" data-ng-click="activar_registro($event)" data="{{marcas.id}}|{{marcas.estatus}}">
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_eliminar{{$index}}" name="btn_eliminar{{$index}}"  class="btn btn-danger flotar_izquierda" title="Eliminar" data-ng-click="eliminar_registro($event)" data="{{marcas.id}}|2">
                                                    <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </td>
                                    </tbody>
                                </table>
                            </form>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
