<div class="page-wrapper">
	<div class="container-fluid" ng-controller="Color_Controller">
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center titulo-dashboard">
				<h3 class="text-themecolor">Productos</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Productos</li>
					<li class="breadcrumb-item active">Colores</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">{{titulo_pagina}}</h4>
						<h6 class="card-subtitle">{{subtitulo_pagina}}</h6>
						<hr>
						<form class="form-material m-t-40"  name="formularioMarca" id="formularioMarca">
							<div class="row p-20">
								<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 padding0_min " style="padding-top: 5px; ">
									<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
									<label class="">Idioma:</label>
									<select name="idioma" id="idioma" class="form-control m-bot15 select-picker" data-done-button="true" data-actions-box="true"  data-live-search-placeholder="Seleccione tipo de idioma" placholder="Seleccione tipo de idioma" data-style="btn-fff " data-live-search="true" ng-options="option.descripcion for option in idioma track by option.id" ng-model="color.id_idioma" data-size="3">
										<option value="">--Seleccione un idioma--</option>
									</select>
								</div>
								<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
									<div class="form-group">
										<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
										<label class="">Color:</label>
											<div class="controls">
												<input type="text" name="titulo" id="titulo" class="form-control form-control-line" ng-model="color.descripcion" placeholder="Ingrese el color" onKeyPress="return valida(event,this,4,100)" onBlur="valida2(this,4,100);">
											</div>
									</div>
								</div>
							</div>
							<div class="row p-20">
							<!-- 	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
									<div class="form-group">
										<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
											<label>Descripción de la Línea:</label>
											<div class="div_wisig" id="div_descripcion" name="div_descripcion" data-toggle="modal" data-target="#wisiModal" data-whatever="@mdo" ng-click="wisi_modal('1')">
												Pulse aquí para ingresar el contenido
											</div>
									</div>
								</div> -->
								<!-- <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
									<div class="img_principal"  ng-click="seleccione_img_principal()">
										<div ng-class="{'invisible':activo_img==='activo','visible':activo_img==='inactivo'}">
											<span>
												<i class="fas fa-image" aria-hidden="true"></i>
												Seleccione imagen
											</span>
										</div>
										<div class="mensaje_img_principal" ng-class="{'visible':activo_img==='activo','invisible':activo_img==='inactivo'}">
												<img class="img_noticias" id="img_seleccionada" ng-src="{{base_url}}{{marcas.imagen}}" height="115" data="">
										</div>
									</div>
								</div> -->
							</div>
							<div class="row button-group">
								<div class="col-lg-6">
									<div class="row">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="row">
										<div class="col-lg-4 col-md-4" ng-if="id_color!=''">
											<a href="<?=base_url();?>cms/color_prod">
												<button type="button" class="btn waves-effect waves-light btn-block btn-success">Nuevo</button>
											</a>
										</div>
										<div class="col-lg-4 col-md-4" ng-if="id_color==''">
											<button type="button" class="btn waves-effect waves-light btn-block btn-success" ng-click="limpiar_cajas_color()">Limpiar</button>
										</div>

										<div class="col-lg-4 col-md-4">
											<a href="<?=base_url();?>cms/color_prod/consultar_color">
												<button type="button" class="btn waves-effect waves-light btn-block btn-danger" >{{titulo_cons}}</button>
											</a>
										</div>
										<div class="col-lg-4 col-md-4">
												<button type="button" class="btn waves-effect waves-light btn-block btn-info" ng-click="registrarColor()">{{titulo_registrar}}</button>
										</div>
										<input type="hidden" name="id_color" id="id_color" value="<?php if(isset($id)){echo $id;}?>">
										<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="wisiModal" name="wisiModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">Ingrese el contenido</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-group">
											<textarea id="textarea_editor" name="textarea_editor" class="textarea_editor form-control" rows="15" placeholder="Ingrese texto..."></textarea>
                                    </div>
                                </form>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="cerrarModal" type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" ng-click="agregarWisi()">Agregar</button>
                    </div>
                </div>
            </div>
        </div>

		<div class="modal fade" id="modal_img1" name="modal_img1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
			<div class="modal-dialog modal-lg .modal-sm">
			  <div class="modal-content">
				<div class="modal-header header_conf">
					<button type="button" id="cerrar_mensaje2" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<p id="cabecera_mensaje" name="cabecera_mensaje"><b> Información:</b> Seleccione uma imagen</p>
				</div>
				<div class="modal-body" id="cuerpo_mensaje" name="cuerpo_mensaje">
					<!--Modal body -->
					<div class="centrar_galeria" ng-show="galery.length ==0">Galería de imágenes Slider</div>
					<div id="" ng-show="galery.length!=0" class="fade-in-out">
						<div>
							<div class="form-group">
								<input type='text' name='filtro_marcas' id='filtro_marcas' placeholder='Ingrese el valor a filtrar' class="form-control" ng-model="searchMarcas">
							</div>
							<div class="col-lg-12" padding="0px;">
								<div class="row">
									<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 divbiblioteca" ng-repeat="imagen in galery|filter:searchMarcas track by $index">
										<img class="imgbiblioteca"  id="img_biblioteca{{$index}}" ng-src="{{base_url}}/{{imagen.ruta}}" height="115" data="{{imagen.id}}|{{imagen.ruta}}" data-ng-click="seleccionar_imagen($event)">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="clear:both"></div>
					<!-- modal body-->
				</div>
				<div class="modal-footer footer_conf">
					<!-- Footter del modal -->
					  <button type="button" name="modal_reporte_salir" id="modal_reporte_salir" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
					<!-- Fin footter del modal -->
				</div>
				<div style="clear:both"></div>
			  </div>
			</div>
		</div>
	</div>
</div>
